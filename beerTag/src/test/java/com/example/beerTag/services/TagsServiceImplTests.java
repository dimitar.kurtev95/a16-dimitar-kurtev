package com.example.beerTag.services;

import com.example.beerTag.entities.Tag;
import com.example.beerTag.exceptions.DuplicateEntityException;
import com.example.beerTag.exceptions.EntityNotFoundException;
import com.example.beerTag.repositories.TagRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class TagsServiceImplTests {
    private List<Tag> tags;

    @Mock
    TagRepository tagRepository;

    @InjectMocks
    TagServiceImpl tagService;


    @Before
    public void init() {
        Tag tag1 = new Tag();
        tag1.setId(1);
        tag1.setName("tag1");
        tags = new ArrayList<>();
        tags.add(tag1);
    }

    @Test
    public void getAll_Should_ReturnAllTags() {
        //Arrange
        Mockito.when(tagRepository.getAll(""))
                .thenReturn(tags);
        //Act
        List<Tag> returnedTags = tagService.getAll("");

        //Assert
        Assert.assertEquals(tags, returnedTags);
        Assert.assertEquals(tags.size(), returnedTags.size());
    }

    @Test
    public void getById_Should_ReturnTag_WhenTagExists() {
        //Arrange
        Mockito.when(tagRepository.getById(1))
                .thenReturn(tags.get(0));
        //Act
        Tag returnedTag = tagService.getById(1);

        //Assert
        Assert.assertEquals(tags.get(0), returnedTag);
    }

    @Test
    public void getById_Should_ThrowException_WhenTagDoesNotExist() {
        //Arrange
        Mockito.when(tagRepository.getById(1))
                .thenThrow(EntityNotFoundException.class);
        //Act
        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> tagService.getById(1));
    }

    @Test
    public void createTag_Should_ReturnTag_WhenItDoesNotExist_AndUserHasAuthority() {
        //Arrange
        Mockito.when(tagRepository.createTag(tags.get(0)))
                .thenReturn(tags.get(0));
        //Act
        Tag returnedTag = tagService.createTag(tags.get(0));

        //Assert
        Assert.assertEquals(tags.get(0), returnedTag);
    }

    @Test
    public void createTag_Should_ThrowException_WhenTagExists() {
        //Arrange
        Mockito.when(tagRepository.createTag(tags.get(0)))
                .thenThrow(DuplicateEntityException.class);
        //Act
        //Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> tagService.createTag(tags.get(0)));
    }

    @Test
    public void updateTag_Should_ReturnTag_WhenItExist_AndUserHasAuthority() {
        //Arrange
        Mockito.when(tagRepository.updateTag(tags.get(0).getId(), tags.get(0)))
                .thenReturn(tags.get(0));
        //Act
        Tag returnedTag = tagService.updateTag(tags.get(0).getId(), tags.get(0));

        //Assert
        Assert.assertEquals(tags.get(0), returnedTag);
    }

    @Test
    public void updateTag_Should_ThrowException_WhenTagDoesNotExist() {
        //Arrange
        Mockito.when(tagRepository.updateTag(tags.get(0).getId(), tags.get(0)))
                .thenThrow(EntityNotFoundException.class);
        //Act
        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> tagService.updateTag(tags.get(0).getId(), tags.get(0)));
    }

    @Test
    public void deleteTag_Should_deleteTag_WhenTagExists() {
        //Arrange

        //Act
        tagService.deleteTag(0);
        //Assert
        Mockito.verify(tagRepository, Mockito.times(1)).deleteTag(0);
    }
}
