package com.example.beerTag.services;

import com.example.beerTag.entities.Country;
import com.example.beerTag.exceptions.DuplicateEntityException;
import com.example.beerTag.exceptions.EntityNotFoundException;
import com.example.beerTag.repositories.CountryRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class CountryServiceImplTests {
    private List<Country> countries;
    private List<Country> emptyCountryList;

    @Mock
    CountryRepository countryRepository;

    @InjectMocks
    CountryServiceImpl countryService;

    @Before
    public void createAndPopulateCountryList() {
        countries = new ArrayList<>();
        emptyCountryList = new ArrayList<>();
        countries.add(new Country(1, "Bulgaria"));
        countries.add(new Country(2, "Germany"));
        countries.add(new Country(3, "Italy"));
        countries.add(new Country(4, "Spain"));
    }

    @Test
    public void getAll_Should_ReturnAllBeers() {
        //Arrange
        Mockito.when(countryRepository.getAll())
                .thenReturn(countries);

        //Act
        List<Country> countryList = countryService.getAll();

        //Assert
        Assert.assertEquals(countryList, countries);
    }

    @Test
    public void getById_Should_ReturnCountry_WhenExists() {
        //Arrange
        Mockito.when(countryRepository.getById(1))
                .thenReturn(countries.get(0));

        //Act
        Country returnedCountry = countryService.getById(1);

        //Assert
        Assert.assertEquals(returnedCountry, countries.get(0));
    }

    @Test
    public void getById_Should_ThrowException_WhenCountryDoesNotExist() {
        //Arrange
        Mockito.when(countryRepository.getById(1))
                .thenThrow(EntityNotFoundException.class);

        //Act
        //Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> countryService.getById(1));
    }

    @Test
    public void getCountryByName_Should_ReturnCountry_WhenCountryWithNameExists() {
        //Arrange
        String testName = "Bulgaria";
        Mockito.when(countryRepository.filteredByName(testName))
                .thenReturn(Collections.singletonList(countries.get(0)));
        //Act
        List<Country> expectedCountries = countryService.getCountriesByName(testName);

        //Assert
        Assert.assertEquals(expectedCountries.get(0),
                countries.stream()
                        .filter(country -> country.getName().equals(testName))
                        .findFirst()
                        .get());
    }

    @Test
    public void createCountry_Should_ReturnCountry_WhenItDoesNotExist() {
        //Arrange
        Mockito.when(countryRepository.createCountry(countries.get(2)))
                .thenReturn(countries.get(2));
        //Act
        Country returnedCountry = countryService.createCountry(countries.get(2));

        //Assert
        Assert.assertEquals(returnedCountry, countries.get(2));
        Mockito.verify(countryRepository, Mockito.times(1))
                .createCountry(countries.get(2));
    }

    @Test
    public void createCountry_Should_ThrowException_WhenCountryExists() {
        //Arrange
        Mockito.when(countryRepository.createCountry(countries.get(2)))
                .thenThrow(DuplicateEntityException.class);
        //Act
        //Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> countryService.createCountry(countries.get(2)));
    }

    @Test
    public void updateCountry_Should_ReturnCountry_WhenCountryExists() {
        //Arrange
        Mockito.when(countryRepository.updateCountry(countries.get(3).getId(), countries.get(3)))
                .thenReturn(countries.get(3));
        //Act
        Country returnedCountry = countryService.updateCountry(countries.get(3).getId(), countries.get(3));

        //Assert
        Assert.assertEquals(returnedCountry, countries.get(3));
    }

    @Test
    public void updateCountry_Should_ThrowException_WhenCountryIdDoesNotExist() {
        //Arrange
        Mockito.when(countryRepository.updateCountry(10, countries.get(3)))
                .thenThrow(EntityNotFoundException.class);
        //Act
        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> countryService.updateCountry(10, countries.get(3)));
    }

    @Test
    public void deleteCountry_Should_DeleteCountry_WhenCountryExists() {
        //Arrange
        //Act
        countryService.deleteCountry(1);
        //Assert
        Mockito.verify(countryRepository, Mockito.times(1)).deleteCountry(1);
    }
}
