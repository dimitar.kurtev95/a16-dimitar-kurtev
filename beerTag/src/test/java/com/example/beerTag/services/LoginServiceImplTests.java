package com.example.beerTag.services;

import com.example.beerTag.entities.Login;
import com.example.beerTag.repositories.LoginRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class LoginServiceImplTests {

    @Mock
    LoginRepository loginRepository;

    @InjectMocks
    LoginServicesImpl loginServices;

    @Test
    public void getByEmailShould_ReturnLogin_WhenLoginExists() {
        //Arrange
        Login expectedLogin = new Login();
        expectedLogin.setUsername("test");

        Mockito.when(loginServices.getByUsername(expectedLogin.getUsername())).thenReturn(expectedLogin);
        //Act
        Login login =loginServices.getByUsername(expectedLogin.getUsername());
        //Assert
        Assert.assertSame(expectedLogin, login);
    }


}
