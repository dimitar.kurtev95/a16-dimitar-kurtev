package com.example.beerTag.controllers;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ErrorHandlerController implements ErrorController {

    @RequestMapping("/error")
    public String handleError(@RequestParam(name="errorMessage",defaultValue = "") String errorMessage
            , Model model) {

        model.addAttribute("errorMessage", errorMessage);
        return "error";

    }

    @Override
    public String getErrorPath() {
        return "/error";
    }

//    private int getErrorCode(HttpServletRequest httpRequest) {
//        return (Integer) httpRequest.getAttribute("javax.servlet.error.status.code");
//    }
}
