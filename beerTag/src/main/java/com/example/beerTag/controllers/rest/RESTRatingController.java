package com.example.beerTag.controllers.rest;

import com.example.beerTag.entities.Beer;
import com.example.beerTag.entities.Rating;
import com.example.beerTag.entities.UserDetail;
import com.example.beerTag.exceptions.EntityNotFoundException;
import com.example.beerTag.exceptions.InvalidOperationException;
import com.example.beerTag.models.DtoMapper;
import com.example.beerTag.models.RatingDto;
import com.example.beerTag.services.BeerServices;
import com.example.beerTag.services.RatingService;
import com.example.beerTag.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/ratings")
public class RESTRatingController {

    private RatingService service;
    private DtoMapper dtoMapper;
    private UserService userService;
    private BeerServices beerServices;

    @Autowired
    public RESTRatingController(RatingService service, DtoMapper dtoMapper, UserService userService, BeerServices beerServices) {
        this.service = service;
        this.dtoMapper = dtoMapper;
        this.userService = userService;
        this.beerServices = beerServices;
    }

    @GetMapping
    List<Rating> getAll() {
        return service.getAll();
    }

//    @GetMapping("/beers/{beerId}")
//    public List<Rating> getBeerAllRatings(@PathVariable int beerId) {
//
//        try {
//            return service.getBeerAllRatings(beerId);
//        } catch (EntityNotFoundException e) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
//        }
//    }

    @GetMapping("/{id}")
    public Rating getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    //TODO validation in service if rated beer is in drank list of the user !!!
    @PostMapping("/{beerId}")
    // not sur for this endpoint
    public Rating createRating(@RequestBody RatingDto ratingDto,
                               @RequestHeader(name = "Authorization")
                                       String authorization,
                               @PathVariable int beerId) {

        try {
            UserDetail userDetail = userService.getByEmail(authorization);
            Beer beer = beerServices.getBeerById(beerId);
            Rating rating = dtoMapper.createRatingFromDto(ratingDto);
            return service.createRating(rating, userDetail, beer);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
         catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

    }

    @PutMapping("/{id}")
    public Rating updateRating(@PathVariable int id, @RequestBody RatingDto ratingDto,
                               @RequestHeader(name = "Authorization")
                                       String authorization) {
        try {
            Rating ratingToUpdate = service.getById(id);
            ratingToUpdate.setReview(ratingDto.getReview());
            ratingToUpdate.setRating(ratingDto.getRating());
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            ratingToUpdate.setDateCreated(dateFormat.format(date));
            return service.updateRating(authorization, ratingToUpdate);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

    }

    @DeleteMapping("/{id}")
    public void deleteRating(@PathVariable int id, @RequestHeader(name = "Authorization")
            String authorization) {

        try {
            Rating rating = service.getById(id);
            service.deleteRating(rating, authorization);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }


    }
}
