package com.example.beerTag.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.WhereJoinTable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor// (access = AccessLevel.PRIVATE)
@Table(name = "users_details")
public class UserDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "profile_picture")
    private String photo;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;

    @OneToOne
    @JoinColumn(name = "login_username")
    private Login login;


    @LazyCollection(LazyCollectionOption.FALSE)
    @JsonIgnore
    @ManyToMany
    @JoinTable(
            name = "users_beers_status",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "beer_id")
    )
    @WhereJoinTable(clause = "status_id = 2")
    List<Beer> wishList = new ArrayList<>();

    @LazyCollection(LazyCollectionOption.FALSE)
    @JsonIgnore
    @ManyToMany
    @JoinTable(
            name = "users_beers_status",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "beer_id")
    )
    @WhereJoinTable(clause = "status_id = 1")
    List<Beer> drankList = new ArrayList<>();

    @LazyCollection(LazyCollectionOption.FALSE)
    @JsonIgnore
    @OneToMany(mappedBy = "userDetail")
    private List<Rating> beerRatings = new ArrayList<>();



    public UserDetail(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

}
