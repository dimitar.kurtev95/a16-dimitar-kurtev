package com.example.beerTag.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "styles")
public class Style {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "style_id")
    private int id;

    @Column(name = "style_name")
    private String name;

    @Column(name = "style_description")
    private String description;

    @Column(name="date_created")
    private String date;

//    @JsonIgnore
//    @ManyToOne
//    @JoinColumn(name = "user_id")
//    private UserDetail createdByUserDetailId;

    @Column(name = "is_deleted")
    private boolean isDeleted;
}

