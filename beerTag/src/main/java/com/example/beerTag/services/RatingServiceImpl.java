package com.example.beerTag.services;

import com.example.beerTag.entities.Beer;
import com.example.beerTag.entities.Rating;
import com.example.beerTag.entities.UserDetail;
import com.example.beerTag.exceptions.EntityNotFoundException;
import com.example.beerTag.exceptions.InvalidOperationException;
import com.example.beerTag.repositories.RatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RatingServiceImpl implements RatingService {

    private RatingRepository repository;

    @Autowired
    public RatingServiceImpl(RatingRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Rating> getAll() {
        return repository.getAll();
    }

//    @Override
//    public List<Rating> getBeerAllRatings(int beerId) {
//        try {
//            return repository.getBeerAllRatings(beerId);
//        } catch (EntityNotFoundException e) {
//            throw new EntityNotFoundException("Beer", beerId);
//        }
//    }

    @Override
    public Rating createRating(Rating rating, UserDetail userDetail, Beer beer) {
        if (beer.getBeerRatings()
                .stream()
                .anyMatch(r -> r.getUserDetail().getEmail().equals(userDetail.getEmail()))) {
            throw new InvalidOperationException(String.format("User %s %s can not rate beer %s more than once",
                    userDetail.getFirstName(), userDetail.getLastName(), beer.getName()));
        }
        rating.setUserDetail(userDetail);
        rating.setBeer(beer);

        return repository.createRating(rating);
    }

    @Override
    public Rating updateRating(String authorization, Rating ratingToUpdate) {
        if (!ratingToUpdate.getUserDetail().getEmail().equals(authorization)) {
            throw new InvalidOperationException(String.format("User with email %s can not modify this rating",
                    authorization));
        }

        return repository.updateRating(ratingToUpdate);

    }

    @Override
    public void deleteRating(Rating rating, String email) {

            if(!rating.getUserDetail().getEmail().equals(email)) {
                throw new InvalidOperationException(String.format("User with email %s can not modify this rating", email));
            }
            repository.deleteRating(rating);

    }

    @Override
    public Rating getById(int id) {
        try {
            return repository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Rating", id);
        }
    }
}
