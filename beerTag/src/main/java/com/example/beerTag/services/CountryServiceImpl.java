package com.example.beerTag.services;

import com.example.beerTag.exceptions.DuplicateEntityException;
import com.example.beerTag.entities.Country;
import com.example.beerTag.exceptions.EntityNotFoundException;
import com.example.beerTag.repositories.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {
    CountryRepository repository;

    @Autowired
    public CountryServiceImpl(CountryRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Country> getAll() {
        return repository.getAll();
    }

    @Override
    public Country getById(int id) {
        return repository.getById(id);
    }

    @Override
    public List<Country> getCountriesByName(String name) {
        return repository.filteredByName(name);
    }

    @Override
    public Country createCountry(Country country) {
        try {
            return repository.createCountry(country);
        } catch (DataIntegrityViolationException ex) {
            throw new DuplicateEntityException("Country", "Name", country.getName());
        }
    }

    @Override
    public Country updateCountry(int id, Country country) {
        try {
            return repository.updateCountry(id, country);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Country", id);
        }
    }

    @Override
    public void deleteCountry(int id) {
        try {
            repository.deleteCountry(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Country", id);
        }
    }
}
