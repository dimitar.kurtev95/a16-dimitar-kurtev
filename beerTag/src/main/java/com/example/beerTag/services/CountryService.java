package com.example.beerTag.services;

import com.example.beerTag.entities.Country;

import java.util.List;

public interface CountryService {


    List<Country> getAll();

    Country getById(int id);

    List<Country> getCountriesByName(String name);

    Country createCountry(Country country);

    Country updateCountry(int id,Country country);

    void deleteCountry(int id);

}

