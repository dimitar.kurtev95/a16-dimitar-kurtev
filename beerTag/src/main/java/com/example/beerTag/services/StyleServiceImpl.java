package com.example.beerTag.services;

import com.example.beerTag.entities.Beer;
import com.example.beerTag.exceptions.DuplicateEntityException;
import com.example.beerTag.exceptions.EntityNotFoundException;
import com.example.beerTag.entities.Style;
import com.example.beerTag.models.StyleDto;
import com.example.beerTag.repositories.BeerRepository;
import com.example.beerTag.repositories.StylesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StyleServiceImpl implements StyleService {

//    public static final String UPDATE_AUTHORIZATION_LEVEL = "Admin";
//    private static final String INVALID_OPERATION_MESSAGE = "User %s %s cannot modify style";

    private StylesRepository stylesRepository;


    @Autowired
    public StyleServiceImpl(StylesRepository stylesRepository) {
        this.stylesRepository = stylesRepository;
    }

    @Override
    public List<Style> getAll(String name) {

        return stylesRepository.getAll(name);
    }

    @Override
    public Style getById(int id) {
        try {
            return stylesRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Style", id);
        }
    }

//    @Override
//    public List<Style> getByName(String name) {
//        try {
//            return stylesRepository.getByName(name);
//        } catch (EntityNotFoundException e) {
//            throw new EntityNotFoundException("Style", "Name", name);
//        }
//    }

//    @Override// check if styleid exist
//    public List<Beer> getBeersByStyle(int styleId) {
//
////        return beerRepository.getBeerByStyle(styleId);
//        return null;
//    }

    @Override
    public void createStyle(Style style) {
        try {
            stylesRepository.createStyle(style);
        } catch (DuplicateEntityException e) {
            throw new DuplicateEntityException("Style", "name", style.getName());
        }
    }

    @Override
    public void updateStyle(Style style) {

        stylesRepository.updateStyle(style);

    }

    @Override
    public void deleteStyle(Style style) {
        style.setDeleted(true);
        stylesRepository.deleteStyle(style);

    }
}
