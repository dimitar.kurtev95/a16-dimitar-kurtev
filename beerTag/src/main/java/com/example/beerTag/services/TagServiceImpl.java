package com.example.beerTag.services;

import com.example.beerTag.entities.Tag;
import com.example.beerTag.exceptions.DuplicateEntityException;
import com.example.beerTag.exceptions.EntityNotFoundException;
import com.example.beerTag.repositories.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagServiceImpl implements TagService {

    TagRepository repository;

    @Autowired
    public TagServiceImpl(TagRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Tag> getAll(String name) {
        return repository.getAll(name);
    }

    @Override
    public Tag getById(int id) {
        try {
            return repository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Tag", id);
        }
    }

    @Override
    public Tag createTag(Tag tag) {
        try {
            return repository.createTag(tag);
        } catch (DataIntegrityViolationException ex) {
            throw new DuplicateEntityException("Tag", "Name", tag.getName());
        }
    }

    @Override
    public Tag updateTag(int id, Tag tag) {
        try {
            return repository.updateTag(id, tag);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Tag", id);
        }
    }

    @Override
    public void deleteTag(int id) {
        try {
            repository.deleteTag(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Tag", id);
        }
    }
}

