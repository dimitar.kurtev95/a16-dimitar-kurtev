package com.example.beerTag.services;

import com.example.beerTag.entities.Beer;
import com.example.beerTag.entities.Rating;
import com.example.beerTag.entities.UserDetail;

import java.util.List;

public interface RatingService {

    List<Rating> getAll();
  //  List<Rating>getBeerAllRatings(int beerId);

    Rating createRating(Rating rating, UserDetail userDetail, Beer beer);

    Rating updateRating(String authorization, Rating rating);

    void deleteRating(Rating rating, String authorization);

    Rating getById(int id);


}
