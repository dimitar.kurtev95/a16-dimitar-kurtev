package com.example.beerTag.services;

import com.example.beerTag.entities.Brewery;
import com.example.beerTag.exceptions.DuplicateEntityException;
import com.example.beerTag.exceptions.EntityNotFoundException;
import com.example.beerTag.repositories.BreweriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BreweryServiceImpl implements BreweryService {

    private BreweriesRepository repository;

    @Autowired
    public BreweryServiceImpl(BreweriesRepository breweriesRepository) {
        this.repository = breweriesRepository;
    }

    @Override
    public List<Brewery> getAll(String name, String countryName) {
        return repository.getAll(name, countryName);
    }

    @Override
    public Brewery getById(int id) {
        return repository.getById(id);
    }


    @Override
    public Brewery createBrewery(Brewery brewery) {
        try {
                return repository.createBrewery(brewery);
            } catch (DuplicateEntityException e) {
                throw new DuplicateEntityException("Brewery", "Name", brewery.getName());
            }
    }

    @Override
    public Brewery updateBrewery(int id, Brewery brewery) {
        try {
                return repository.updateBrewery(id, brewery);
            } catch (EntityNotFoundException e) {
                throw new EntityNotFoundException("Brewery", id);
            }
    }

    @Override
    public void deleteBrewery(int id) {
        try {
                repository.deleteBrewery(id);
            } catch (EntityNotFoundException e) {
                throw new EntityNotFoundException("Brewery", id);
            }
    }

}
