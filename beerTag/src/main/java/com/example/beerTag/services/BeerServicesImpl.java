package com.example.beerTag.services;

import com.example.beerTag.exceptions.DuplicateEntityException;
import com.example.beerTag.exceptions.EntityNotFoundException;
import com.example.beerTag.entities.Beer;
import com.example.beerTag.repositories.BeerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BeerServicesImpl implements BeerServices {

    private BeerRepository repository;

    @Autowired
    public BeerServicesImpl(BeerRepository beerRepository) {
        this.repository = beerRepository;
    }

    @Override
    public List<Beer> getAll(String name, String breweryName, String countryName, String styleName, String abv, String rating, String sortBy, String tagID) {
        return repository.getAll(name, breweryName, countryName, styleName, abv, rating, sortBy, tagID);
    }

    @Override
    public Beer getBeerById(int id) {
        try {
            return repository.getBeerById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Beer", id);
        }
    }

    @Override
    public Beer createBeer(Beer beer) {
        try {
            return repository.createBeer(beer);
        } catch (DuplicateEntityException e) {
            throw new DuplicateEntityException("Beer", "Name", beer.getName());
        }
    }

    @Override
    public void updateBeer(Beer beer) {
        try {
            repository.updateBeer(beer);
        } catch (EntityNotFoundException e) {
            throw new DuplicateEntityException("Beer","name", beer.getName());
        }
    }

    @Override
    public void deleteBeer(int id) {
        try {
            repository.deleteBeer(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Beer", id);
        }
    }
}