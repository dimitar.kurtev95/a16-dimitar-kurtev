package com.example.beerTag.services;

import com.example.beerTag.entities.Beer;
import com.example.beerTag.entities.UserDetail;

import java.util.List;

public interface UserService {

    List<UserDetail> getAll(String firstName, String lastName, String sortBy);

   UserDetail getByEmail(String email);

   List <Beer> getTopThreeRated(UserDetail userDetail);

    UserDetail getById(int id);

    void createUser(UserDetail userDetail);

    UserDetail updateUser(UserDetail userDetail);

    void deleteUser(int id);

    void addBeerToDrankList(UserDetail userDetail, int beerId);

    void addBeerToWishList(UserDetail userDetail, int beerId);

    void deleteBeerFromList(UserDetail userDetail, int beerId);

    List<Beer> getList(UserDetail userDetail, String typeOfList);

    //   void addBeerToWishList(Beer beer,User user);
    //   void addBeerToDrankList(Beer beer, User user);
    //   List<Beers> getWishList();
    //   List<Beers> getDrankList();

}
