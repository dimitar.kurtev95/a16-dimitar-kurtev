package com.example.beerTag.services;

import com.example.beerTag.entities.Tag;

import java.util.List;

public interface TagService {
    List<Tag> getAll(String name);

    Tag getById(int id);

    Tag createTag(Tag tag);

    Tag updateTag(int id, Tag tag);

    void deleteTag(int id);
}
