package com.example.beerTag.repositories;

import com.example.beerTag.entities.Login;
import com.example.beerTag.exceptions.EntityNotFoundException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class LoginRepositoryImpl implements LoginRepository {


    private SessionFactory sessionFactory;

    @Autowired
    public LoginRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;

    }

    @Override
    public void deleteLogin(String email) {
        try (Session session = sessionFactory.openSession()) {
            Login login = session.get(Login.class, email);
            login.setEnabled(false);
        }
    }

    @Override
    public Login getByUsername(String email) {
        try (Session session = sessionFactory.openSession()) {

            Login login = session.get(Login.class, email);
            if(login == null || !login.isEnabled()){
                throw new EntityNotFoundException("User", "email", email);
            }

//            Query<Login> query = session
//                    .createQuery("from Login where username like :email");
//            query.setParameter("email", email);
//
//            if (query.list().isEmpty() || !query.list().get(0).isEnabled()) {
//                throw new EntityNotFoundException("User", "email", email);
//            }
//            Login login = query.list().get(0);

            return login;
        }
    }
}
