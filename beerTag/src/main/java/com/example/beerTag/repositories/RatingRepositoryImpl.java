package com.example.beerTag.repositories;

import com.example.beerTag.entities.Beer;
import com.example.beerTag.entities.Rating;

import com.example.beerTag.exceptions.EntityNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Repository
public class RatingRepositoryImpl implements RatingRepository {

    private SessionFactory sessionFactory;


    @Autowired
    public RatingRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;

    }

    @Override
    public List<Rating> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Rating where userDetail.login.enabled", Rating.class).list();
        }
    }

//    @Override
//    public List<Rating> getBeerAllRatings(int beerId) {
//        try (Session session = sessionFactory.openSession()) {
//
//            Beer ratedBeer = session.get(Beer.class, beerId);
//            if (ratedBeer == null || ratedBeer.isDeleted()) {
//                throw new EntityNotFoundException("Beer", beerId);
//            }
//            return ratedBeer.getBeerRatings();
//        }
//}

    @Override
    public Rating createRating(Rating rating) {

        try (Session session = sessionFactory.openSession()) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            rating.setDateCreated(dateFormat.format(date));
            session.save(rating);

            Beer beerToUpdate = rating.getBeer();
            session.refresh(beerToUpdate);
            updateAverageRatingOfBeer(beerToUpdate);


            session.beginTransaction();
            session.update(beerToUpdate);
            session.getTransaction().commit();
        }
        return rating;
    }


    @Override
    public Rating updateRating(Rating rating) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(rating);
            session.getTransaction().commit();

            Beer beerToUpdate = rating.getBeer();
            session.refresh(beerToUpdate);
            updateAverageRatingOfBeer(beerToUpdate);


            session.beginTransaction();
            session.update(beerToUpdate);
            session.getTransaction().commit();

        }
        return rating;
    }

    @Override
    public void deleteRating(Rating rating) {

        try (Session session = sessionFactory.openSession()) {

            session.beginTransaction();
            session.delete(rating);
            session.getTransaction().commit();

            Beer beerToUpdate = rating.getBeer();
            session.refresh(beerToUpdate);
            updateAverageRatingOfBeer(beerToUpdate);


            session.beginTransaction();
            session.update(beerToUpdate);
            session.getTransaction().commit();
        }

    }

    @Override
    public Rating getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Rating rating = session.get(Rating.class, id);
            if (rating == null) {
                throw new EntityNotFoundException("Rating", id);
            }
            return rating;
        }
    }

    private void updateAverageRatingOfBeer(Beer beer) {
        beer.setAverageRating((int) beer.getBeerRatings()
                .stream()
                .mapToInt(Rating::getRating)
                .average()
                .orElse(0));
    }

}

