package com.example.beerTag.repositories;

import com.example.beerTag.exceptions.EntityNotFoundException;
import com.example.beerTag.entities.Country;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CountryRepositoryImpl implements CountryRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public CountryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Country> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Country", Country.class).list();
        }
    }

    @Override
    public Country getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Country country = session.get(Country.class, id);
            if (checkIfExists(id)) {
                throw new EntityNotFoundException("Country", id);
            } else {
                return country;
            }
        }
    }

    @Override
    public List<Country> filteredByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Country> query = session.createQuery("from Country where name like :name", Country.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    @Override
    public Country createCountry(Country country) {
        try (Session session = sessionFactory.openSession()) {
            session.save(country);
            return country;
        }
    }

    @Override
    public Country updateCountry(int id, Country country) {
        try (Session session = sessionFactory.openSession()) {
            if (checkIfExists(id)) {
                throw new EntityNotFoundException("Country", id);
            } else {
                session.beginTransaction();
                session.update(country);
                session.getTransaction().commit();
                return country;
            }
        }
    }

    @Override
    public void deleteCountry(int id) {
        try (Session session = sessionFactory.openSession()) {
            Country country = session.get(Country.class, id);
            if (checkIfExists(id)) {
                throw new EntityNotFoundException("Country", id);
            } else {
                session.beginTransaction();
                session.delete(country);
                session.getTransaction().commit();
            }
        }
    }

    @Override
    public boolean checkIfExists(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Country.class, id) == null;
        }
    }
}
