package com.example.beerTag.repositories;

import com.example.beerTag.entities.Tag;

import java.util.List;

public interface TagRepository {

    List<Tag> getAll(String name);

    Tag getById(int id);

    Tag createTag(Tag tag);

    Tag updateTag(int id,Tag tag);

    void deleteTag(int id);
}
