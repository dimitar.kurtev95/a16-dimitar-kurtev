package com.example.beerTag.models;

import lombok.*;

import javax.validation.constraints.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BeerDto {

    @NotBlank
    @NotNull
    @NonNull
    @Size(min = 5, max = 20, message = "Beer name should be between {min} and {max} characters long.")
    private String name;

    @NotBlank
    @NotNull
    @NonNull
    @Size(min = 10, max = 50, message = "Beer description should be between {min} and {max} characters long.")
    private String description;

    private int breweryId;

    private int styleId;

    private String tags;

    @Positive
    private double abv;

    private String pictureURL;
}
