package com.company.commands;

public class CommandConstants {
    // Error messages
    public static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";
    public static final String INVALID_COMMAND_ERROR_MESSAGE = "Invalid command name: %s";
    public static final String ITEM_NOT_FOUND = "%s %s not found.";
    public static final String ITEM_EXISTS = "%s %s already exists.";
    public static final String EMPTY_LIST = "List is empty...";


    // Success messages
    public static final String ITEM_CREATED_SUCCESS_MESSAGE = "%s %s created on %s.";
    public static final String ITEM_WITH_ID_CREATED_SUCCESS_MESSAGE = "%s %s with ID %d created on %s.";
    public static final String ITEM_ADDED_TO_WORKING_ITEMS = "%s %s with ID %d added to Working items by %s on %s.";
    public static final String ITEM_ACTIVITY_HISTORY = "%s %s activity history: ";
    public static final String PRIORITY_CHANGE_MESSAGE = "Priority of the %s with id %d was changed from %s to %s by %s on %s.";
    public static final String SIZE_CHANGE_MESSAGE = "Size of %s with id %d changed from %s to %s by %s on %s.";
    public static final String STATUS_CHANGE_MESSAGE = "Status of %s with id %d changed from %s to %s by %s on %s.";
    public static final String BUG_SEVERITY_CHANGE_MESSAGE = "%s severity on %s with id %s changed from %s to %s by %s on %s.";
    public static final String FEEDBACK_RATING_CHANGE_MESSAGE = "Rating changed from %d to %d by %s on %s.";
    public static final String ADD_MEMBER_TO_TEAM_SUCCESS_MESSAGE = "Member %s was added to team %S on %s.";
    public static final String ASSIGN_WORK_ITEM_TO_MEMBER_SUCCESS_MESSAGE = "Working item with ID %s was assigned to %s on %s";
    public static final String BOARD_ADDED_TO_TEAM = "Board %s added to Team %s by %s in %s.";
    public static final String WORKING_ITEM_UNASSIGNED = "Working item with id %s unassigned on %s.";
    public static final String COMMENT_ADDED_SUCCESS_MESSAGE = "The comment '%s' added to working item by %s in %s board on %s.";
}
