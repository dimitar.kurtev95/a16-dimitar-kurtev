package com.company.commands.listCommands;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.models.contracts.WorkingItems;

import java.util.List;
import java.util.stream.Collectors;

import static com.company.commands.team.CreateTeam.TEAM_STRING;

public class ListAllWorkingItemsInTeam extends CommandBaseImpl implements Command {
    private static final int CORRECT_NUMBER_OF_ARGS = 1;
    public static final String LABEL_FOR_LIST_ALL_IN_TEAM = "All working items in team %s:";

    public ListAllWorkingItemsInTeam(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {
        CommandHelper.checkForNumberOfParameters(parameters, CORRECT_NUMBER_OF_ARGS);

        String teamName = parameters.get(0);

        StringBuilder print = new StringBuilder("");

        return listAllWorkingItemsInTeam(print, teamName);
    }


    private String listAllWorkingItemsInTeam(StringBuilder print, String team) {

        CommandHelper.checkRepositoryForItem(repository.getTeams(), team, TEAM_STRING);

        print.append(String.format(LABEL_FOR_LIST_ALL_IN_TEAM +System.lineSeparator(), team.toUpperCase()));

        List<WorkingItems> items = repository.getWorkingItems()
                .values()
                .stream()
                .filter(workingItems -> workingItems.getTeamName().equals(team))
                .collect(Collectors.toList());

        for (WorkingItems item : items) {
            print.append(item.toString()+System.lineSeparator());
        }
        return print.toString();
    }
}
