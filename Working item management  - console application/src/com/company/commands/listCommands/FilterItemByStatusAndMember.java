package com.company.commands.listCommands;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.models.contracts.AssignableWorkingItems;
import com.company.models.contracts.WorkingItems;

import java.util.List;
import java.util.stream.Collectors;

import static com.company.commands.CommandConstants.INVALID_COMMAND_ERROR_MESSAGE;

public class FilterItemByStatusAndMember extends CommandBaseImpl implements Command {
    private static final int CORRECT_NUMBER_OF_ARGS = 3;
    public static final String lABEL_FOR_FILTER_STATUS_AND_ASSIGNE = "Working items with status %s and member %s:\n\n";

    public FilterItemByStatusAndMember(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {
        CommandHelper.checkForNumberOfParameters(parameters, CORRECT_NUMBER_OF_ARGS);

        String filter = parameters.get(0);
        if (!filter.toLowerCase().equals("filter")) {
            throw new IllegalArgumentException(String.format(INVALID_COMMAND_ERROR_MESSAGE, filter));
        }

        String status = parameters.get(1);
        String memberName = parameters.get(2);
        StringBuilder print = new StringBuilder("");
        switch (status.toUpperCase()) {
            case "ACTIVE":
            case "FIXED":
            case "DONE":
            case "NOTDONE":
            case "SCHEDULED":
            case "UNSCHEDULED":
            case "NEW":
            case "INPROGRESS":
                return filterWorkingItemsByStatus
                        (print, status.toUpperCase(), memberName);
            default:
                throw new IllegalArgumentException(String.format(INVALID_COMMAND_ERROR_MESSAGE, memberName));
        }
    }

    private String filterWorkingItemsByStatus(StringBuilder print, String status, String memberName) {

        CommandHelper.checkRepositoryForItem(repository.getMembers(),memberName, "Member");

        print.append(String.format(lABEL_FOR_FILTER_STATUS_AND_ASSIGNE, status.toUpperCase(), memberName.toUpperCase()));

        List<AssignableWorkingItems> asignees = repository.getAssignableWorkingItems()
                .values()
                .stream()
                .filter(assignableWorkingItems -> assignableWorkingItems.getAssignees().containsKey(memberName))
                .collect(Collectors.toList());

        List<AssignableWorkingItems> assigneesAndStatus = asignees
                .stream()
                .filter(assignee -> assignee.getStatus().toString().equals(status))
                .collect(Collectors.toList());

        for (AssignableWorkingItems s : assigneesAndStatus) {
            print.append(s.toString() + System.lineSeparator());
        }
        return print.toString();

    }
}
