package com.company.commands.listCommands.sort;

import com.company.commands.CommandBaseImpl;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.models.contracts.Feedback;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.company.commands.CommandConstants.EMPTY_LIST;

public class SortFeedbackByRating extends CommandBaseImpl implements Command {
    public SortFeedbackByRating(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {
        if (repository.getFeedback().isEmpty()) {
            throw new IllegalArgumentException(EMPTY_LIST);
        }

        List<Feedback> sorted = repository.getFeedback()
                .stream()
                .sorted(Comparator.comparing(Feedback::getRating))
                .collect(Collectors.toList());

        return "Feedback is sorted by RATING:" +  System.lineSeparator() + " "+
                sorted.toString().replace(",", System.lineSeparator())
                        .replace("[", "")
                        .replace("]", "") +
                "-------------------------------" + System.lineSeparator();
    }
}
