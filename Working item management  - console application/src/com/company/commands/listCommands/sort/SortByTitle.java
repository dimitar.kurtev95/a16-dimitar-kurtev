package com.company.commands.listCommands.sort;

import com.company.commands.CommandBaseImpl;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.models.contracts.WorkingItems;

import java.util.Comparator;
import java.util.List;

import java.util.stream.Collectors;

import static com.company.commands.CommandConstants.*;

public class SortByTitle extends CommandBaseImpl implements Command {
    public SortByTitle(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {
        if (repository.getWorkingItems().isEmpty()) {
            throw new IllegalArgumentException(EMPTY_LIST);
        }

        List<WorkingItems> sorted = repository.getWorkingItems()
                .values()
                .stream()
                .sorted(Comparator.comparing(WorkingItems::getTitle))
                .collect(Collectors.toList());

        return "The working items are sorted by TITLE:" + System.lineSeparator() + " "+
                sorted.toString().replace(",", System.lineSeparator())
                        .replace("[", "")
                        .replace("]", "")+
                "-------------------------------" + System.lineSeparator();

    }
}
