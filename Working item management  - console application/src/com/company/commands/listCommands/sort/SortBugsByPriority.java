package com.company.commands.listCommands.sort;

import com.company.commands.CommandBaseImpl;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.models.contracts.Bug;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.company.commands.CommandConstants.EMPTY_LIST;

public class SortBugsByPriority extends CommandBaseImpl implements Command {

    public SortBugsByPriority(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {
        if (repository.getBugs().isEmpty()) {
            throw new IllegalArgumentException(EMPTY_LIST);
        }

        List<Bug> sorted = repository.getBugs()
                .stream()
                .sorted(Comparator.comparing(Bug::getPriorityWeight))
                .collect(Collectors.toList());

        return "The bugs are sorted by PRIORITY:" + System.lineSeparator() + " "+
                sorted.toString().replace(",", System.lineSeparator())
                        .replace("[", "")
                        .replace("]", "") +
                "-------------------------------" + System.lineSeparator();
    }
}

