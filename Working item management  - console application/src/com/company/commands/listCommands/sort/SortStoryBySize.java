package com.company.commands.listCommands.sort;

import com.company.commands.CommandBaseImpl;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.models.contracts.Story;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.company.commands.CommandConstants.EMPTY_LIST;

public class SortStoryBySize extends CommandBaseImpl implements Command {
    public SortStoryBySize(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {
        if (repository.getStory().isEmpty()) {
            throw new IllegalArgumentException(EMPTY_LIST);
        }

        List<Story> sorted = repository.getStory()
                .stream()
                .sorted(Comparator.comparing(Story::getSizeWeight))
                .collect(Collectors.toList());

        return "The stories are sorted by SIZE:" + System.lineSeparator() + " "+
                sorted.toString().replace(",", System.lineSeparator())
                        .replace("[", "")
                        .replace("]", "") +
                "-------------------------------" + System.lineSeparator();
    }
}
