package com.company.commands.listCommands;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;

import java.util.List;

public class ListAll extends CommandBaseImpl implements Command {

    public ListAll(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {

        StringBuilder print = new StringBuilder("");

        return listAll(print);
    }

    private String listAll(StringBuilder print) {
        CommandHelper.checkForEmptyMap(repository.getTeams());

        repository.getTeams().forEach((key, value) -> {
            print.append(String.format("~~~~~~~~~~~~~~~~\nTeam %s boards:\n", value.getName()));
            value.getBoard().forEach((string, Board) -> print.append(String.format("%s\n", Board.getName())));
            print.append("~~~~~~~~~~~~~~~~\n");
            value.getBoard().forEach((string, Board) -> {
                print.append(java.lang.String.format("Working items in board %s:\n", Board.getName()));
                Board.getWorkingItems().forEach((id, WorkingItems) -> print.append(WorkingItems.toString()).append(System.lineSeparator()));
            });
        });
        return print.toString();


    }
}