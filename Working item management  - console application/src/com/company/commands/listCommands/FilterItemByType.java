package com.company.commands.listCommands;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.models.contracts.AssignableWorkingItems;
import com.company.models.contracts.WorkingItems;

import java.util.List;
import java.util.stream.Collectors;

import static com.company.commands.CommandConstants.INVALID_COMMAND_ERROR_MESSAGE;
import static com.company.commands.team.CreateTeam.TEAM_STRING;

public class FilterItemByType extends CommandBaseImpl implements Command {
    private static final int CORRECT_NUMBER_OF_ARGS = 3;
    public static final String LABEL_FOR_FILTER_BY_TYPE = "Working items of type %s in team %s:\n\n";

    public FilterItemByType(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {
        CommandHelper.checkForNumberOfParameters(parameters, CORRECT_NUMBER_OF_ARGS);

        String filter = parameters.get(0);
        if (!filter.toLowerCase().equals("filter")) {
            throw new IllegalArgumentException(String.format(INVALID_COMMAND_ERROR_MESSAGE, filter));
        }
        String teamName = parameters.get(1);
        CommandHelper.checkRepositoryForItem(repository.getTeams(), teamName, TEAM_STRING);

        String type = parameters.get(2);

        StringBuilder print = new StringBuilder("");

        switch (type.toUpperCase()) {
            case "BUG":
            case "STORY":
            case "FEEDBACK":
                return filterWorkingItemsByType(print, teamName, type.toUpperCase());
            default:
                throw new IllegalArgumentException(String.format(INVALID_COMMAND_ERROR_MESSAGE, type));
        }
    }

    private String filterWorkingItemsByType(StringBuilder print, String team, String itemType) {

        print.append(String.format(LABEL_FOR_FILTER_BY_TYPE, itemType.toUpperCase(), team.toUpperCase()));

        List<WorkingItems> items = repository.getWorkingItems()
                .values()
                .stream()
                .filter(workingItems -> workingItems.getTeamName().equals(team))
                .filter(workingItems -> workingItems.getClass().toString().toUpperCase().contains(itemType))
                .collect(Collectors.toList());

        for (WorkingItems item : items) {
            print.append(item.toString()+System.lineSeparator());
        }
        return print.toString();
    }

}
