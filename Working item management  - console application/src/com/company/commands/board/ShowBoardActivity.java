package com.company.commands.board;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;

import java.util.List;

import static com.company.commands.board.CreateBoard.BOARD_STRING;
import static com.company.commands.CommandConstants.*;
import static com.company.commands.team.CreateTeam.TEAM_STRING;


public class ShowBoardActivity extends CommandBaseImpl implements Command {
    private static final int CORRECT_NUMBER_OF_ARGS = 2;

    public ShowBoardActivity(Repository repository, Factory factory) {
        super(repository, factory);
    }


    @Override
    public String execute(List<String> parameters) {
        CommandHelper.checkForNumberOfParameters(parameters, CORRECT_NUMBER_OF_ARGS);

        String teamName = parameters.get(0);
        String boardName = parameters.get(1);

        return showBoardActivity(teamName, boardName);
    }

    private String showBoardActivity(String teamName, String boardName) {
        //Check if team exists
        CommandHelper.checkRepositoryForItem(repository.getTeams(), teamName, TEAM_STRING);
        //Check if board exists
        CommandHelper.checkForEmptyMap(repository.getBoards(teamName));
        // Check if board already exists in team.
        CommandHelper.checkRepositoryForExisting(repository.getBoards(teamName), boardName, BOARD_STRING);

        StringBuilder print = new StringBuilder(String.format(ITEM_ACTIVITY_HISTORY + System.lineSeparator(), BOARD_STRING, boardName));

        for (String string : repository.getBoards(teamName).get(boardName).getActivityHistory()) {
            print.append(string+System.lineSeparator());
        }

        return print.toString();
    }

}
