package com.company.commands.board;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.models.contracts.Board;

import java.util.List;

import static com.company.commands.CommandConstants.*;
import static com.company.commands.team.CreateTeam.TEAM_STRING;

public class CreateBoard extends CommandBaseImpl implements Command {

    public static final String BOARD_STRING = "Board";

    private static final int CORRECT_NUMBER_OF_ARGS = 3;

    public CreateBoard(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {
        CommandHelper.checkForNumberOfParameters(parameters, CORRECT_NUMBER_OF_ARGS);

        String boardName = parameters.get(0);
        String teamName = parameters.get(1);
        String memberName = parameters.get(2);


        return createBoard(boardName, teamName, memberName);
    }

    private String createBoard(String boardName, String teamName, String memberName) {
        //check if team exists
        CommandHelper.checkRepositoryForItem(repository.getTeams(), teamName, TEAM_STRING);

        //check if member exist
        CommandHelper.checkIfMemberIsInTeam(repository.getTeams().get(teamName).getMember(), repository.getMembers().get(memberName), memberName, teamName);


        // Check if board already exists in team.
        CommandHelper.checkRepositoryForExisting(repository.getBoards(teamName), boardName, BOARD_STRING);

        // Creating board
        Board board = factory.CreateBoard(boardName);

        // Adding message to board and member activity history.
        board.addToActivityHistory(String.format(BOARD_ADDED_TO_TEAM, boardName, teamName, memberName, CommandHelper.dateAndTime));
        repository.getMembers().get(memberName).addToActivityHistory((String.format(BOARD_ADDED_TO_TEAM, boardName, teamName, memberName, CommandHelper.dateAndTime)));

        repository.addBoard(teamName, board);


        return String.format(ITEM_CREATED_SUCCESS_MESSAGE.replace("!", " in %s!"), BOARD_STRING, boardName, teamName);

    }

}

