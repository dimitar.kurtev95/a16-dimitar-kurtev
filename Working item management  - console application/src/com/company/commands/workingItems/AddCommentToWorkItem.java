package com.company.commands.workingItems;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import com.company.models.contracts.WorkingItems;

import java.util.List;

import static com.company.commands.CommandConstants.*;
import static com.company.commands.member.CreateMember.MEMBER_STRING;

public class AddCommentToWorkItem extends CommandBaseImpl implements Command {

    public AddCommentToWorkItem(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {

        int id = Integer.parseInt(parameters.get(0));
        String memberName = parameters.get(1);
        StringBuilder comment = new StringBuilder();
        comment.append(parameters.get(2)); // Add only first word of the comment

        //adding the rest words of the comments, I did it because I avoid " " at the end of the sentence.
        for (int i = 4; i < parameters.size(); i++) {
            comment.append(" " + parameters.get(i));
        }

        return addCommentToWorkItem(id, memberName,comment.toString());
    }

    private String addCommentToWorkItem(int id, String memberName, String comment) {

        CommandHelper.checkRepositoryForItem(repository.getWorkingItems(), id, "Working Item");
        CommandHelper.checkRepositoryForItem(repository.getMembers(), memberName, MEMBER_STRING);

        WorkingItems workItem = repository.getWorkingItems().get(id);
        Member member = repository.getMembers().get(memberName);
        Board board =repository.getTeams()
                .values()
                .stream()
                .filter(team1 -> team1.getBoard().containsKey(workItem.getBoardName()))
                .findFirst()
                .orElse(null)
                .getBoard().get(workItem.getBoardName());

        workItem.addComment(comment);

        String commentAddedSuccessMessage = String.format(COMMENT_ADDED_SUCCESS_MESSAGE, comment, memberName, workItem.getBoardName(), CommandHelper.dateAndTime);

        workItem.addToActivityHistory(commentAddedSuccessMessage);
        member.addToActivityHistory(commentAddedSuccessMessage);
        board.addToActivityHistory(commentAddedSuccessMessage);

        return commentAddedSuccessMessage;
    }

}
