package com.company.commands.workingItems.feedback;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.models.contracts.Feedback;
import com.company.models.contracts.Member;

import java.util.List;

import static com.company.commands.member.CreateMember.MEMBER_STRING;
import static com.company.commands.workingItems.feedback.CreateFeedback.FEEDBACK_STRING;
import static com.company.commands.CommandConstants.*;

public class ChangeFeedbackRating extends CommandBaseImpl implements Command {

    private static final int CORRECT_NUMBER_OF_ARGS = 3;

    public ChangeFeedbackRating(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {
        CommandHelper.checkForNumberOfParameters(parameters, CORRECT_NUMBER_OF_ARGS);
        String memberName = parameters.get(0);
        int id = Integer.parseInt(parameters.get(1));
        int rating = Integer.parseInt(parameters.get(2));

        return changeFeedbackRating(memberName, id, rating);
    }

    private String changeFeedbackRating(String memberName, int id, int rating) {
        CommandHelper.checkRepositoryForItem(repository.getWorkingItems(), id, FEEDBACK_STRING);
        CommandHelper.checkRepositoryForItem(repository.getMembers(), memberName, MEMBER_STRING);

        //  Feedback feedback = (Feedback) repository.getWorkingItems().get(id);
        Feedback feedback = repository.getFeedback()
                .stream()
                .filter(feedback1 -> feedback1.getId() == id)
                .findFirst()
                .orElse(null);

        Member member = repository.getMembers().get(memberName);

        int oldRating = feedback.getRating();

        feedback.changeRating(rating);

        String ratingChangeMessage = String.format(FEEDBACK_RATING_CHANGE_MESSAGE, oldRating, rating, memberName, CommandHelper.dateAndTime);

        feedback.addToActivityHistory(ratingChangeMessage);
        member.addToActivityHistory(ratingChangeMessage);
        // repository.getBoards(feedback.getTeamName()).get(feedback.getBoardName()).addToActivityHistory(ratingChangeMessage);
        repository.getFeedback()
                .stream()
                .filter(feedback1 -> feedback1.getId() == id)
                .forEach(bug1 -> bug1.addToActivityHistory(ratingChangeMessage));


        return ratingChangeMessage;
    }
}
