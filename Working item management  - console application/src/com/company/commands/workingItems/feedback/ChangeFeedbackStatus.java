package com.company.commands.workingItems.feedback;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.models.common.FeedbackStatusType;
import com.company.models.contracts.Feedback;
import com.company.models.contracts.Member;

import java.util.List;

import static com.company.commands.CommandConstants.*;
import static com.company.commands.workingItems.feedback.CreateFeedback.FEEDBACK_STRING;
import static com.company.commands.member.CreateMember.MEMBER_STRING;

public class ChangeFeedbackStatus extends CommandBaseImpl implements Command {

    private static final int CORRECT_NUMBER_OF_ARGS = 3;

    public ChangeFeedbackStatus(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {
        CommandHelper.checkForNumberOfParameters(parameters, CORRECT_NUMBER_OF_ARGS);

        String memberName = parameters.get(0);
        int id = Integer.parseInt(parameters.get(1));
        FeedbackStatusType status = FeedbackStatusType.valueOf(parameters.get(2).toUpperCase());

        return changeFeedbackStatus(memberName, id, status);
    }

    private String changeFeedbackStatus(String memberName, int id, FeedbackStatusType status) {
        CommandHelper.checkRepositoryForItem(repository.getWorkingItems(), id, FEEDBACK_STRING);
        CommandHelper.checkRepositoryForItem(repository.getMembers(), memberName, MEMBER_STRING);

       // Feedback feedback = (Feedback) repository.getWorkingItems().get(id);
        Feedback feedback=repository.getFeedback()
                .stream()
                .filter(feedback1-> feedback1.getId() == id)
                .findFirst()
                .orElse(null);
        Member member = repository.getMembers().get(memberName);

        String oldStatus = feedback.getStatus().toString();

        feedback.setStatus(status);

        String feedbackStatusChangeMessage = String.format(STATUS_CHANGE_MESSAGE, FEEDBACK_STRING, feedback.getId(), oldStatus, status, memberName, CommandHelper.dateAndTime);

        feedback.addToActivityHistory(feedbackStatusChangeMessage);
        member.addToActivityHistory(feedbackStatusChangeMessage);
       // repository.getBoards(feedback.getTeamName()).get(feedback.getBoardName()).addToActivityHistory(feedbackStatusChangeMessage);
        repository.getFeedback()
                .stream()
                .filter(feedback1 -> feedback1.getId() == id)
                .forEach(bug1 -> bug1.addToActivityHistory(feedbackStatusChangeMessage));

        return feedbackStatusChangeMessage;
    }
}
