package com.company.commands.workingItems.feedback;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.models.contracts.Board;
import com.company.models.contracts.Feedback;
import com.company.models.contracts.Member;

import java.util.List;

import static com.company.commands.CommandConstants.*;
import static com.company.commands.board.CreateBoard.BOARD_STRING;
import static com.company.commands.team.CreateTeam.TEAM_STRING;

public class CreateFeedback extends CommandBaseImpl implements Command {
    public static final String FEEDBACK_STRING = "Feedback";
    private static final int CORRECT_NUMBER_OF_ARGS = 6;

    public CreateFeedback(Repository repository, Factory factory) {
        super(repository, factory);
    }


    public String execute(List<String> parameters) {
        CommandHelper.checkForNumberOfParameters(parameters, CORRECT_NUMBER_OF_ARGS);

        String teamName = parameters.get(0);
        String memberName = parameters.get(1);
        String boardName = parameters.get(2);
        String title = parameters.get(3);
        String description = parameters.get(4);
        int rating = Integer.parseInt(parameters.get(5));


        return createFeedback(teamName, memberName, boardName, title, description, rating);
    }

    private String createFeedback(String teamName, String memberName, String boardName, String title, String description, int rating) {

        CommandHelper.checkRepositoryForItem(repository.getTeams(), teamName, TEAM_STRING);
        CommandHelper.checkRepositoryForItem(repository.getBoards(teamName), boardName, BOARD_STRING);
        CommandHelper.checkIfMemberIsInTeam(repository.getTeams().get(teamName).getMember(), repository.getMembers().get(memberName), memberName, teamName);

        // Creating workingItem
        Member member = repository.getMembers().get(memberName);
        Feedback feedback = factory.CreateFeedback(title, description, rating);
        Board board = repository.getBoards(teamName).get(boardName);

        String feedbackCreatedMessage = String.format(ITEM_WITH_ID_CREATED_SUCCESS_MESSAGE, FEEDBACK_STRING, title, feedback.getId(), CommandHelper.dateAndTime);
        String feedbackAddedToWorkingItems = String.format(ITEM_ADDED_TO_WORKING_ITEMS, FEEDBACK_STRING, title, feedback.getId(), teamName, CommandHelper.dateAndTime);

        //add to activity history
        feedback.addToActivityHistory(feedbackCreatedMessage);
        feedback.addToActivityHistory(feedbackAddedToWorkingItems);

        //setting name
        feedback.setBoardName(boardName);
        feedback.setTeamName(teamName);

        member.addToActivityHistory(feedbackCreatedMessage);
        member.addToActivityHistory(feedbackAddedToWorkingItems);

        board.addToActivityHistory(feedbackCreatedMessage);
        board.addToActivityHistory(feedbackAddedToWorkingItems);

        //adding to repository
        repository.addFeedback(feedback);
        board.addWorkingItem(feedback);

        return feedbackCreatedMessage;
    }
}
