package com.company.commands.workingItems.story;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.models.common.SizeType;
import com.company.models.contracts.Member;
import com.company.models.contracts.Story;

import java.util.List;

import static com.company.commands.CommandConstants.*;
import static com.company.commands.member.CreateMember.MEMBER_STRING;
import static com.company.commands.workingItems.story.CreateStory.STORY_STRING;

public class ChangeStorySize extends CommandBaseImpl implements Command {
    private static final int CORRECT_NUMBER_OF_ARGS = 3;

    public ChangeStorySize(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {
        CommandHelper.checkForNumberOfParameters(parameters, CORRECT_NUMBER_OF_ARGS);

        String memberName = parameters.get(0);
        int id = Integer.parseInt(parameters.get(1));
        SizeType size = SizeType.valueOf(parameters.get(2).toUpperCase());

        return changeStorySize(memberName, id, size);
    }

    private String changeStorySize(String memberName, int id, SizeType size) {
        CommandHelper.checkRepositoryForItem(repository.getWorkingItems(), id, STORY_STRING);
        CommandHelper.checkRepositoryForItem(repository.getMembers(), memberName, MEMBER_STRING);

        //Story story = (Story) repository.getWorkingItems().get(id);
        Story story=repository.getStory()
                .stream()
                .filter(story1-> story1.getId() == id)
                .findFirst()
                .orElse(null);

        Member member = repository.getMembers().get(memberName);

        String oldSize = story.getSizeType().toString();
        story.changeSize(size);

        String storySizeChangeMessage = String.format(SIZE_CHANGE_MESSAGE, STORY_STRING, story.getId(), oldSize, size, memberName, CommandHelper.dateAndTime);

        story.addToActivityHistory(storySizeChangeMessage);
        member.addToActivityHistory(storySizeChangeMessage);
       // repository.getBoards(story.getTeamName()).get(story.getBoardName()).addToActivityHistory(storySizeChangeMessage);
        repository.getStory()
                .stream()
                .filter(story1-> story1.getId() == id)
                .forEach(bug1 -> bug1.addToActivityHistory(storySizeChangeMessage));


        return storySizeChangeMessage;
    }
}
