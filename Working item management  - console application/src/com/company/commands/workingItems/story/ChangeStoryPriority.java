package com.company.commands.workingItems.story;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.models.common.PriorityType;
import com.company.models.contracts.Member;
import com.company.models.contracts.Story;

import java.util.List;

import static com.company.commands.CommandConstants.*;
import static com.company.commands.workingItems.story.CreateStory.STORY_STRING;
import static com.company.commands.member.CreateMember.MEMBER_STRING;

public class ChangeStoryPriority extends CommandBaseImpl implements Command {
    private static final int CORRECT_NUMBER_OF_ARGS = 3;

    public ChangeStoryPriority(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {
        CommandHelper.checkForNumberOfParameters(parameters, CORRECT_NUMBER_OF_ARGS);

        String memberName = parameters.get(0);
        int id = Integer.parseInt(parameters.get(1));
        PriorityType priority = PriorityType.valueOf(parameters.get(2).toUpperCase());

        return changeStoryPriority(memberName, id, priority);
    }

    private String changeStoryPriority(String memberName, int id, PriorityType priority) {
        CommandHelper.checkRepositoryForItem(repository.getWorkingItems(), id, STORY_STRING);
        CommandHelper.checkRepositoryForItem(repository.getMembers(), memberName, MEMBER_STRING);

       // Story story = (Story) repository.getWorkingItems().get(id);
        Story story=repository.getStory()
                .stream()
                .filter(story1-> story1.getId() == id)
                .findFirst()
                .orElse(null);

        Member member = repository.getMembers().get(memberName);
        String oldPriority = story.getPriorityType().toString();
        story.changePriority(priority);

        String storyPriorityChangeMessage = String.format(PRIORITY_CHANGE_MESSAGE, STORY_STRING,id, oldPriority, priority, memberName, CommandHelper.dateAndTime);

        story.addToActivityHistory(storyPriorityChangeMessage);
        member.addToActivityHistory(storyPriorityChangeMessage);
        //repository.getBoards(story.getTeamName()).get(story.getBoardName()).addToActivityHistory(storyPriorityChangeMessage);

        repository.getStory()
                .stream()
                .filter(story1-> story1.getId() == id)
                .forEach(bug1 -> bug1.addToActivityHistory(storyPriorityChangeMessage));

        return storyPriorityChangeMessage;
    }
}
