package com.company.commands.workingItems.story;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.models.common.StoryStatusType;
import com.company.models.contracts.Member;
import com.company.models.contracts.Story;

import java.util.List;

import static com.company.commands.CommandConstants.*;
import static com.company.commands.member.CreateMember.MEMBER_STRING;
import static com.company.commands.workingItems.story.CreateStory.STORY_STRING;

public class ChangeStoryStatus extends CommandBaseImpl implements Command {
    private static final int CORRECT_NUMBER_OF_ARGS = 3;

    public ChangeStoryStatus(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {
        CommandHelper.checkForNumberOfParameters(parameters, CORRECT_NUMBER_OF_ARGS);

        String memberName = parameters.get(0);
        int id = Integer.parseInt(parameters.get(1));
        StoryStatusType status = StoryStatusType.valueOf(parameters.get(2).toUpperCase());

        return changeStoryStatus(memberName, id, status);
    }

    private String changeStoryStatus(String memberName, int id, StoryStatusType status) {
        CommandHelper.checkRepositoryForItem(repository.getWorkingItems(), id, STORY_STRING);
        CommandHelper.checkRepositoryForItem(repository.getMembers(), memberName, MEMBER_STRING);

       // Story story = (Story) repository.getWorkingItems().get(id);
        Story story=repository.getStory()
                .stream()
                .filter(story1-> story1.getId() == id)
                .findFirst()
                .orElse(null);

        Member member = repository.getMembers().get(memberName);

        String oldStatus = story.getStatus().toString();

        story.setStatus(status);

        String statusChangeMessage = String.format(STATUS_CHANGE_MESSAGE, STORY_STRING, story.getId(), oldStatus, status, memberName, CommandHelper.dateAndTime);

        story.addToActivityHistory(statusChangeMessage);
        member.addToActivityHistory(statusChangeMessage);
        //repository.getBoards(story.getTeamName()).get(story.getBoardName()).addToActivityHistory(statusChangeMessage);

        repository.getStory()
                .stream()
                .filter(story1-> story1.getId() == id)
                .forEach(bug1 -> bug1.addToActivityHistory(statusChangeMessage));

        return statusChangeMessage;
    }
}
