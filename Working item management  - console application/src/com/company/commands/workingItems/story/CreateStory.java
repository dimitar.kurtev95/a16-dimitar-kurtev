package com.company.commands.workingItems.story;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Story;

import java.util.List;

import static com.company.commands.CommandConstants.*;
import static com.company.commands.team.CreateTeam.TEAM_STRING;
import static com.company.commands.board.CreateBoard.BOARD_STRING;

public class CreateStory extends CommandBaseImpl implements Command {
    public static final String STORY_STRING = "Story";
    private static final int CORRECT_NUMBER_OF_ARGS = 7;

    public CreateStory(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {
        CommandHelper.checkForNumberOfParameters(parameters, CORRECT_NUMBER_OF_ARGS);

        String teamName = parameters.get(0);
        String memberName = parameters.get(1);
        String boardName = parameters.get(2);
        String title = parameters.get(3);
        String description = parameters.get(4);
        String priority = parameters.get(5);
        String size = parameters.get(6);

        return createStory(teamName, memberName, boardName, title, description, priority, size);
    }

    private String createStory(String teamName, String memberName, String boardName, String title, String description, String priority, String size) {
        CommandHelper.checkRepositoryForItem(repository.getTeams(), teamName, TEAM_STRING);
        CommandHelper.checkRepositoryForItem(repository.getBoards(teamName), boardName, BOARD_STRING);
        CommandHelper.checkIfMemberIsInTeam(repository.getTeams().get(teamName).getMember(), repository.getMembers().get(memberName), memberName, teamName);

        // Creating story
        Member member = repository.getMembers().get(memberName);
        Story story = factory.CreateStory(title, description, priority, size);
        Board board = repository.getBoards(teamName).get(boardName);

        String storyCreatedMessage = String.format(ITEM_WITH_ID_CREATED_SUCCESS_MESSAGE, STORY_STRING, title, story.getId(), CommandHelper.dateAndTime);
        String storyAddedToWorkingItems = String.format(ITEM_ADDED_TO_WORKING_ITEMS, STORY_STRING, title, story.getId(), teamName, CommandHelper.dateAndTime);
        //adding to activity history
        story.addToActivityHistory(storyCreatedMessage);
        story.addToActivityHistory(storyAddedToWorkingItems);
        //setting name
        story.setBoardName(boardName);
        story.setTeamName(teamName);

        member.addToActivityHistory(storyCreatedMessage);
        member.addToActivityHistory(storyAddedToWorkingItems);

        board.addToActivityHistory(storyCreatedMessage);
        board.addToActivityHistory(storyAddedToWorkingItems);

        //adding to repository
        board.addWorkingItem(story);
        repository.addStory(story);
        return storyCreatedMessage;
    }
}
