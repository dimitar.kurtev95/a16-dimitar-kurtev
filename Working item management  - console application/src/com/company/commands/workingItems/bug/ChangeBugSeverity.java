package com.company.commands.workingItems.bug;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.models.common.SeverityType;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Member;

import java.util.List;

import static com.company.commands.CommandConstants.*;
import static com.company.commands.member.CreateMember.MEMBER_STRING;
import static com.company.commands.workingItems.bug.CreateBug.*;


public class ChangeBugSeverity extends CommandBaseImpl implements Command {
    private static final int CORRECT_NUMBER_OF_ARGS = 3;

    public ChangeBugSeverity(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {
        CommandHelper.checkForNumberOfParameters(parameters, CORRECT_NUMBER_OF_ARGS);


        String memberName = parameters.get(0);
        int id = Integer.parseInt(parameters.get(1));
        SeverityType severity = SeverityType.valueOf(parameters.get(2).toUpperCase());

        return changeBugSeverity(memberName, id, severity);
    }

    private String changeBugSeverity(String memberName, int id, SeverityType severity) {
        CommandHelper.checkRepositoryForItem(repository.getWorkingItems(), id, BUG_STRING);
        CommandHelper.checkRepositoryForItem(repository.getMembers(), memberName, MEMBER_STRING);

   //     Bug bug = (Bug) repository.getWorkingItems().get(id);
        Bug bug=repository.getBugs()
                .stream()
                .filter(bug1 -> bug1.getId() == id)
                .findFirst()
                .orElse(null);

        Member member = repository.getMembers().get(memberName);

        String oldSeverity = bug.getSeverityType().toString();

        bug.changeSeverity(severity);

        String bugSeverityChangeMessage = String.format(BUG_SEVERITY_CHANGE_MESSAGE, BUG_STRING, BUG_STRING, bug.getId(), oldSeverity, severity, memberName, CommandHelper.dateAndTime);
        bug.addToActivityHistory(bugSeverityChangeMessage);
        member.addToActivityHistory(bugSeverityChangeMessage);
        //repository.getBoards(bug.getTeamName()).get(bug.getBoardName()).addToActivityHistory(bugSeverityChangeMessage);
        repository.getBugs()
                .stream()
                .filter(bug1 -> bug1.getId() == id)
                .forEach(bug1 -> bug1.addToActivityHistory(bugSeverityChangeMessage));

        return bugSeverityChangeMessage;
    }
}
