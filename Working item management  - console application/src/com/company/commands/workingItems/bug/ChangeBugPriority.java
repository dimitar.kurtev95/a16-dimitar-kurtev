package com.company.commands.workingItems.bug;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.models.common.PriorityType;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Member;

import java.util.List;

import static com.company.commands.CommandConstants.*;
import static com.company.commands.workingItems.bug.CreateBug.*;

public class ChangeBugPriority extends CommandBaseImpl implements Command {
    private static final int CORRECT_NUMBER_OF_ARGS = 3;

    public ChangeBugPriority(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {
        CommandHelper.checkForNumberOfParameters(parameters, CORRECT_NUMBER_OF_ARGS);

        String memberName = parameters.get(0);
        int id = Integer.parseInt(parameters.get(1));
        PriorityType priority = PriorityType.valueOf(parameters.get(2).toUpperCase());

        return changeBugPriority(memberName, id, priority);
    }

    private String changeBugPriority(String memberName, int id, PriorityType priority) {
        CommandHelper.checkRepositoryForItem(repository.getWorkingItems(), id, BUG_STRING);

 //   Bug bug = (Bug) repository.getWorkingItems().get(id);
        Bug bug=repository.getBugs()
                .stream()
                .filter(bug1 -> bug1.getId() == id)
                .findFirst()
                .orElse(null);


        Member member = repository.getMembers().get(memberName);

        String oldPriority = bug.getPriorityType().toString();

        bug.changePriority(priority);

        String bugPriorityChangeMessage = String.format(PRIORITY_CHANGE_MESSAGE, BUG_STRING, id,oldPriority, priority, memberName, CommandHelper.dateAndTime);

        bug.addToActivityHistory(bugPriorityChangeMessage);
        member.addToActivityHistory(bugPriorityChangeMessage);
   //     repository.getBoards(bug.getTeamName()).get(bug.getBoardName()).addToActivityHistory(bugPriorityChangeMessage);
        repository.getBugs()
                .stream()
                .filter(bug1 -> bug1.getId() == id)
                .forEach(bug1 -> bug1.addToActivityHistory(bugPriorityChangeMessage));

        return bugPriorityChangeMessage;

    }
}
