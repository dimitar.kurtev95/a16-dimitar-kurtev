package com.company.commands.workingItems.bug;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.models.common.BugStatusType;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Member;

import java.util.List;

import static com.company.commands.CommandConstants.*;
import static com.company.commands.workingItems.bug.CreateBug.*;
import static com.company.commands.member.CreateMember.MEMBER_STRING;

public class ChangeBugStatus extends CommandBaseImpl implements Command {
    private static final int CORRECT_NUMBER_OF_ARGS = 3;

    public ChangeBugStatus(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {
        CommandHelper.checkForNumberOfParameters(parameters, CORRECT_NUMBER_OF_ARGS);

        String memberName = parameters.get(0);
        int id = Integer.parseInt(parameters.get(1));
        BugStatusType status = BugStatusType.valueOf(parameters.get(2).toUpperCase());

        return changeBugPriority(memberName, id, status);
    }

    private String changeBugPriority(String memberName, int id, BugStatusType status) {
        // Check if bug exists
        CommandHelper.checkRepositoryForItem(repository.getWorkingItems(), id, BUG_STRING);
        CommandHelper.checkRepositoryForItem(repository.getMembers(), memberName, MEMBER_STRING);

        // Retrieve bug from repository
        Bug bug=repository.getBugs()
                .stream()
                .filter(bug1 -> bug1.getId() == id)
                .findFirst()
                .orElse(null);

        // Bug bug = (Bug) repository.getWorkingItems().get(id);
        Member member = repository.getMembers().get(memberName);

        // Save old status
        String oldStatus = bug.getStatus().toString();

        // Change bug status
        bug.setStatus(status);

        String bugStatusChangeMessage = String.format(STATUS_CHANGE_MESSAGE ,BUG_STRING, bug.getId(), oldStatus, status, memberName, CommandHelper.dateAndTime);

        // Add status change to member history
        bug.addToActivityHistory(bugStatusChangeMessage);
        member.addToActivityHistory(bugStatusChangeMessage);
   //     repository.getBoards(bug.getTeamName()).get(bug.getBoardName()).addToActivityHistory(bugStatusChangeMessage);
        repository.getBugs()
                .stream()
                .filter(bug1 -> bug1.getId() == id)
                .forEach(bug1 -> bug1.addToActivityHistory(bugStatusChangeMessage));


        return bugStatusChangeMessage;

    }
}
