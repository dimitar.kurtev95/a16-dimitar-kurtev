package com.company.commands.workingItems.bug;

import com.company.commands.*;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.models.contracts.Board;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Member;

import java.util.List;

import static com.company.commands.CommandConstants.*;
import static com.company.commands.team.CreateTeam.TEAM_STRING;
import static com.company.commands.board.CreateBoard.BOARD_STRING;

public class CreateBug extends CommandBaseImpl implements Command {
    public static final String BUG_STRING = "Bug";
    private static final int CORRECT_NUMBER_OF_ARGS = 7;

    public CreateBug(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {
        CommandHelper.checkForNumberOfParameters(parameters, CORRECT_NUMBER_OF_ARGS);

        String teamName = parameters.get(0);
        String memberName = parameters.get(1);
        String boardName = parameters.get(2);
        String bugTitle = parameters.get(3);
        String bugDescription = parameters.get(4);
        String bugPriorityType = parameters.get(5);
        String bugSeverityType = parameters.get(6);

        return createBug(teamName, memberName, boardName, bugTitle, bugDescription, bugPriorityType, bugSeverityType);
    }

    private String createBug(String teamName, String memberName, String boardName, String title, String description, String priorityType, String severityType) {

        CommandHelper.checkRepositoryForItem(repository.getTeams(), teamName, TEAM_STRING);
        CommandHelper.checkRepositoryForItem(repository.getBoards(teamName), boardName, BOARD_STRING);
        //checking for existing member in team
        CommandHelper.checkIfMemberIsInTeam(repository.getTeams().get(teamName).getMember(), repository.getMembers().get(memberName), memberName, teamName);


        // Creating Bug
        Member member = repository.getMembers().get(memberName);
        Bug bug = factory.CreateBug(title, description, priorityType, severityType);
        Board board = repository.getBoards(teamName).get(boardName);

        String bugCreatedMessage = String.format(ITEM_WITH_ID_CREATED_SUCCESS_MESSAGE, BUG_STRING, title, bug.getId(), CommandHelper.dateAndTime);
        String bugAddedToWorkingItems = String.format(ITEM_ADDED_TO_WORKING_ITEMS, BUG_STRING, title, bug.getId(), teamName, CommandHelper.dateAndTime);

        //adding to activity
        bug.addToActivityHistory(bugCreatedMessage);
        bug.addToActivityHistory(bugAddedToWorkingItems);
        //setting name
        bug.setBoardName(boardName);
        bug.setTeamName(teamName);

        member.addToActivityHistory(bugCreatedMessage);
        member.addToActivityHistory(bugAddedToWorkingItems);

        board.addToActivityHistory(bugCreatedMessage);
        board.addToActivityHistory(bugAddedToWorkingItems);

        //adding to repository
        repository.addBug(bug); //to list
        board.addWorkingItem(bug);

        return bugCreatedMessage;
    }


}