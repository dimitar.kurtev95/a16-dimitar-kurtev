package com.company.commands.team;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;

import java.util.List;

import static com.company.commands.team.CreateTeam.TEAM_STRING;

public class ShowAllTeamBoards extends CommandBaseImpl implements Command {
    private static final int CORRECT_NUMBER_OF_ARGS = 1;

    public ShowAllTeamBoards(Repository repository, Factory factory) {
        super(repository, factory);
    }


    @Override
    public String execute(List<String> parameters) {
        CommandHelper.checkForNumberOfParameters(parameters, CORRECT_NUMBER_OF_ARGS);
        String teamName = parameters.get(0);
        return showAllTeamBoards(teamName);

    }

    private String showAllTeamBoards(String teamName) {
        CommandHelper.checkRepositoryForItem(repository.getTeams(), teamName, TEAM_STRING);
        CommandHelper.checkForEmptyMap(repository.getBoards(teamName));

        StringBuilder print = new StringBuilder(String.format("Team %s has the following boards:" + System.lineSeparator(), teamName));

        repository.getBoards(teamName).forEach((s, board) -> print.append(board.getName()).append(System.lineSeparator()));

        return print.toString();
    }
}