package com.company.commands.team;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;

import java.util.List;

import static com.company.commands.CommandConstants.*;

public class ShowAllTeams extends CommandBaseImpl implements Command {

    public ShowAllTeams(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {
        CommandHelper.checkForEmptyMap(repository.getTeams());

        StringBuilder print = new StringBuilder("All teams in the application are:" + System.lineSeparator());
        repository.getTeams()
                .values()
                .forEach(team -> print.append(team.toString() + System.lineSeparator()));

        return print.toString();
    }
}
