package com.company.commands.team;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.models.contracts.Member;

import java.util.List;

import static com.company.commands.CommandConstants.*;
import static com.company.commands.team.CreateTeam.TEAM_STRING;
import static com.company.commands.member.CreateMember.MEMBER_STRING;

public class AddMemberToTeam extends CommandBaseImpl implements Command {

    private static final int CORRECT_NUMBER_OF_ARGS = 2;

    public AddMemberToTeam(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {
        CommandHelper.checkForNumberOfParameters(parameters, CORRECT_NUMBER_OF_ARGS);
        String memberName = parameters.get(0);
        String teamName = parameters.get(1);

        return addMemberToTeam(memberName, teamName);
    }

    private String addMemberToTeam(String memberName, String teamName) {

        CommandHelper.checkRepositoryForItem(repository.getTeams(), teamName, TEAM_STRING);
        CommandHelper.checkRepositoryForItem(repository.getMembers(), memberName, MEMBER_STRING);

        Member member = repository.getMembers().get(memberName);

        String memberAddedToTeamMessage = String.format(ADD_MEMBER_TO_TEAM_SUCCESS_MESSAGE, memberName, teamName, CommandHelper.dateAndTime);
        member.addToActivityHistory(memberAddedToTeamMessage);

        repository.getTeams().get(teamName).addMember(member);

        return memberAddedToTeamMessage;
    }

}
