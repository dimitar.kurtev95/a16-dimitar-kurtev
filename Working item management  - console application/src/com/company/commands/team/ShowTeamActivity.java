package com.company.commands.team;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;

import java.util.List;

import static com.company.commands.team.CreateTeam.TEAM_STRING;

public class ShowTeamActivity extends CommandBaseImpl implements Command {
    private static final int CORRECT_NUMBER_OF_ARGS = 1;

    public ShowTeamActivity(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {
        CommandHelper.checkForNumberOfParameters(parameters, CORRECT_NUMBER_OF_ARGS);
        String teamName = parameters.get(0);
        return showTeamActivity(teamName);

    }

    private String showTeamActivity(String teamName) {
        CommandHelper.checkRepositoryForItem(repository.getTeams(), teamName, TEAM_STRING);
        StringBuilder print = new StringBuilder(String.format("Team %s activity:" + System.lineSeparator(), teamName));

        CommandHelper.checkForEmptyList(repository.getTeams().get(teamName).getTeamActivityHistory(), print);

        print.append(System.lineSeparator());
        return print.toString();

    }
}

