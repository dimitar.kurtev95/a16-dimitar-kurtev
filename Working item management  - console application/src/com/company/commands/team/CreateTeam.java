package com.company.commands.team;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.models.contracts.Team;


import java.util.List;

import static com.company.commands.CommandConstants.*;

public class CreateTeam extends CommandBaseImpl implements Command {
    public static final String TEAM_STRING = "Team";
    private static final int CORRECT_NUMBER_OF_ARGS = 1;

    public CreateTeam(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {
        CommandHelper.checkForNumberOfParameters(parameters, CORRECT_NUMBER_OF_ARGS);
        String teamName = parameters.get(0);

        return createTeam(teamName);
    }

    private String createTeam(String name) {
        CommandHelper.checkRepositoryForExisting(repository.getTeams(), name, TEAM_STRING);

        Team team = factory.CreateTeam(name);
        repository.addTeam(name, team);

        return String.format(ITEM_CREATED_SUCCESS_MESSAGE, TEAM_STRING, name, CommandHelper.dateAndTime);
    }
}
