package com.company.commands;

import com.company.models.contracts.Member;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.company.commands.CommandConstants.*;

public class CommandHelper {
    public static String dateAndTime = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(new Date());

    public static void checkForNumberOfParameters(List<String> parameters, int correctNumber) {
        if (parameters.size() != correctNumber) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, correctNumber, parameters.size()));
        }
    }

    public static <T> void checkRepositoryForExisting(Map<T, ?> map, T key, String type) {
        if (map.containsKey(key)) {
            throw new IllegalArgumentException(String.format(ITEM_EXISTS, type, key));
        }
    }

    public static <T> void checkRepositoryForItem(Map<T, ?> map, T key, String type) {
        if (!map.containsKey(key)) {
            throw new IllegalArgumentException(String.format(ITEM_NOT_FOUND, type, key));
        }
    }

    public static <T> void checkForEmptyList(List<T> list, StringBuilder print) {
        if (list.isEmpty()) {
            print.append(EMPTY_LIST);
        } else {
            list.forEach(T -> print.append(T.toString()).append(System.lineSeparator()));
        }
    }

    public static <T> void checkForEmptyMap(Map<T,?> map) {
        if (map.isEmpty()) {
           throw new IllegalArgumentException(EMPTY_LIST.replace("List","Map"));
        }
    }

    public static <T extends Member> void checkIfMemberIsInTeam(List<T> list, T object, String memberName, String teamName) {
        if (!list.contains(object)) {
            throw new IllegalArgumentException(String.format(ITEM_NOT_FOUND.replace(".", " in team %s"), "Member", memberName, teamName));
        }
    }
}