package com.company.commands.enums;

public enum CommandType {
    ADDCOMMENTTOWORKITEM, // {ID} {name} '{comment}'
    ADDMEMBERTOTEAM, // {Member name} {Team name}
    ASSIGNWORKITEMTOMEMBER, // {ID} {member} {team}
    CREATEBOARD, // {Board name} {Team name} {member name}
    CREATEMEMBER, // {Member name}
    SHOWBOARDACTIVITY, // {team name} {Board name}
    SHOWMEMBERS, // will display all members - no need of input
    SHOWMEMBERACTIVITY, // {Member name}
    UNASIGNWORKITEMTOMEMBER, // {WorkItem type} {WorkItem ID} {Member name}
    CREATETEAM, // {Team name}
    SHOWALLTEAMBOARDS, // {Team name}
    SHOWALLTEAMMEMBERS, // {Team name}
    SHOWALLTEAMS, // will display all teams - no need of input
    SHOWTEAMACTIVITY, // {Team name}
    CREATESTORY, // {team} {member} {board name} {title} {description} {priority} {size}
    CHANGESTORYPRIORITY, // {member} {id} {Priority type}
    CHANGESTORYSIZE, // {member} {id}  {Size type}
    CHANGESTORYSTATUS, // {member} {id} {Status type}
    CREATEFEEDBACK, //{team}{member} {board name} {title} {description} {rating}
    CHANGEFEEDBACKRATING, // {member} {id}  {Rating type}
    CHANGEFEEDBACKSTATUS, // {member} {id} {Status type}
    CREATEBUG, // {team} {member} {board name} {title} {description} {priority} {severity}
    CHANGEBUGPRIORITY, // {member} {id}  {Priority type}
    CHANGEBUGSEVERITY, // {member} {id} {Severity type}
    CHANGEBUGSTATUS, // {member} {id} {Status type}
    LISTALL,
    LISTALLWORKINGITEMSINTEAM,// {team}
    FILTERITEMBYTYPE, // filter  {team} {type}
    FILTERITEMBYSTATUSANDMEMBER, // filter {status} {member}
    SORTBYTITLE,
    SORTBUGSBYPRIORITY,
    SORTBUGSBYSEVERITY,
    SORTFEEDBACKBYRATING,
    SORTSTORYBYSIZE,
    SORTSTORYBYPRIORITY
}
