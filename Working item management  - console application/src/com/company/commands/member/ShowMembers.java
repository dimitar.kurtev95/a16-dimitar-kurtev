package com.company.commands.member;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;

import java.util.List;

public class ShowMembers extends CommandBaseImpl implements Command {

    public ShowMembers(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {
        CommandHelper.checkForEmptyMap(repository.getMembers());

        StringBuilder print = new StringBuilder("All members in application are:" + System.lineSeparator());

        repository.getMembers().forEach((String, Member)
                -> print.append(Member).append(System.lineSeparator()));

        return print.toString();
    }
}
