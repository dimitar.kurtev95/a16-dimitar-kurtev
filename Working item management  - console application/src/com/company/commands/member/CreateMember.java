package com.company.commands.member;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.models.contracts.Member;

import java.util.List;

import static com.company.commands.CommandConstants.*;

public class CreateMember extends CommandBaseImpl implements Command {
    public static final String MEMBER_STRING = "Member";
    private static final int CORRECT_NUMBER_OF_ARGS = 1;

    public CreateMember(Repository repository, Factory factory) {
        super(repository, factory);
    }


    @Override
    public String execute(List<String> parameters) {
        CommandHelper.checkForNumberOfParameters(parameters, CORRECT_NUMBER_OF_ARGS);

        String name = parameters.get(0);

        return createMember(name);
    }

    private String createMember(String name) {
        CommandHelper.checkRepositoryForExisting(repository.getMembers(), name, MEMBER_STRING);

        Member member = factory.CreateMember(name);

        String memberAddedMessage = String.format(ITEM_CREATED_SUCCESS_MESSAGE, MEMBER_STRING, member.getName(), CommandHelper.dateAndTime);

        member.addToActivityHistory(memberAddedMessage);

        repository.addMember(name, member);

        return memberAddedMessage;
    }
}
