package com.company.commands.member;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.models.contracts.AssignableWorkingItems;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.WorkingItems;

import java.util.List;

import static com.company.commands.CommandConstants.*;
import static com.company.commands.member.CreateMember.MEMBER_STRING;

public class UnassignWorkItemToMember extends CommandBaseImpl implements Command {

    private static final int CORRECT_NUMBER_OF_ARGS = 2;

    public UnassignWorkItemToMember(Repository repository, Factory factory) {
        super(repository, factory);
    }


    @Override
    public String execute(List<String> parameters) {
        CommandHelper.checkForNumberOfParameters(parameters, CORRECT_NUMBER_OF_ARGS);

        int id = Integer.parseInt(parameters.get(0));
        String memberName = parameters.get(1);

        return unAssignWorkItemToMember(id, memberName);
    }

    private String unAssignWorkItemToMember(int id, String memberName) {
        CommandHelper.checkRepositoryForItem(repository.getMembers(), memberName, MEMBER_STRING);
        CommandHelper.checkRepositoryForItem(repository.getAssignableWorkingItems(), id, "Working Item");

        Member member = repository.getMembers().get(memberName);
        AssignableWorkingItems workingItem = repository.getAssignableWorkingItems().get(id);
        Board board =repository.getTeams()
                .values()
                .stream()
                .filter(team1 -> team1.getBoard().containsKey(workingItem.getBoardName()))
                .findFirst()
                .orElse(null)
                .getBoard().get(workingItem.getBoardName());

        member.removeWorkingItem(id);
        workingItem.removeAssignee(memberName, member);

        String assigneeRemovedMessage = String.format(WORKING_ITEM_UNASSIGNED + " It was to %s", id, CommandHelper.dateAndTime, memberName);

        member.addToActivityHistory(String.format(assigneeRemovedMessage, id, CommandHelper.dateAndTime));
        workingItem.addToActivityHistory(assigneeRemovedMessage);
        board.addToActivityHistory(assigneeRemovedMessage);


        return assigneeRemovedMessage;
    }
}
