package com.company.commands.member;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;

import java.util.List;

import static com.company.commands.member.CreateMember.MEMBER_STRING;
import static com.company.commands.CommandConstants.*;

public class ShowMemberActivity extends CommandBaseImpl implements Command {
    private static final int CORRECT_NUMBER_OF_ARGS = 1;

    public ShowMemberActivity(Repository repository, Factory factory) {
        super(repository, factory);
    }


    @Override
    public String execute(List<String> parameters) {
        CommandHelper.checkForNumberOfParameters(parameters, CORRECT_NUMBER_OF_ARGS);

        String memberName = parameters.get(0);

        return showMemberActivity(memberName);
    }

    private String showMemberActivity(String memberName) {

        CommandHelper.checkRepositoryForItem(repository.getMembers(), memberName, MEMBER_STRING);

        StringBuilder print = new StringBuilder(String.format(ITEM_ACTIVITY_HISTORY + System.lineSeparator(), MEMBER_STRING, memberName));

        repository.getMembers().get(memberName).getActivityHistory().forEach(string -> print.append(string).append(System.lineSeparator()));

        return print.toString();
    }
}
