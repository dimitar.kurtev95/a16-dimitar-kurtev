package com.company.commands.member;

import com.company.commands.CommandBaseImpl;
import com.company.commands.CommandHelper;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.models.contracts.AssignableWorkingItems;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;

import java.util.List;

import static com.company.commands.CommandConstants.*;
import static com.company.commands.member.CreateMember.MEMBER_STRING;

public class AssignWorkItemToMember extends CommandBaseImpl implements Command {
    private static final int CORRECT_NUMBER_OF_ARGS = 2;

    public AssignWorkItemToMember(Repository repository, Factory factory) {
        super(repository, factory);
    }

    @Override
    public String execute(List<String> parameters) {
        CommandHelper.checkForNumberOfParameters(parameters, CORRECT_NUMBER_OF_ARGS);

        int id = Integer.parseInt(parameters.get(0));
        String memberName = parameters.get(1);

        return assignWorkItemToMember(id, memberName);
    }

    private String assignWorkItemToMember(int id, String memberName) {

        CommandHelper.checkRepositoryForItem(repository.getMembers(), memberName, MEMBER_STRING);
        CommandHelper.checkRepositoryForItem(repository.getAssignableWorkingItems(), id, "Working Item");

        Member member = repository.getMembers().get(memberName);
        AssignableWorkingItems workingItem = repository.getAssignableWorkingItems().get(id);
        Board board =repository.getTeams()
                .values()
                .stream()
                .filter(team1 -> team1.getBoard().containsKey(workingItem.getBoardName()))
                .findFirst()
                .orElse(null)
                .getBoard().get(workingItem.getBoardName());

        workingItem.addAssignee(memberName, member);

        String memberAssignedMessage = String.format(ASSIGN_WORK_ITEM_TO_MEMBER_SUCCESS_MESSAGE, String.valueOf(id), member, CommandHelper.dateAndTime);

        member.addToActivityHistory(memberAssignedMessage);
        workingItem.addToActivityHistory(memberAssignedMessage);
        board.addToActivityHistory(memberAssignedMessage);

        return memberAssignedMessage;
    }
}
