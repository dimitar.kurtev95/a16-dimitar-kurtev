package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;

import java.util.List;


public abstract class CommandBaseImpl implements Command {

    protected final Repository repository;
    protected final Factory factory;

    public CommandBaseImpl(Repository repository, Factory factory) {
        this.repository = repository;
        this.factory = factory;
    }

    @Override
    public abstract String execute(List<String> parameters);
}
