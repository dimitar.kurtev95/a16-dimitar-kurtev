package com.company;

import com.company.core.providers.EngineImpl;

import java.io.ByteArrayInputStream;

public class Main {

    private static void testInput() {
        String input = "Createteam team1 \n" +
                "Createteam team2\n" +
                "Createmember pesho \n" +
                "Createmember vanko\n" +
                "Createmember ivan \n" +
                "Createmember stanimir \n" +
                "Createmember vlado \n" +
                "Createmember pesho \n" +
                "Createmember ivan \n" +
                "Createmember stanimir \n" +
                "Createmember vlado \n" +
                "Addmembertoteam pesho team1\n" +
                "Addmembertoteam pesho team1\n" +
                "Addmembertoteam ivan team1\n" +
                "Addmembertoteam stanimir team2\n" +
                "Addmembertoteam vlado team2\n" +
                "Addmembertoteam vanko team1 \n" +
                "Createboard board1 team1 pesho\n" +
                "Createboard board1 team2 stanimir\n" +
                "Createboard board3 team1 pesho\n" +
                "Createboard board3 team2 stanimir\n" +
                "Createboard board3 team1 vanko\n" +
                "Createbug team1 pesho board1 bug1asssaassasa description234565346463 low minor\n" +
                "Createbug team1 stanimir board1 bug1asssaassasa description234565346463 low minor\n" +
                "Createbug team1 ivan board1 bug1asssaasssdds description234565346463 medium major\n" +
                "Createbug team1 stanimir board1 bug1asssaasssddswda description234565346463 low minor\n" +
                "Createbug team1 pesho board1 bug1asssaasssddswda description234565346463 high minor\n" +
                "createfeedback team1 vanko board1 titleofthefeedback descriptionofthefeedback 5\n" +
                "createfeedback team1 vanko board1 bestfeedback descriptionofthefeedback 9\n" +
                "createbug team1 vanko board1 titleofthebugis descriptionofthebug high critical\n" +
                "createstory team2 pesho board3 titleofthestory descriptionofthestory high medium\n" +
                "createstory team2 pesho board3 titleofthestory descriptionofthestory low large\n" +
                "createstory team1 pesho board3 titleofthestorys descriptionofthestorys high large\n" +
                "CHANGEBUGPRIORITY pesho 1 high\n" +
                "CHANGESTORYSIZE pesho 8 small\n" +
                "CHANGESTORYSTATUS pesho 8 DONE\n" +
                "ADDCOMMENTTOWORKITEM 3 pesho This was fixed by me\n" +
                "ASSIGNWORKITEMTOMEMBER 1 pesho\n" +
                //    "UNASIGNWORKITEMTOMEMBER 1 pesho\n" +
                "Showallteams\n" +
                "Showmembers\n" +
                "Showallteamboards team1\n" +
                "Showallteammembers team1\n" +
                "showmemberactivity vanko\n" +
                "showboardactivity team1 board1\n" +
                "sortbytitle\n" +
                "filteritembystatusandmember filter active pesho\n" +
                "filteritembytype filter team1 bug\n" +
                "listallworkingitemsinteam team1\n" +
//                "sortbugsbypriority\n" +
//                "sortbugsbyseverity\n" +
//                "sortfeedbackbyrating\n" +
//                "sortstorybysize\n" +
//                "sortstorybypriority\n" +
//                "listall\n" +
                "exit";
        System.setIn(new ByteArrayInputStream(input.getBytes()));
    }

    public static void main(String[] args) {
//        testInput();
        EngineImpl engine = new EngineImpl();
        engine.start();
    }
}
