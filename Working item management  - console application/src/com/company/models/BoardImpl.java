package com.company.models;

import com.company.models.contracts.Board;
import com.company.models.contracts.WorkingItems;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BoardImpl implements Board {
    private static final int MIN_NAME_LENGTH = 5;
    private static final int MAX_NAME_LENGTH = 10;
    private static final String INVALID_BOARD_NAME_MESSAGE = String.format("Board name must be between %d and %d characters long.", MIN_NAME_LENGTH, MAX_NAME_LENGTH);

    private String name;
    private Map<Integer, WorkingItems> workingItems;
    private List<String> activityHistory;

    public BoardImpl(String name) {
        setName(name);
        workingItems = new HashMap<>();
        activityHistory = new ArrayList<>();
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public Map<Integer, WorkingItems> getWorkingItems() {
        return new HashMap<>(workingItems);
    }

    @Override
    public List<String> getActivityHistory() {
        return new ArrayList<>(activityHistory);
    }

    @Override
    public void addToActivityHistory(String message) {
        ValidationHelperChecker.checkForNull(message);
        activityHistory.add(message);
    }

    @Override
    public void addWorkingItem(WorkingItems workingItem) {
        ValidationHelperChecker.checkForNull(workingItem);
        workingItems.put(workingItem.getId(), workingItem);
    }

    private void setName(String name) {
        this.name = ValidationHelperChecker.stringLengthCheck(name, MIN_NAME_LENGTH, MAX_NAME_LENGTH, INVALID_BOARD_NAME_MESSAGE);
    }

    @Override
    public String toString() {
        return String.format("Board with name %s" + System.lineSeparator(), getName());
    }
}
