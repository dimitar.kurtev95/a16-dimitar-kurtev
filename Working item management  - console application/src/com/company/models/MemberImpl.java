package com.company.models;

import com.company.models.contracts.Member;
import com.company.models.contracts.WorkingItems;

import java.util.ArrayList;
import java.util.List;

public class MemberImpl implements Member {

    private static final int MIN_NAME_LENGTH = 5;
    private static final int MAX_NAME_LENGTH = 15;
    private static final String INVALID_NAME_MESSAGE = String.format("The name can't be less than %d and more than %d symbols.", MIN_NAME_LENGTH, MAX_NAME_LENGTH);


    private String name;
    private List<WorkingItems> workingItems;
    private List<String> activityHistory;

    public MemberImpl(String name) {
        setName(name);
        workingItems = new ArrayList<>();
        activityHistory = new ArrayList<>();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<WorkingItems> getWorkingItems() {
        return new ArrayList<>(workingItems);
    }

    @Override
    public List<String> getActivityHistory() {
        return new ArrayList<>(activityHistory);
    }

    @Override
    public void addToActivityHistory(String message) {
        ValidationHelperChecker.checkForNull(message);
        activityHistory.add(message);
    }

    @Override
    public void addToWorkingItems(WorkingItems workingItem) {
        ValidationHelperChecker.checkForNull(workingItem);
        this.workingItems.add(workingItem);
    }

    @Override
    public void removeWorkingItem(int id) {
        this.workingItems.forEach(workingItems1 -> {
            if (workingItems1.getId() == id) {
                this.workingItems.remove(workingItems1);
            }
        });
    }

    private void setName(String name) {
        this.name = ValidationHelperChecker.stringLengthCheck(name, MIN_NAME_LENGTH, MAX_NAME_LENGTH, INVALID_NAME_MESSAGE);
    }

    @Override
    public String toString() {
        return String.format("Member %s", getName());
    }
}


