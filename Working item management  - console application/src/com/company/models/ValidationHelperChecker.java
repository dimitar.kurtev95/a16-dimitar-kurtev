package com.company.models;

public class ValidationHelperChecker {
    private static final String INVALID_NULL_MESSAGE = "The value can not be null";
    static final String MESSAGE_DUPLICATED_UNIT = "The unit already exist";

    public static void checkForNull(Object object) {
        if (object == null) {
            throw new IllegalArgumentException(INVALID_NULL_MESSAGE);
        }
    }

    public static String stringLengthCheck(String string, int min, int max, String message) {
        checkForNull(string);
        if (string.length() < min || string.length() > max) throw new IllegalArgumentException(message);
        return string;
    }

    public static int intValueChecker(int number, int min, int max, String message) {
        if (number < min || number > max) throw new IllegalArgumentException(message);
        return number;
    }

}


