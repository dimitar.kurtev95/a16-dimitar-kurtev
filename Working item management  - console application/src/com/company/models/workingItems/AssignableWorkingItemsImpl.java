package com.company.models.workingItems;

import com.company.models.ValidationHelperChecker;
import com.company.models.common.PriorityType;
import com.company.models.contracts.AssignableWorkingItems;
import com.company.models.contracts.Member;
import com.company.models.contracts.WorkingItems;

import java.util.HashMap;

public abstract class AssignableWorkingItemsImpl extends WorkingItemsImpl implements AssignableWorkingItems {
    private HashMap<String, Member> assignees;
    private PriorityType priorityType;

    public AssignableWorkingItemsImpl(String title, String description, PriorityType priorityType) {
        super(title, description);
        changePriority(priorityType);
        assignees = new HashMap<>();
    }


    @Override
    public HashMap<String, Member> getAssignees() {
        return new HashMap<>(assignees);
    }

    @Override
    public void addAssignee(String name, Member member) {
        ValidationHelperChecker.checkForNull(member);
        assignees.put(name, member);
    }

    @Override
    public void removeAssignee(String name, Member member) {
        ValidationHelperChecker.checkForNull(member);
        assignees.remove(name, member);
    }
    @Override
    public PriorityType getPriorityType() {
        return priorityType;
    }
    @Override
    public void changePriority(PriorityType priority) {
        ValidationHelperChecker.checkForNull(priority);
        this.priorityType = priority;
    }
    @Override
    public int getPriorityWeight() {
        int weight = 0;
        switch (getPriorityType()) {
            case LOW:
                weight = 3;
                break;
            case MEDIUM:
                weight = 2;
                break;
            case HIGH:
                weight = 1;
                break;
        }
        return weight;
    }
}
