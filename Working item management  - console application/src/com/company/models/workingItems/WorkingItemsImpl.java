package com.company.models.workingItems;

import com.company.models.ValidationHelperChecker;
import com.company.models.contracts.Member;
import com.company.models.contracts.WorkingItems;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class WorkingItemsImpl implements WorkingItems {
    private static final int MIN_TITLE_LENGTH = 10;
    private static final int MAX_TITLE_LENGTH = 50;
    private static final String INVALID_TITLE_MESSAGE = String.format("The title can't be less than %d and more than %d symbols.", MIN_TITLE_LENGTH, MAX_TITLE_LENGTH);
    private static final int MIN_DESCRIPTION_LENGTH = 10;
    private static final int MAX_DESCRIPTION_LENGTH = 500;
    private static final String INVALID_DESCRIPTION_MESSAGE = String.format("The description can't be less than %d and more than %d symbols.", MIN_DESCRIPTION_LENGTH, MAX_DESCRIPTION_LENGTH);
    private static int idCounter = 1;

    private int id;
    private String title;
    private String description;
    private List<String> comments;
    private List<String> history;
    private Enum status;
    private String boardName;
    private String teamName;


    public WorkingItemsImpl(String title, String description) {
        setTitle(title);
        setDescription(description);
        comments = new ArrayList<>();
        history = new ArrayList<>();
        id = idCounter++;
        setBoardName(boardName);
        setTeamName(teamName);
    }

    public void setStatus(Enum status) {
        this.status = status;
    }

    public Enum getStatus() {
        return status;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public List<String> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public List<String> getMessagesHistory() {
        return new ArrayList<>(history);
    }


    public int getMinTitleLength() {
        return MIN_TITLE_LENGTH;
    }

    public int getMaxTitleLength() {
        return MAX_TITLE_LENGTH;
    }

    public int getMinDescriptionLength() {
        return MIN_DESCRIPTION_LENGTH;
    }

    public int getMaxDescriptionLength() {
        return MAX_DESCRIPTION_LENGTH;
    }

    @Override
    public String getBoardName() {
        return boardName;
    }

    @Override
    public void setBoardName(String boardName) {
        this.boardName = boardName;
    }

    @Override
    public String getTeamName() {
        return teamName;
    }

    @Override
    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    @Override
    public void addComment(String comment) {
        ValidationHelperChecker.checkForNull(comment);
        comments.add(comment);
    }

    @Override
    public void addToActivityHistory(String text) {
        history.add(text);
    }


    @Override
    public String toString() {
        return String.format("%s" + System.lineSeparator() +
                        "ID: %s" + System.lineSeparator() +
                        "Title: %s" + System.lineSeparator() +
                        "Description: %s" + System.lineSeparator() +
                        "Status: %s", getClass().getName().replace("com.company.models.workingItems.", "").replace("Impl", ""),
                String.valueOf(getId()), getTitle(), getDescription(), getStatus());
    }

    private void setTitle(String title) {
        this.title = ValidationHelperChecker.stringLengthCheck(title, getMinTitleLength(), getMaxTitleLength(), INVALID_TITLE_MESSAGE);

    }

    private void setDescription(String description) {
        this.description = ValidationHelperChecker.stringLengthCheck(description, getMinDescriptionLength(), getMaxDescriptionLength(), INVALID_DESCRIPTION_MESSAGE);
    }

}
