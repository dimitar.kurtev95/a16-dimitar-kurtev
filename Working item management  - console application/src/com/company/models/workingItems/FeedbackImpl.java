package com.company.models.workingItems;

import com.company.models.ValidationHelperChecker;
import com.company.models.common.FeedbackStatusType;
import com.company.models.contracts.Feedback;

public class FeedbackImpl extends WorkingItemsImpl implements Feedback {

    private static final int MIN_RATING = 1;
    private static final int MAX_RATING = 10;
    private static final String INVALID_RATING_MESSAGE = String.format("Rating must be from %d to %d.", MIN_RATING, MAX_RATING);

    private int rating;


    public FeedbackImpl(String title, String description, int rating) {
        super(title, description);
        setRating(rating);
        setStatus(FeedbackStatusType.NEW);
    }

    @Override
    public int getRating() {
        return rating;
    }


    @Override
    public void changeRating(int rating) {
        this.rating = ValidationHelperChecker.intValueChecker(rating, MIN_RATING, MAX_RATING, INVALID_RATING_MESSAGE);
    }


    private void setRating(int rating) {
        this.rating = ValidationHelperChecker.intValueChecker(rating, MIN_RATING, MAX_RATING, INVALID_RATING_MESSAGE);
    }

    @Override
    public String toString() {
        return String.format("%s" + System.lineSeparator() +
                        "Rating: %s" + System.lineSeparator()
                , super.toString(), getRating());
    }
}
