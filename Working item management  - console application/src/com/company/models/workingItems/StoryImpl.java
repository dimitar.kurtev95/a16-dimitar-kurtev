package com.company.models.workingItems;

import com.company.models.common.PriorityType;
import com.company.models.common.SizeType;
import com.company.models.common.StoryStatusType;
import com.company.models.contracts.Story;

public class StoryImpl extends AssignableWorkingItemsImpl implements Story {

    private SizeType sizeType;

    public StoryImpl(String title, String description,
                     PriorityType priorityType, SizeType sizeType) {
        super(title, description, priorityType);
        this.sizeType = sizeType;
        setStatus(StoryStatusType.NOTDONE);
    }


    @Override
    public SizeType getSizeType() {
        return sizeType;
    }


    @Override
    public void changeSize(SizeType size) {
        this.sizeType = size;
    }


    @Override
    public String toString() {
        return String.format("%s" + System.lineSeparator() +
                "Priority: %s" + System.lineSeparator() +
                "Size: %s" + System.lineSeparator(), super.toString(), getPriorityType(), getSizeType());
    }

    public int getSizeWeight() {
        int weight = 0;
        switch (getSizeType()) {
            case SMALL:
                weight = 3;
                break;
            case MEDIUM:
                weight = 2;
                break;
            case LARGE:
                weight = 1;
                break;
        }
        return weight;
    }


}
