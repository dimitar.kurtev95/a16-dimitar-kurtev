package com.company.models.workingItems;

import com.company.models.common.BugStatusType;
import com.company.models.common.PriorityType;
import com.company.models.common.SeverityType;
import com.company.models.contracts.Bug;

import java.util.ArrayList;
import java.util.List;

public class BugImpl extends AssignableWorkingItemsImpl implements Bug {

    private List<String> stepsToReproduce;
    private SeverityType severityType;


    public BugImpl(String title, String description, PriorityType priorityType,
                   SeverityType severityType) {
        super(title, description, priorityType);
        this.severityType = severityType;
        stepsToReproduce = new ArrayList<>();
        setStatus(BugStatusType.ACTIVE);
    }

    @Override
    public List<String> getStepsToReproduce() {
        return new ArrayList<>(stepsToReproduce);
    }

    @Override
    public SeverityType getSeverityType() {
        return severityType;
    }


    @Override
    public void changeSeverity(SeverityType severityType) {
        this.severityType = severityType;
    }

    @Override
    public String toString() {
        return String.format("%s" + System.lineSeparator() +
                "Priority: %s" + System.lineSeparator() +
                "Severity: %s" + System.lineSeparator(), super.toString(), getPriorityType(), getSeverityType());
    }


    @Override
    public int getSeverityWeight() {
        int weight = 0;
        switch (getSeverityType()) {
            case MINOR:
                weight = 3;
                break;
            case MAJOR:
                weight = 2;
                break;
            case CRITICAL:
                weight = 1;
                break;
        }
        return weight;
    }

}
