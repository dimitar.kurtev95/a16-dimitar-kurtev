package com.company.models.common;

public enum PriorityType {
    HIGH,
    MEDIUM,
    LOW
}
