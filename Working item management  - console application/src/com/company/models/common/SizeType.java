package com.company.models.common;

public enum SizeType {
    LARGE,
    MEDIUM,
    SMALL
}
