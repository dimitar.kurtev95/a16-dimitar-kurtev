package com.company.models.common;

public enum FeedbackStatusType {
    NEW,
    UNSCHEDULED,
    SCHEDULED,
    DONE
}
