package com.company.models.common;

public enum BugStatusType {
    ACTIVE,
    FIXED
}
