package com.company.models.common;

public enum SeverityType {
    CRITICAL,
    MAJOR,
    MINOR
}
