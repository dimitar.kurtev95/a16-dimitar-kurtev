package com.company.models.common;

public enum StoryStatusType {
    NOTDONE,
    INPROGRESS,
    DONE
}
