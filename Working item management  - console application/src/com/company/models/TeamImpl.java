package com.company.models;

import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.company.models.ValidationHelperChecker.*;

public class TeamImpl implements Team {

    private String name;
    private List<Member> members;
    private Map<String, Board> boards;
    private List<String> teamActivityHistory;

    public TeamImpl(String name) {
        setName(name);
        members = new ArrayList<>();
        boards = new HashMap<>();
        teamActivityHistory = new ArrayList<>();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Member> getMember() {
        return new ArrayList<>(members);
    }

    @Override
    public Map<String, Board> getBoard() {
        return new HashMap<>(boards);
    }

    public List<String> getTeamActivityHistory() {
        boards.forEach((string, board) -> teamActivityHistory.addAll(board.getActivityHistory()));
        members.forEach(member -> teamActivityHistory.addAll(member.getActivityHistory()));
        return new ArrayList<>(teamActivityHistory);
    }

    @Override
    public void addMember(Member member) {
        ValidationHelperChecker.checkForNull(member);
        if (members.contains(member)) {
            throw new IllegalArgumentException(MESSAGE_DUPLICATED_UNIT);
        }
        members.add(member);
    }

    @Override
    public void addBoard(String boardName, Board board) {
        ValidationHelperChecker.checkForNull(board);
        boards.put(boardName, board);
    }

    @Override
    public String toString() {
        return String.format("Team with name %s", getName());
    }

    private void setName(String name) {
        ValidationHelperChecker.checkForNull(name);
        this.name = name;
    }
}

