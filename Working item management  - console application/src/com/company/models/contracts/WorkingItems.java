package com.company.models.contracts;

import java.util.List;
import java.util.Map;

public interface WorkingItems {

    int getId();

    String getTitle();

    String getDescription();

    List<String> getComments();

    List<String> getMessagesHistory();

    Enum getStatus();

    String getBoardName();

    String getTeamName();

    void setStatus(Enum status);

    void addComment(String comment);


    void addToActivityHistory(String text);

    void setTeamName(String teamName);

    void setBoardName(String boardName);
}
