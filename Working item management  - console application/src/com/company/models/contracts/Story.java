package com.company.models.contracts;

import com.company.models.common.SizeType;
//import com.sun.glass.ui.Size;

public interface Story extends AssignableWorkingItems {

    SizeType getSizeType();

    int getSizeWeight();

    void changeSize(SizeType size);


}
