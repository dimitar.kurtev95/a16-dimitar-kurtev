package com.company.models.contracts;

import java.util.List;
import java.util.Map;

public interface Team {

    String getName();

    List<Member> getMember();

    Map<String, Board> getBoard();

    void addMember(Member member);

    void addBoard(String boardName, Board board);

   List<String> getTeamActivityHistory();
}
