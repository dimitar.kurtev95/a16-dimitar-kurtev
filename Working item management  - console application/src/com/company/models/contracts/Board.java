package com.company.models.contracts;

import java.util.List;
import java.util.Map;

public interface Board {
    String getName();

    Map<Integer, WorkingItems> getWorkingItems();

    List<String> getActivityHistory();

    void addToActivityHistory(String string);

    void addWorkingItem(WorkingItems workingItem);
}
