package com.company.models.contracts;

import com.company.models.common.*;

import java.util.List;

public interface Bug extends AssignableWorkingItems {

    List<String> getStepsToReproduce();

    SeverityType getSeverityType();

    int getSeverityWeight();

    void changeSeverity(SeverityType severityType);
}
