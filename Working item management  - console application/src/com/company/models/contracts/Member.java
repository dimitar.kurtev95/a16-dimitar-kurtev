package com.company.models.contracts;

import java.util.List;

public interface Member {

    String getName();

    List<WorkingItems> getWorkingItems();

    List<String> getActivityHistory();

    void addToActivityHistory(String string);

    void addToWorkingItems(WorkingItems workingItem);

    void removeWorkingItem(int id);
}
