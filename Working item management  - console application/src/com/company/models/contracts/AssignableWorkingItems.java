package com.company.models.contracts;
import com.company.models.common.PriorityType;

import java.util.HashMap;

public interface AssignableWorkingItems extends WorkingItems {
    HashMap<String, Member> getAssignees();

    void addAssignee(String name, Member member);

    void removeAssignee(String name, Member member);

    PriorityType getPriorityType();

    void changePriority(PriorityType priority);

    int getPriorityWeight();
}
