package com.company.models.contracts;

import com.company.models.common.FeedbackStatusType;

public interface Feedback extends WorkingItems {
    int getRating();

    void changeRating(int rating);
}
