package com.company.core.contracts;

import com.company.models.contracts.*;

import java.util.List;
import java.util.Map;

public interface Repository {
    Map<String, Team> getTeams();

    Map<String, Member> getMembers();

    Map<String, Board> getBoards(String teamName);

    Map<Integer, WorkingItems> getWorkingItems();

    Map<Integer, AssignableWorkingItems> getAssignableWorkingItems();

    List<Bug> getBugs();

    List<Feedback> getFeedback();

    List<Story> getStory();

    void addTeam(String name, Team team);

    void addMember(String name, Member member);

    void addBoard(String teamName, Board board);

    void addBug(Bug bug);

    void addFeedback(Feedback feedback);

    void addStory(Story story);
}
