package com.company.core.contracts;

import com.company.models.contracts.*;


public interface Factory {

    Team CreateTeam(String name);

    Member CreateMember(String name);

    Board CreateBoard(String name);

    Bug CreateBug(String title, String description, String priorityType, String severityType);

    Feedback CreateFeedback(String title, String description, int rating);

    Story CreateStory(String title, String description, String priorityType, String sizeType);
}
