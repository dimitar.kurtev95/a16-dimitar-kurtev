package com.company.core.factories;

import com.company.core.contracts.Factory;
import com.company.models.*;
import com.company.models.common.*;
import com.company.models.contracts.*;
import com.company.models.workingItems.*;

public class FactoryImpl implements Factory {
    @Override
    public Team CreateTeam(String name) {
        return new TeamImpl(name);
    }

    @Override
    public Member CreateMember(String name) {
        return new MemberImpl(name);
    }

    @Override
    public Board CreateBoard(String name) {
        return new BoardImpl(name);
    }

    @Override
    public Bug CreateBug(String title, String description, String priorityType, String severityType) {
        return new BugImpl(title, description, getPriorityType(priorityType), getSeverityType(severityType));
    }

    @Override
    public Feedback CreateFeedback(String title, String description, int rating) {
        return new FeedbackImpl(title, description, rating);
    }

    @Override
    public Story CreateStory(String title, String description, String priorityType,
                             String sizeType) {
        return new StoryImpl(title, description, getPriorityType(priorityType), getSizeType(sizeType));
    }

    private PriorityType getPriorityType(String priority) {
        return PriorityType.valueOf(priority.toUpperCase());
    }

    private SeverityType getSeverityType(String severity) {
        return SeverityType.valueOf(severity.toUpperCase());
    }

    private SizeType getSizeType(String size) {
        return SizeType.valueOf(size.toUpperCase());
    }
}
