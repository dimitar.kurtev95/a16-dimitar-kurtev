package com.company.core.factories;

import com.company.commands.listCommands.*;
import com.company.commands.listCommands.sort.*;
import com.company.commands.member.*;
import com.company.commands.workingItems.AddCommentToWorkItem;
import com.company.commands.board.CreateBoard;
import com.company.commands.board.ShowBoardActivity;
import com.company.commands.workingItems.bug.ChangeBugPriority;
import com.company.commands.workingItems.bug.ChangeBugSeverity;
import com.company.commands.workingItems.bug.ChangeBugStatus;
import com.company.commands.workingItems.bug.CreateBug;
import com.company.commands.contracts.Command;
import com.company.commands.enums.CommandType;
import com.company.commands.workingItems.feedback.ChangeFeedbackRating;
import com.company.commands.workingItems.feedback.ChangeFeedbackStatus;
import com.company.commands.workingItems.feedback.CreateFeedback;
import com.company.commands.workingItems.story.ChangeStoryPriority;
import com.company.commands.workingItems.story.ChangeStorySize;
import com.company.commands.workingItems.story.ChangeStoryStatus;
import com.company.commands.workingItems.story.CreateStory;
import com.company.commands.team.*;
import com.company.core.contracts.CommandFactory;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;

public class CommandFactoryImpl implements CommandFactory {

    private static final String INVALID_COMMAND = "Invalid command name: %s!";

    @Override
    public Command createCommand(String commandTypeAsString, Factory factory, Repository repository) {
        CommandType commandType = CommandType.valueOf(commandTypeAsString.toUpperCase());

        switch (commandType) {
            case CREATETEAM:
                return new CreateTeam(repository, factory);
            case CREATEBOARD:
                return new CreateBoard(repository, factory);
            case CREATEMEMBER:
                return new CreateMember(repository, factory);
            case CREATESTORY:
                return new CreateStory(repository, factory);
            case CREATEFEEDBACK:
                return new CreateFeedback(repository, factory);
            case CREATEBUG:
                return new CreateBug(repository, factory);
            case CHANGEFEEDBACKRATING:
                return new ChangeFeedbackRating(repository, factory);
            case CHANGEFEEDBACKSTATUS:
                return new ChangeFeedbackStatus(repository, factory);
            case CHANGESTORYPRIORITY:
                return new ChangeStoryPriority(repository, factory);
            case CHANGESTORYSIZE:
                return new ChangeStorySize(repository, factory);
            case CHANGESTORYSTATUS:
                return new ChangeStoryStatus(repository, factory);
            case CHANGEBUGPRIORITY:
                return new ChangeBugPriority(repository, factory);
            case CHANGEBUGSEVERITY:
                return new ChangeBugSeverity(repository, factory);
            case CHANGEBUGSTATUS:
                return new ChangeBugStatus(repository, factory);
            case SHOWALLTEAMBOARDS:
                return new ShowAllTeamBoards(repository, factory);
            case SHOWALLTEAMS:
                return new ShowAllTeams(repository, factory);
            case SHOWALLTEAMMEMBERS:
                return new ShowAllTeamMembers(repository, factory);
            case SHOWBOARDACTIVITY:
                return new ShowBoardActivity(repository, factory);
            case SHOWTEAMACTIVITY:
                return new ShowTeamActivity(repository, factory);
            case SHOWMEMBERACTIVITY:
                return new ShowMemberActivity(repository, factory);
            case SHOWMEMBERS:
                return new ShowMembers(repository, factory);
            case ADDCOMMENTTOWORKITEM:
                return new AddCommentToWorkItem(repository, factory);
            case ADDMEMBERTOTEAM:
                return new AddMemberToTeam(repository, factory);
            case ASSIGNWORKITEMTOMEMBER:
                return new AssignWorkItemToMember(repository, factory);
            case UNASIGNWORKITEMTOMEMBER:
                return new UnassignWorkItemToMember(repository, factory);
            case LISTALL:
                return new ListAll(repository, factory);
            case LISTALLWORKINGITEMSINTEAM:
                return new ListAllWorkingItemsInTeam(repository, factory);
            case FILTERITEMBYTYPE:
                return new FilterItemByType(repository, factory);
            case FILTERITEMBYSTATUSANDMEMBER:
                return new FilterItemByStatusAndMember(repository, factory);
            case SORTBYTITLE:
                return new SortByTitle(repository, factory);
            case SORTBUGSBYPRIORITY:
                return new SortBugsByPriority(repository, factory);
            case SORTBUGSBYSEVERITY:
                return new SortBugsBySeverity(repository, factory);
            case SORTFEEDBACKBYRATING:
                return new SortFeedbackByRating(repository, factory);
            case SORTSTORYBYSIZE:
                return new SortStoryBySize(repository, factory);
            case SORTSTORYBYPRIORITY:
                return new SortStoryByPriority(repository, factory);
            default:
                throw new IllegalArgumentException(String.format(INVALID_COMMAND, commandTypeAsString));
        }
    }
}
