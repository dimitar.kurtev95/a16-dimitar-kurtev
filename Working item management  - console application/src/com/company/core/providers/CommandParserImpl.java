package com.company.core.providers;

import com.company.core.contracts.CommandParser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandParserImpl implements CommandParser {

    private static final String PLEASE_CLOSE_QUOTES = "Please close quotes to proceed.";

    @Override
    public String parseCommand(String fullCommand) {
        return fullCommand.split(" ")[0];
    }

    @Override
    public List<String> parseParameters(String fullCommand) {
        if (fullCommand.contains("\"")) {
            return filterQuotes(fullCommand);
        } else {
            String[] commandParts = fullCommand.split(" ");
            return new ArrayList<>(Arrays.asList(commandParts).subList(1, commandParts.length));
        }
    }

    private static List<String> filterQuotes(String fullCommand) {
        int quoteCounter = 0;

        for (int i = 0; i < fullCommand.length(); i++) {
            if (fullCommand.charAt(i) == '"') {
                quoteCounter++;
            }
        }

        if (quoteCounter % 2 != 0) {
            throw new IllegalArgumentException(PLEASE_CLOSE_QUOTES);
        }

        ArrayList<String> quotedStrings = new ArrayList<>();
        Pattern pattern = Pattern.compile("(\"+[\\w\\d'!?.,@#$%^&*()_= ]+\")");
        Matcher matcher = pattern.matcher(fullCommand);

        while (matcher.find()) {
            quotedStrings.add(matcher.group().substring(1, matcher.group().length() - 1));
            fullCommand = fullCommand.replace(matcher.group(), "*");
        }

        List<String> parameters = Arrays.asList(fullCommand.split(" "));
        AtomicInteger group = new AtomicInteger(0);

        parameters.forEach(s -> {
            if (s.equals("*")) {
                parameters.set(parameters.indexOf(s), quotedStrings.get(group.get()));
                group.set(group.get() + 1);
            }
        });

        return parameters.subList(1, parameters.size());
    }
}
