package com.company.core.providers;

import com.company.core.contracts.Repository;
import com.company.models.contracts.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RepositoryImpl implements Repository {

    private Map<String, Team> teams;
    private Map<String, Member> members;
    private Map<Integer, WorkingItems> workingItems;
    private Map<Integer, AssignableWorkingItems> assignableWorkingItems;
    private List<Bug> bugs;
    private List<Feedback> feedback;
    private List<Story> stories;


    public RepositoryImpl() {
        this.teams = new HashMap<>();
        this.members = new HashMap<>();
        this.workingItems = new HashMap<>();
        this.assignableWorkingItems = new HashMap<>();
        this.bugs = new ArrayList<>();
        this.feedback = new ArrayList<>();
        this.stories = new ArrayList<>();
    }

    @Override
    public Map<String, Team> getTeams() {
        return new HashMap<>(teams);
    }

    @Override
    public Map<String, Member> getMembers() {
        return new HashMap<>(members);
    }

    @Override
    public Map<String, Board> getBoards(String teamName) {
        return teams.get(teamName).getBoard();
    }

    @Override
    public Map<Integer, WorkingItems> getWorkingItems() {
        teams.forEach((key, value)
                -> value.getBoard().forEach((string, board)
                -> board.getWorkingItems().forEach((integer, workingItems1)
                -> workingItems.put(workingItems1.getId(), workingItems1))));
        return new HashMap<>(workingItems);
    }

    @Override
    public Map<Integer, AssignableWorkingItems> getAssignableWorkingItems() {
        bugs.forEach(Bug -> assignableWorkingItems.put(Bug.getId(), Bug));
        stories.forEach(Story -> assignableWorkingItems.put(Story.getId(), Story));
        return new HashMap<>(assignableWorkingItems);
    }

    @Override
    public void addTeam(String name, Team team) {
        this.teams.put(name, team);
    }

    @Override
    public void addMember(String name, Member member) {
        this.members.put(name, member);
    }

    @Override
    public void addBoard(String teamName, Board board) {
        this.teams.get(teamName).addBoard(board.getName(), board);
    }

    @Override
    public void addBug(Bug bug) {
        bugs.add(bug);
    }

    @Override
    public void addFeedback(Feedback feedback) {
        this.feedback.add(feedback);
    }

    @Override
    public void addStory(Story story) {
        stories.add(story);
    }

    public List<Bug> getBugs() {
        return new ArrayList<>(bugs);
    }

    public List<Feedback> getFeedback() {
        return new ArrayList<>(feedback);
    }

    public List<Story> getStory() {
        return new ArrayList<>(stories);
    }
}
