# Java16 TEAM#7  Dimitar and Kristiyan

## [Our Trello team board](https://trello.com/b/830YPV03/work-item-management-wim)
------------------------------------------------------------
<dl>
    <dt><b>Application Commands:</b></d>
      <dd><i>Command {Required Input}..</i></dd>

<dt><b>Create:</b></d>
    <dd>CREATEBOARD {Board name} {Team name} {Member name}</dd>
    <dd>CREATEMEMBER {Member name}</dd>
    <dd>CREATETEAM {Team name}</dd>
    <dd>CREATESTORY {Team name} {Member name} {Board name} "{Title}" "{Description}" {Priority} {Size} </dd>
    <dd>CREATEFEEDBACK {Team name} {Member name} {Board name} "{Title}" "{Description}" {Rating}</dd>
    <dd>CREATEBUG {Team name} {Member name} {Board name} "{Title}" "{Description}" {Priority} {Severity}</dd>


<dt><b>Add:</b></d>
    <dd>ADDCOMMENTTOWORKITEM {Workitem ID} {Member name} {Comment}</dd>
    <dd>ADDMEMBERTOTEAM {Member name} {Team name}</dd>

    
<dt><b>Addign\Unassign:</b></d>
    <dd>ASSIGNWORKITEMTOMEMBER {Workitem ID} {Member name}</dd>
    <dd>UNASIGNWORKITEMTOMEMBER {WorkItem ID} {Member name}</dd>


<dt><b>Change:</b></d>
    <dd>CHANGESTORYPRIORITY {Member name} {Workitem ID} {Priority type}</dd>
    <dd>CHANGESTORYSIZE {Member name} {Workitem ID} {Size type}</dd>
    <dd>CHANGESTORYSTATUS {Member name} {Workitem ID} {Status type}</dd>
    <dd>CHANGEFEEDBACKRATING {Member name} {Workitem ID} {Rating type}</dd>
    <dd>CHANGEFEEDBACKSTATUS {Member name} {Workitem ID} {Status type}</dd>
    <dd>CHANGEBUGPRIORITY {Member name} {Workitem ID} {Priority type}</dd>
    <dd>CHANGEBUGSEVERITY {Member name} {Workitem ID} {Severity type}</dd>
    <dd>CHANGEBUGSTATUS {Member name} {Workitem ID} {Status type}</dd>


<dt><b>Show:</b></d>
    <dd>SHOWBOARDACTIVITY {Team name} {Board name}</dd>
    <dd>SHOWMEMBERS</dd>
    <dd>SHOWMEMBERACTIVITY {Member name}</dd>
    <dd>SHOWALLTEAMBOARDS {Team name}</dd>
    <dd>SHOWALLTEAMMEMBERS {Team name}</dd>
    <dd>SHOWALLTEAMS</dd>
    <dd>SHOWTEAMACTIVITY {Team name}</dd>


<dt><b>List:</b></d>
    <dd>LISTALL</dd>
    <dd>LISTALLWORKINGITEMSINTEAM {Team name}</dd>


<dt><b>Filter:</b></d>
    <dd>FILTERITEMBYTYPE filter {Team name} {Workitem type}</dd>
    <dd>FILTERITEMBYSTATUSANDMEMBER filter {Status} {Member name}</dd>


<dt><b>Sort:</b></d>
    <dd>SORTBYTITLE</dd>
    <dd>SORTBUGSBYPRIORITY</dd>
    <dd>SORTBUGSBYSEVERITY</dd>
    <dd>SORTFEEDBACKBYRATING</dd>
    <dd>SORTSTORYBYSIZE</dd>
    <dd>SORTSTORYBYPRIORITY</dd>
</dl>

**_Sample Input:_**

<dl>
<dt>CreateTeam team1</dt>
<dt>CreateTeam team2</dt>
<dt>CreateTeam team3</dt>
<dt>CreateMember pesho</dt>
<dt>CreateMember ivannn</dt>
<dt>CreateMember stannn</dt>
<dt>CreateMember fazannn</dt>
<dt>AddMemberToTeam pesho team1</dt>
<dt>AddMemberToTeam ivannn team2</dt>
<dt>AddMemberToTeam stannn team2</dt>
<dt>AddMemberToTeam fazannn team3</dt>
<dt>CreateBoard board1 team1 pesho</dt>
<dt>CreateBoard board1 team2 ivannn</dt>
<dt>CreateBoard board1 team3 fazannn</dt>
<dt>CreateStory team1 pesho board1 "Story1 This is story1's title" "This is the description" low large</dt>
<dt>CreateFeedback team1 pesho board1 "Feedback1 This is feedback1's title" "This is the description" 5</dt>
<dt>CreateBug team1 pesho board1 "Bug1 This is bug1's title" "This is the description" medium minor</dt>
<dt>CreateStory team2 ivannn board1 "Story2 This is story2's title" "This is the description" low large</dt>
<dt>CreateFeedback team2 stannn board1 "Feedback2 This is feedback2's title" "This is the description" 5</dt>
<dt>CreateBug team2 stannn board1 "Bug2 This is bug2's title" "This is the description" high minor</dt>
<dt>CreateStory team3 fazannn board1 "Story3 This is story3's title" "This is the description" low large</dt>
<dt>CreateFeedback team3 fazannn board1 "Feedback3 This is feedback3's title" "This is the description" 5</dt>
<dt>CreateBug team3 fazannn board1 "Bug3 This is bug3's title" "This is the description" high minor</dt>
<dt>ChangeBugPriority pesho 3 high</dt>
<dt>ChangeStorySize pesho 1 small</dt>
<dt>ListAll</dt>
<dt>ListAllWorkingItemsInTeam team1</dt>
<dt>SortByTitle</dt>
<dt>SortBugsByPriority</dt>
</dl>