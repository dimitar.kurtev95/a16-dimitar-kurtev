package com.company.tests.models;
import com.company.core.contracts.Repository;
import com.company.models.BoardImpl;
import com.company.models.MemberImpl;
import com.company.models.TeamImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class TeamImpl_Tests {


    private Member memberTest= new MemberImpl("Pesho");
    private Board boardTest = new BoardImpl("board1");



    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_NameIsNull() {
        // Arrange, Act, Assert
        Team team = new TeamImpl(null);
    }

    @Test
    public void constructor_should_returnTeam_when_valuesAreValid() {
        // Arrange, Act
        Team team = new TeamImpl("Team1");

        // Assert
        Assert.assertEquals("Team1",team.getName() );
    }

@Test public void team_should_addMember_when_valuesAreValid(){
        //Arrange
    Team team = new TeamImpl("Team2");
       // Act
    team.addMember(memberTest);
       //Assert
    Assert.assertSame(team.getMember().get(0), memberTest);
}

    @Test public void team_should_getTeamActivity_when_veluesAreValid(){
        //Arrange
        Team team = new TeamImpl("team1");
        team.addMember(memberTest);
        team.addBoard("board1",boardTest);
        boardTest.addToActivityHistory("board");
        memberTest.addToActivityHistory("member");
        List <String> testTeamActivityHistory = new ArrayList<>();
        testTeamActivityHistory.addAll(boardTest.getActivityHistory());
        testTeamActivityHistory.addAll(memberTest.getActivityHistory());

        //Act,Assert
        Assert.assertEquals(team.getTeamActivityHistory(),testTeamActivityHistory);

    }

}
