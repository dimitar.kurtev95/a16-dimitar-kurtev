package com.company.tests.models.workingItems;

import com.company.models.common.PriorityType;
import com.company.models.common.SizeType;
import com.company.models.contracts.Story;
import com.company.models.workingItems.StoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class StoryImpl_tests {
    private String title;
    private String description;
    private String priorityType;
    private String sizeType;
    private Story story;

    @Before
    public void initTests() {
        title = "Story 1 title dsokfosdko";
        description = "Story 1 description dsokfosdko";
        priorityType = PriorityType.MEDIUM.toString();
        sizeType = SizeType.SMALL.toString();
    }

    @Test
    public void constructor_should_createStory_when_parametersAreValid() {
        //Arrange
        //Act
        story = new StoryImpl(title, description, PriorityType.valueOf(priorityType), SizeType.valueOf(sizeType));
        //Assert
        Assert.assertEquals(title, story.getTitle());
        Assert.assertEquals(description, story.getDescription());
        Assert.assertEquals(priorityType, story.getPriorityType().toString());
        Assert.assertEquals(sizeType, story.getSizeType().toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwException_when_titleIsNotValid() {
        //Arrange
        title = "bla";
        //Act & Assert
        story = new StoryImpl(title, description, PriorityType.valueOf(priorityType), SizeType.valueOf(sizeType));
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwException_when_descriptionIsNotValid() {
        //Arrange
        StringBuilder tooLongString = new StringBuilder("");
        for (int i = 0; i < 50; i++) {
            tooLongString.append("StoryStory");
        }
        description = tooLongString.toString() + "1";
        //Act & Assert
        story = new StoryImpl(title, description, PriorityType.valueOf(priorityType), SizeType.valueOf(sizeType));
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwException_when_priorityIsNotValid() {
        //Arrange
        priorityType = "bla";
        //Act & Assert
        story = new StoryImpl(title, description, PriorityType.valueOf(priorityType), SizeType.valueOf(sizeType));
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwException_when_sizeIsNotValid() {
        //Arrange
        sizeType = "bla";
        //Act & Assert
        story = new StoryImpl(title, description, PriorityType.valueOf(priorityType), SizeType.valueOf(sizeType));
    }
}
