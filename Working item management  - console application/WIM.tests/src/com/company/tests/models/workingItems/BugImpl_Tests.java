package com.company.tests.models.workingItems;

import com.company.models.MemberImpl;
import com.company.models.common.PriorityType;
import com.company.models.common.SeverityType;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Member;
import com.company.models.workingItems.BugImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BugImpl_Tests {

    StringBuilder description = new StringBuilder();

    @Before
    public void before(){
        for (int i = 0; i <502 ; i++) {
            description.append("s");
        }
    }

    @Test
    public void constructor_should_returnBug_when_valuesAreValid() {
        // Arrange, Act
        Bug bug = new BugImpl("adsdsadasdsasad","dsadasdasdasdasads", PriorityType.LOW, SeverityType.MAJOR);

        // Assert
        Assert.assertEquals("adsdsadasdsasad", bug.getTitle());
        Assert.assertEquals("dsadasdasdasdasads", bug.getDescription());
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_titleLengthLess() {
        // Arrange, Act, Assert
        Bug bug = new BugImpl("adsd","dsadasdasdasdasads", PriorityType.LOW, SeverityType.MAJOR);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_titleLengthMore() {
        // Arrange, Act, Assert
        Bug bug = new BugImpl("adsdassssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss" +
                "gffggfggfgfgffffffffffffffffffffffffffffffffffffffffffff","dsadasdasdasdasads", PriorityType.LOW, SeverityType.MAJOR);
    }


    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_descriptionLengthLess() {
        // Arrange, Act, Assert
        Bug bug = new BugImpl("adsdassasasasa","dsada", PriorityType.LOW, SeverityType.MAJOR);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_descriptionLengthMore() {
        // Arrange, Act, Assert
        Bug bug = new BugImpl("adsdassasasasa",String.valueOf(description), PriorityType.LOW, SeverityType.MAJOR);
    }


}
