package com.company.tests.models.workingItems;

import com.company.models.contracts.Feedback;
import com.company.models.workingItems.FeedbackImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FeedbackImpl_tests {
    private String title;
    private String description;
    private int rating;
    private Feedback feedback;

    @Before
    public void initTests() {
        title = "Story 1 title dsokfosdko";
        description = "Story 1 description dsokfosdko";
        rating = 7;
    }

    @Test
    public void constructor_should_createStory_when_parametersAreValid() {
        //Arrange
        //Act
        feedback = new FeedbackImpl(title, description, rating);
        //Assert
        Assert.assertEquals(title, feedback.getTitle());
        Assert.assertEquals(description, feedback.getDescription());
        Assert.assertEquals(rating, feedback.getRating());
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwException_when_titleIsNotValid() {
        //Arrange
        title = "bla";
        //Act & Assert
        feedback = new FeedbackImpl(title, description, rating);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwException_when_descriptionIsNotValid() {
        //Arrange
        StringBuilder tooLongString = new StringBuilder("");
        for (int i = 0; i < 50; i++) {
            tooLongString.append("StoryStory");
        }
        description = tooLongString.toString() + "1";
        //Act & Assert
        feedback = new FeedbackImpl(title, description, rating);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwException_when_ratingIsNotValid() {
        //Arrange
        rating = 142;
        //Act & Assert
        feedback = new FeedbackImpl(title, description, rating);
    }
}
