package com.company.tests.models;

import com.company.models.MemberImpl;
import com.company.models.TeamImpl;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import org.junit.Assert;
import org.junit.Test;

public class MemberImpl_Tests {
    @Test
    public void constructor_should_returnMember_when_valuesAreValid() {
        // Arrange, Act
        Member member = new MemberImpl("pesho");

        // Assert
        Assert.assertEquals("pesho",member.getName() );
    }

    @Test
    public void constructor_should_addToMemberActivityHistory_when_valuesAreValid() {
        // Arrange
        Member member = new MemberImpl("pesho");
        String comment ="Test for comments";
        //  Act
        member.addToActivityHistory(comment);
        // Assert
        Assert.assertEquals(member.getActivityHistory().toString()
                .replace("[","")
                .replace("]",""), comment);
    }


    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throw_when_memberNameIsLessThanMinCharacters() {
        // Act
        Member member = new MemberImpl("pepi");
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throw_when_memberNameIsMoreThanMaxCharacters() {
        // Act
        Member member = new MemberImpl("Peshoooooooooooooo");
    }




}
