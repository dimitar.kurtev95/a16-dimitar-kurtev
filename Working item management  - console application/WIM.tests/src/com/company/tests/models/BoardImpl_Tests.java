package com.company.tests.models;


import com.company.models.BoardImpl;
import com.company.models.common.FeedbackStatusType;
import com.company.models.contracts.Board;
import com.company.models.contracts.WorkingItems;
import com.company.models.workingItems.FeedbackImpl;
import org.junit.Assert;
import org.junit.Test;

public class BoardImpl_Tests {
    WorkingItems workingItems = new FeedbackImpl("titleWorkingItem", "Description of working item",5);

    @Test
    public void constructor_should_returnBoard_when_valuesAreValid() {
        // Arrange, Act
        Board board = new BoardImpl("board1");

        // Assert
        Assert.assertEquals(board.getName(),"board1");
    }

    @Test
    public void constructor_should_addToBoardActivityHistory_when_valuesAreValid() {
        // Arrange
        Board board = new BoardImpl("board");
        String comment ="Test for comments";
        //  Act
        board.addToActivityHistory(comment);
        // Assert
        Assert.assertEquals(board.getActivityHistory().toString()
                .replace("[","")
                .replace("]",""), comment);
    }

    @Test
    public void constructor_should_addWorkingItemToBoard_when_valuesAreValid() {
        // Arrange
        Board board = new BoardImpl("board");
        String comment ="Test for comments";
        //  Act
        board.addToActivityHistory(comment);
        // Assert
        Assert.assertEquals(board.getActivityHistory().toString()
                .replace("[","")
                .replace("]",""), comment);
    }



    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throw_when_boardNameIsLessThanMinCharacters() {
        // Act
        Board board = new BoardImpl("bord");
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throw_when_boardNameIsMoreThanMaxCharacters() {
        // Act
        Board board = new BoardImpl("Board_212233213131313");
    }




}
