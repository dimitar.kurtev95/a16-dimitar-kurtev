package com.company.tests.commands.member;

import com.company.commands.contracts.Command;
import com.company.commands.member.ShowMemberActivity;
import com.company.commands.member.ShowMembers;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowMembers_Tests {
    private Command testCommand;
    private Factory factory;
    private Repository repository;
    private List<String> testList;


    @Before
    public void before(){
        testList = new ArrayList<>();
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand=new ShowMembers(repository,factory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_mapWithMembersIsEmpty() {
        // Arrange
        //  Act & Assert
        testCommand.execute(testList);

    }

}
