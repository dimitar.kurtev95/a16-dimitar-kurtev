package com.company.tests.commands.member;

import com.company.commands.board.CreateBoard;
import com.company.commands.contracts.Command;
import com.company.commands.member.CreateMember;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateMember_Tests {
    private Command testCommand;
    private Factory factory;
    private Repository repository;
    private Member member;


    @Before
    public void before() {
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new CreateMember(repository, factory);

    }
    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();


        /// Act & Assert
        testCommand.execute(testList);
    }


    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();

        testList.add("member");
        testList.add("member");

        /// Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_createMember_when_inputIsValid() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("vanko");
        // Act
        testCommand.execute(testList);
        // Assert
        Assert.assertEquals(1, repository.getMembers().size());

    }
    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_memberAlreadyExit() {
        // Arrange
        List<String> testList = new ArrayList<>();

        member = factory.CreateMember("vanko");
        repository.addMember("vanko", member);
        testList.add("vanko");

        // Act,Assert
        testCommand.execute(testList);

    }

    @Test
    public void execute_should_addActivityHistory_when_inputIsValid() {
        // Arrange
        List<String> testList = new ArrayList<>();
        String message = "test";
        member = factory.CreateMember("vanko");
        repository.addMember("vanko", member);
        // Act
        member.addToActivityHistory(message);
        // Assert
        Assert.assertEquals(1, repository.getMembers().get("vanko").getActivityHistory().size());

    }
}
