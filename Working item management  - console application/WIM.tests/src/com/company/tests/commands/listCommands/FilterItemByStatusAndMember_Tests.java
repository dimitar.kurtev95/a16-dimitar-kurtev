package com.company.tests.commands.listCommands;

import com.company.commands.contracts.Command;
import com.company.commands.listCommands.FilterItemByStatusAndMember;
import com.company.commands.listCommands.FilterItemByType;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import com.company.models.contracts.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.company.commands.listCommands.FilterItemByStatusAndMember.lABEL_FOR_FILTER_STATUS_AND_ASSIGNE;
import static com.company.commands.listCommands.FilterItemByType.LABEL_FOR_FILTER_BY_TYPE;


public class FilterItemByStatusAndMember_Tests {


        private Command testCommand;
        private Factory factory;
        private Repository repository;
        private List<String> testList;
        private Team team;
        private Member member;
        private Board board;
        private Bug bug;
        private Feedback feedback;
        String expected;

        @Before
        public void before() {
            testList = new ArrayList<>();
            factory = new FactoryImpl();
            repository = new RepositoryImpl();
            testCommand = new FilterItemByStatusAndMember(repository, factory);
            team = factory.CreateTeam("team");
            repository.addTeam(team.getName(), team);
            member = factory.CreateMember("pesho");
            team.addMember(member);
            repository.addMember(member.getName(), member);
            board = factory.CreateBoard("board");
            team.addBoard(board.getName(), board);
            bug = factory.CreateBug("dsadsadsadsadsdsa", "dasdasadsdasdasdas", "low", "major");
            feedback = factory.CreateFeedback("dsadsadsasaasdsadsdsa", "dasdasadsdaasassdasdas", 5);
            board.addWorkingItem(bug);
            board.addWorkingItem(feedback);
            repository.addBug(bug);
            repository.addFeedback(feedback);
            bug.setTeamName(team.getName());
            feedback.setTeamName(team.getName());
            bug.addAssignee(member.getName(),member);

        }

        @Test(expected = IllegalArgumentException.class)
        public void execute_should_throwException_when_passedLessArguments() {
            // Arrange
            testList.add("member");
            /// Act & Assert
            testCommand.execute(testList);
        }


        @Test(expected = IllegalArgumentException.class)
        public void execute_should_throwException_when_passedMoreArguments() {
            // Arrange
            testList.add("member");
            testList.add("member");
            testList.add("member");
            testList.add("member");
            testList.add("member");
            testList.add("member");

            /// Act & Assert
            testCommand.execute(testList);
        }


        @Test(expected = IllegalArgumentException.class)
        public void execute_should_throwException_when_filterCommandsMissed() {
            // Arrange
            testList.add("filt");
            testList.add("BUG");
            testList.add(member.getName());

            /// Act & Assert
            testCommand.execute(testList);
        }

        @Test(expected = IllegalArgumentException.class)
        public void execute_should_throwException_when_memberNotExist() {
            // Arrange
            testList.add("filt");
            testList.add("ACTIVE");
            testList.add("sddssdsd");


            /// Act & Assert
            testCommand.execute(testList);
        }

        @Test(expected = IllegalArgumentException.class)
        public void execute_should_throwException_when_statusNotExist() {
            // Arrange
            testList.add("filter");
            testList.add("sasdas");
            testList.add(member.getName());

            /// Act & Assert
            testCommand.execute(testList);
        }
    @Test
    public void execute_should_listAll_when_inputIsValid() {
        // Arrange
        String status = "ACTIVE";
        StringBuilder print = new StringBuilder();
        print.append(String.format(lABEL_FOR_FILTER_STATUS_AND_ASSIGNE, status.toUpperCase(), member.getName().toUpperCase()));

        List<AssignableWorkingItems> asignees = repository.getAssignableWorkingItems()
                .values()
                .stream()
                .filter(assignableWorkingItems -> assignableWorkingItems.getAssignees().containsKey(member.getName()))
                .collect(Collectors.toList());

        List<AssignableWorkingItems> assigneesAndStatus = asignees
                .stream()
                .filter(assignee -> assignee.getStatus().toString().equals(status))
                .collect(Collectors.toList());

        for (AssignableWorkingItems s : assigneesAndStatus) {
            print.append(s.toString() + System.lineSeparator());
        }
        testList.add("filter");
        testList.add(status);
        testList.add(member.getName());

        // Act
        String filter=testCommand.execute(testList);

        //Assert
        Assert.assertEquals(print.toString(), filter);
    }

    }
