package com.company.tests.commands.listCommands;

import com.company.commands.contracts.Command;
import com.company.commands.listCommands.FilterItemByType;
import com.company.commands.member.ShowMembers;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import com.company.models.contracts.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.company.commands.listCommands.FilterItemByType.LABEL_FOR_FILTER_BY_TYPE;

public class FilterItemByType_Tests {

    private Command testCommand;
    private Factory factory;
    private Repository repository;
    private List<String> testList;
    private Team team;
    private Member member;
    private Board board;
    private Bug bug;
    private Feedback feedback;
    String expected;

    @Before
    public void before() {
        testList = new ArrayList<>();
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new FilterItemByType(repository, factory);
        team = factory.CreateTeam("team");
        repository.addTeam(team.getName(), team);
        member = factory.CreateMember("pesho");
        team.addMember(member);
        repository.addMember(member.getName(), member);
        board = factory.CreateBoard("board");
        team.addBoard(board.getName(), board);
        bug = factory.CreateBug("dsadsadsadsadsdsa", "dasdasadsdasdasdas", "low", "major");
        feedback = factory.CreateFeedback("dsadsadsasaasdsadsdsa", "dasdasadsdaasassdasdas", 5);
        board.addWorkingItem(bug);
        board.addWorkingItem(feedback);
        repository.addBug(bug);
        repository.addFeedback(feedback);
        bug.setTeamName(team.getName());
        feedback.setTeamName(team.getName());



    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        testList.add("member");
        /// Act & Assert
        testCommand.execute(testList);
    }


    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        testList.add("member");
        testList.add("member");
        testList.add("member");
        testList.add("member");
        testList.add("member");

        /// Act & Assert
        testCommand.execute(testList);
    }


    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_filterCommandsMissed() {
        // Arrange
        testList.add("filt");
        testList.add(team.getName());
        testList.add("BUG");

        /// Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_teamNotExist() {
        // Arrange
        testList.add("filter");
        testList.add("adsdas");
        testList.add("BUG");

        /// Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_typeNotExist() {
        // Arrange
        testList.add("filter");
        testList.add(team.getName());
        testList.add("sasdas");

        /// Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_filterByType_when_inputIsValid() {
        // Arrange
        String itemType = "FEEDBACK";
        expected=(String.format(LABEL_FOR_FILTER_BY_TYPE, itemType.toUpperCase(), team.getName().toUpperCase()));
        List<WorkingItems> items = repository.getWorkingItems()
                .values()
                .stream()
                .filter(workingItems -> workingItems.getTeamName().equals(team.getName()))
                .filter(workingItems -> workingItems.getClass().toString().toUpperCase().contains(itemType))
                .collect(Collectors.toList());

        for (WorkingItems item : items) {
            expected+=(item.toString()+System.lineSeparator());
        }
        testList.add("filter");
        testList.add(team.getName());
        testList.add(itemType);

        // Act
        String filter=testCommand.execute(testList);

        //Assert
        Assert.assertEquals(expected, filter);
    }


}
