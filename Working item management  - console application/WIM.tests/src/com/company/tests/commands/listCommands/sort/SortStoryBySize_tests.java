package com.company.tests.commands.listCommands.sort;

import com.company.commands.contracts.Command;
import com.company.commands.listCommands.sort.SortBugsByPriority;
import com.company.commands.listCommands.sort.SortStoryBySize;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import com.company.models.contracts.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortStoryBySize_tests {
    private Factory factory;
    private Repository repository;
    private Command testCommand;
    private List<String> testInputParameters;
    private Team team;
    private Board board;
    private Member member;
    private Story story;

    @Before
    public void initTest() {
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new SortStoryBySize(repository, factory);
        testInputParameters = new ArrayList<>();
        team = factory.CreateTeam("team1");
        repository.addTeam(team.getName(), team);
        board = factory.CreateBoard("board1");
        repository.addBoard(team.getName(), board);
        member = factory.CreateMember("pesho");
        repository.addMember(member.getName(), member);
        team.addMember(member);
        story = factory.CreateStory("story1foaksdfoa", "fasdfasd asdf asfasdf af", "medium", "medium");
        board.addWorkingItem(story);
        repository.addStory(story);
        story = factory.CreateStory("story2foaksdfoa", "fasdfasd asdf asfasdf af", "low", "large");
        board.addWorkingItem(story);
        repository.addStory(story);
        story = factory.CreateStory("story3foaksdfoa", "fasdfasd asdf asfasdf af", "medium", "small");
        board.addWorkingItem(story);
        repository.addStory(story);
        story = factory.CreateStory("story4foaksdfoa", "fasdfasd asdf asfasdf af", "low", "small");
        board.addWorkingItem(story);
        repository.addStory(story);
        story = factory.CreateStory("story5foaksdfoa", "fasdfasd asdf asfasdf af", "high", "large");
        board.addWorkingItem(story);
        repository.addStory(story);
    }

    @Test
    public void execute_should_sortStoriesBySize() {
        //Arrange
        String expectedResult;
        String result;
        List<Story> sorted = repository.getStory()
                .stream()
                .sorted(Comparator.comparing(Story::getSizeWeight))
                .collect(Collectors.toList());

        expectedResult = "The stories are sorted by SIZE:" + System.lineSeparator() + " " +
                sorted.toString().replace(",", System.lineSeparator())
                        .replace("[", "")
                        .replace("]", "") +
                "-------------------------------" + System.lineSeparator();
        //Act
        result = testCommand.execute(testInputParameters);
        //Assert
        Assert.assertEquals(expectedResult, result);
    }
}
