package com.company.tests.commands.listCommands.sort;

import com.company.commands.contracts.Command;
import com.company.commands.listCommands.sort.SortFeedbackByRating;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import com.company.models.contracts.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortFeedbackByRating_tests {

    private Factory factory;
    private Repository repository;
    private Command testCommand;
    private List<String> testInputParameters;
    private Team team;
    private Board board;
    private Member member;
    private Feedback feedback;

    @Before
    public void initTest() {
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new SortFeedbackByRating(repository, factory);
        testInputParameters = new ArrayList<>();
        team = factory.CreateTeam("team1");
        repository.addTeam(team.getName(), team);
        board = factory.CreateBoard("board1");
        repository.addBoard(team.getName(), board);
        member = factory.CreateMember("pesho");
        repository.addMember(member.getName(), member);
        team.addMember(member);
        feedback = factory.CreateFeedback("feedback1foaksdfoa", "fasdfasd asdf asfasdf af", 7);
        board.addWorkingItem(feedback);
        repository.addFeedback(feedback);
        feedback = factory.CreateFeedback("feedback2foaksdfoa", "fasdfasd asdf asfasdf af", 3);
        board.addWorkingItem(feedback);
        repository.addFeedback(feedback);
        feedback = factory.CreateFeedback("feedback3foaksdfoa", "fasdfasd asdf asfasdf af", 7);
        board.addWorkingItem(feedback);
        repository.addFeedback(feedback);
        feedback = factory.CreateFeedback("feedback4foaksdfoa", "fasdfasd asdf asfasdf af", 3);
        board.addWorkingItem(feedback);
        repository.addFeedback(feedback);
        feedback = factory.CreateFeedback("feedback5foaksdfoa", "fasdfasd asdf asfasdf af", 10);
        board.addWorkingItem(feedback);
        repository.addFeedback(feedback);
    }

    @Test
    public void execute_should_sortFeedbackByRating() {
        //Arrange
        String expectedResult;
        String result;
        List<Feedback> sorted = repository.getFeedback()
                .stream()
                .sorted(Comparator.comparing(Feedback::getRating))
                .collect(Collectors.toList());

        expectedResult = "Feedback is sorted by RATING:" + System.lineSeparator() + " " +
                sorted.toString().replace(",", System.lineSeparator())
                        .replace("[", "")
                        .replace("]", "") +
                "-------------------------------" + System.lineSeparator();
        //Act
        result = testCommand.execute(testInputParameters);
        //Assert
        Assert.assertEquals(expectedResult, result);
    }
}
