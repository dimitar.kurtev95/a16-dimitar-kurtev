package com.company.tests.commands.listCommands.sort;

import com.company.commands.contracts.Command;
import com.company.commands.listCommands.sort.SortBugsBySeverity;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortBugsBySeverity_tests {
    private Factory factory;
    private Repository repository;
    private Command testCommand;
    private List<String> testInputParameters;
    private Team team;
    private Board board;
    private Member member;
    private Bug bug;

    @Before
    public void initTest() {
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new SortBugsBySeverity(repository, factory);
        testInputParameters = new ArrayList<>();
        team = factory.CreateTeam("team1");
        repository.addTeam(team.getName(), team);
        board = factory.CreateBoard("board1");
        repository.addBoard(team.getName(), board);
        member = factory.CreateMember("pesho");
        repository.addMember(member.getName(), member);
        team.addMember(member);
        bug = factory.CreateBug("bug1foaksdfoa", "fasdfasd asdf asfasdf af", "medium", "minor");
        board.addWorkingItem(bug);
        repository.addBug(bug);
        bug = factory.CreateBug("bug2foaksdfoa", "fasdfasd asdf asfasdf af", "low", "minor");
        board.addWorkingItem(bug);
        repository.addBug(bug);
        bug = factory.CreateBug("bug3foaksdfoa", "fasdfasd asdf asfasdf af", "medium", "critical");
        board.addWorkingItem(bug);
        repository.addBug(bug);
        bug = factory.CreateBug("bug4foaksdfoa", "fasdfasd asdf asfasdf af", "low", "MAJOR");
        board.addWorkingItem(bug);
        repository.addBug(bug);
        bug = factory.CreateBug("bug5foaksdfoa", "fasdfasd asdf asfasdf af", "high", "CritIcal");
        board.addWorkingItem(bug);
        repository.addBug(bug);
    }

    @Test
    public void execute_should_sortBugsBySeverity() {
        //Arrange
        String expectedResult;
        String result;
        List<Bug> sorted = repository.getBugs()
                .stream()
                .sorted(Comparator.comparing(Bug::getSeverityWeight))
                .collect(Collectors.toList());

        expectedResult = "The bugs are sorted by SEVERITY:" + System.lineSeparator() +
                sorted.toString().replace(",", System.lineSeparator())
                        .replace("[", "")
                        .replace("]", "") +
                "-------------------------------" + System.lineSeparator();
        //Act
        result = testCommand.execute(testInputParameters);
        //Assert
        Assert.assertEquals(expectedResult, result);
    }
}
