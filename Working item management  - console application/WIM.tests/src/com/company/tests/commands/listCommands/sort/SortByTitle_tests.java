package com.company.tests.commands.listCommands.sort;

import com.company.commands.contracts.Command;
import com.company.commands.listCommands.sort.SortByTitle;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import com.company.models.contracts.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortByTitle_tests {
    private Factory factory;
    private Repository repository;
    private Command testCommand;
    private List<String> testInputParameters;
    private Team team;
    private Board board;
    private Member member;
    private Bug bug;
    private Feedback feedback;
    private Story story;

    @Before
    public void initTest() {
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new SortByTitle(repository, factory);
        testInputParameters = new ArrayList<>();
        team = factory.CreateTeam("team1");
        repository.addTeam(team.getName(), team);
        board = factory.CreateBoard("board1");
        repository.addBoard(team.getName(), board);
        member = factory.CreateMember("pesho");
        repository.addMember(member.getName(), member);
        team.addMember(member);
        story = factory.CreateStory("story1 dofkasfo oasfk", "aosdkfokf aksofdk a", "high", "large");
        board.addWorkingItem(story);
        repository.addStory(story);
        story = factory.CreateStory("story2 dofkasfo oasfk", "aosdkfokf aksofdk a", "high", "large");
        board.addWorkingItem(story);
        repository.addStory(story);
        story = factory.CreateStory("story3 dofkasfo oasfk", "aosdkfokf aksofdk a", "high", "large");
        board.addWorkingItem(story);
        repository.addStory(story);
        story = factory.CreateStory("story4 dofkasfo oasfk", "aosdkfokf aksofdk a", "high", "large");
        board.addWorkingItem(story);
        repository.addStory(story);
        feedback = factory.CreateFeedback("feedback1 oaskdfo akosdf", "aosdfk akdofsk kaosdfk ", 7);
        board.addWorkingItem(feedback);
        repository.addFeedback(feedback);
        feedback = factory.CreateFeedback("feedback2 oaskdfo akosdf", "aosdfk akdofsk kaosdfk ", 7);
        board.addWorkingItem(feedback);
        repository.addFeedback(feedback);
        feedback = factory.CreateFeedback("feedback3 oaskdfo akosdf", "aosdfk akdofsk kaosdfk ", 7);
        board.addWorkingItem(feedback);
        repository.addFeedback(feedback);
        bug = factory.CreateBug("bug1foaksdfoa", "fasdfasd asdf asfasdf af", "medium", "minor");
        board.addWorkingItem(bug);
        repository.addBug(bug);
        bug = factory.CreateBug("bug2foaksdfoa", "fasdfasd asdf asfasdf af", "low", "minor");
        board.addWorkingItem(bug);
        repository.addBug(bug);
        bug = factory.CreateBug("bug3foaksdfoa", "fasdfasd asdf asfasdf af", "medium", "critical");
        board.addWorkingItem(bug);
        repository.addBug(bug);
        bug = factory.CreateBug("bug4foaksdfoa", "fasdfasd asdf asfasdf af", "low", "MAJOR");
        board.addWorkingItem(bug);
        repository.addBug(bug);
        bug = factory.CreateBug("bug5foaksdfoa", "fasdfasd asdf asfasdf af", "high", "CritIcal");
        board.addWorkingItem(bug);
        repository.addBug(bug);

    }

    @Test
    public void execute_should_sortByTitle() {
        //Arrange
        String expectedResult;
        String result;
        List<WorkingItems> sorted = repository.getWorkingItems()
                .values()
                .stream()
                .sorted(Comparator.comparing(WorkingItems::getTitle))
                .collect(Collectors.toList());

        expectedResult =  "The working items are sorted by TITLE:" + System.lineSeparator() + " "+
                sorted.toString().replace(",", System.lineSeparator())
                        .replace("[", "")
                        .replace("]", "")+
                "-------------------------------" + System.lineSeparator();
        //Act
        result = testCommand.execute(testInputParameters);
        //Assert
        Assert.assertEquals(expectedResult, result);
    }
}
