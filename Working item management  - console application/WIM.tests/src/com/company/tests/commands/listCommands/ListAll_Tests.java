package com.company.tests.commands.listCommands;

import com.company.commands.contracts.Command;
import com.company.commands.listCommands.ListAll;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import com.company.models.contracts.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ListAll_Tests {


    private Command testCommand;
    private Factory factory;
    private Repository repository;
    private List<String> testList;
    private Team team;
    private Member member;
    private Board board;
    private Bug bug;
    private Feedback feedback;
    String expected;

    @Before
    public void before() {
        testList = new ArrayList<>();
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new ListAll(repository, factory);

    }


    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_noTeamExist() {
        // Arrange, Act & Assert
        testCommand.execute(testList);
    }


    @Test
    public void execute_should_listAll_when_isValid() {
        // Arrange
        team = factory.CreateTeam("team");
        repository.addTeam(team.getName(), team);
        member = factory.CreateMember("pesho");
        team.addMember(member);
        repository.addMember(member.getName(), member);
        board = factory.CreateBoard("board");
        team.addBoard(board.getName(), board);
        bug = factory.CreateBug("dsadsadsadsadsdsa", "dasdasadsdasdasdas", "low", "major");
        feedback = factory.CreateFeedback("dsadsadsasaasdsadsdsa", "dasdasadsdaasassdasdas", 5);
        board.addWorkingItem(bug);
        board.addWorkingItem(feedback);
        repository.addBug(bug);
        repository.addFeedback(feedback);
        bug.setTeamName(team.getName());
        feedback.setTeamName(team.getName());
        StringBuilder expected= new StringBuilder();
        repository.getTeams().forEach((key, value) -> {
            expected.append(String.format("~~~~~~~~~~~~~~~~\nTeam %s boards:\n", value.getName()));
            value.getBoard().forEach((string, Board) ->   expected.append(String.format("%s\n", Board.getName())));
            expected.append("~~~~~~~~~~~~~~~~\n");
            value.getBoard().forEach((string, Board) -> {
                expected.append(java.lang.String.format("Working items in board %s:\n", Board.getName()));
                Board.getWorkingItems().forEach((id, WorkingItems) ->  expected.append(WorkingItems.toString()).append(System.lineSeparator()));
            });
        });

        //Act
        String actual = testCommand.execute(testList);

        //Assert
        Assert.assertEquals(expected.toString(),actual);
    }


}
