package com.company.tests.commands.board;

import com.company.commands.board.CreateBoard;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import com.company.models.TeamImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateBoard_Tests {
    private Command testCommand;
    private Factory factory;
    private Repository repository;
    private Team team;
    private Board board;
    private Member member;

    @Before
    public void before() {
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new CreateBoard(repository, factory);
        team = factory.CreateTeam("team1");
        repository.addTeam("team1", team);
        member = factory.CreateMember("Vanko");
        repository.addMember("Vanko",member);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("board1");
        //  Act & Assert
        testCommand.execute(testList);

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();

        testList.add("boardsd1");
        testList.add("team1");
        testList.add("boardsd1");
        testList.add("boardsd1");
        testList.add("boardsd1");
        testList.add("boardsd1");
        testList.add("boardsd1");

        /// Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_createBoard_when_inputIsValid() {
        // Arrange
        team.addMember(member);
        List<String> testList = new ArrayList<>();
        testList.add("board1");
        testList.add("team1");
        testList.add("Vanko");
        // Act
        testCommand.execute(testList);
        // Assert
        Assert.assertEquals(1, repository.getBoards("team1").size());

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_boardAlreadyExistInTeam() {
        // Arrange
        List<String> testList = new ArrayList<>();

        board = factory.CreateBoard("board1");
        repository.addBoard("team1", board);
        repository.getTeams().get("team1").addMember(member);

        testList.add("board1");
        testList.add("team1");
        testList.add("vanko");
        // Act,Assert
        testCommand.execute(testList);


    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_memberNotExistInTeam() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("board1");
        testList.add("team1");
        testList.add("vanko");
        // Act,Assert
        testCommand.execute(testList);

    }

    @Test
    public void execute_should_addActivityHistory_when_inputIsValid() {
        // Arrange
        List<String> testList = new ArrayList<>();
        String message = "test";
        board = factory.CreateBoard("board1");
        repository.addBoard("team1",board);

        // Act
        board.addToActivityHistory(message);
        // Assert
        Assert.assertEquals(1, repository.getBoards("team1").get("board1").getActivityHistory().size());

    }


}