package com.company.tests.commands.board;

import com.company.commands.board.ShowBoardActivity;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowBoardActivity_Tests {
    private Command testCommand;
    private Factory factory;
    private Repository repository;
    private  List<String> testList;

    @Before public void before(){
       testList = new ArrayList<>();
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand=new ShowBoardActivity(repository,factory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        testList.add("board1");
        //  Act & Assert
        testCommand.execute(testList);

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        testList.add("boardsd1");
        testList.add("team1");
        testList.add("boardsd1");
        testList.add("boardsd1");
        testList.add("boardsd1");
        testList.add("boardsd1");
        testList.add("boardsd1");

        /// Act & Assert
        testCommand.execute(testList);
    }
    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_teamNotExist() {
        // Arrange
        testList.add("board1");
        testList.add("board1");
        //  Act & Assert
        testCommand.execute(testList);

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_boardNotExist() {
        // Arrange
        testList.add("board1");
        testList.add("board1");
        //  Act & Assert
        testCommand.execute(testList);

    }

}
