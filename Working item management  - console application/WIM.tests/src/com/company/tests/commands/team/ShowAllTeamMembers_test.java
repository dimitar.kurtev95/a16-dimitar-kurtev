package com.company.tests.commands.team;

import com.company.commands.contracts.Command;
import com.company.commands.team.AddMemberToTeam;
import com.company.commands.team.ShowAllTeamMembers;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowAllTeamMembers_test {
    private Factory factory;
    private Repository repository;
    private Command testCommand;
    private Command addMemberToTeam;
    private List<String> testInputParameters;
    private Team team;
    private Member member;


    @Before
    public void initTests() {
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new ShowAllTeamMembers(repository, factory);
        testInputParameters = new ArrayList<>();
        testCommand = new ShowAllTeamMembers(repository, factory);
        addMemberToTeam = new AddMemberToTeam(repository, factory);
        team = factory.CreateTeam("team1");
        member = factory.CreateMember("pesho");
        repository.addTeam(team.getName(), team);
        repository.addMember(member.getName(), member);
        testInputParameters.add(member.getName());
        testInputParameters.add(team.getName());
        addMemberToTeam.execute(testInputParameters);
        testInputParameters.clear();
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        //Arrange, Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedTooManyArguments() {
        //Arrange
        testInputParameters.add(team.getName());
        testInputParameters.add(team.getName());
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_teamDoesNotExists() {
        //Arrange
        testInputParameters.add("team2");
        //Act & Assert
        testCommand.execute(testInputParameters);
    }
}
