package com.company.tests.commands.team;

import com.company.commands.contracts.Command;
import com.company.commands.team.AddMemberToTeam;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import com.company.models.contracts.Member;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AddMemberToTeam_test {
    private Repository repository;
    private Factory factory;
    private Command testCommand;
    private List<String> testInputParameters;
    private Member testMember;
    private static final String TEAM_NAME = "team1";
    private static final String MEMBER_NAME = "pesho";

    @Before
    public void initTests(){
        repository = new RepositoryImpl();
        factory = new FactoryImpl();
        testCommand = new AddMemberToTeam(repository, factory);
        testInputParameters = new ArrayList<>();
        testMember = factory.CreateMember(MEMBER_NAME);
        repository.addMember(MEMBER_NAME, testMember);
        repository.addTeam(TEAM_NAME, factory.CreateTeam(TEAM_NAME));
    }

    @Test
    public void execute_should_addMemberToTeam_ifTeamAndMemberExist(){
        //Arrange
        testInputParameters.add(MEMBER_NAME);
        testInputParameters.add(TEAM_NAME);
        //Act
        testCommand.execute(testInputParameters);
        //Assert
        Assert.assertEquals(MEMBER_NAME, repository.getTeams().get(TEAM_NAME).getMember().get(0).getName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_whenMemberExistsInTeam(){
        //Arrange
        testInputParameters.add(MEMBER_NAME);
        testInputParameters.add(TEAM_NAME);
        repository.getTeams().get(TEAM_NAME).addMember(testMember);
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_teamDoesNotExist() {
        //Arrange
        testInputParameters.add(MEMBER_NAME);
        testInputParameters.add("dfgsdg");
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_memberDoesNotExist() {
        //Arrange
        testInputParameters.add("ivan");
        testInputParameters.add(TEAM_NAME);
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        //Arrange
        testInputParameters.add(MEMBER_NAME);
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedTooManyArguments() {
        //Arrange
        testInputParameters.add(MEMBER_NAME);
        testInputParameters.add(MEMBER_NAME);
        testInputParameters.add(MEMBER_NAME);
        testInputParameters.add(MEMBER_NAME);
        //Act & Assert
        testCommand.execute(testInputParameters);
    }
}
