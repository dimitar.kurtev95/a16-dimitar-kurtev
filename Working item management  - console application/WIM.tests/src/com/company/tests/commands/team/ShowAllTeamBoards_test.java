package com.company.tests.commands.team;

import com.company.commands.contracts.Command;
import com.company.commands.team.ShowAllTeamBoards;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Team;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowAllTeamBoards_test {
    private Factory factory;
    private Repository repository;
    private Command testCommand;
    private Team testTeam;
    private Board testBoard;
    private List<String> testInputParameters;

    @Before
    public void initTests() {
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new ShowAllTeamBoards(repository,factory);
        testTeam = factory.CreateTeam("team1");
        testBoard = factory.CreateBoard("board1");
        repository.addTeam(testTeam.getName(), testTeam);
        repository.addBoard(testTeam.getName(), testBoard);
        testInputParameters = new ArrayList<>();
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        //Arrange
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedTooManyArguments() {
        //Arrange
        testInputParameters.add(testTeam.getName());
        testInputParameters.add(testTeam.getName());
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_shout_throwException_when_teamDoesNotExist() {
        //Arrange
        testInputParameters.add("team23");
        //Act & Assert
        testCommand.execute(testInputParameters);
    }
}
