package com.company.tests.commands.team;

import com.company.commands.contracts.Command;
import com.company.commands.team.CreateTeam;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateTeam_test {
    private Repository repository;
    private Factory factory;
    private Command testCommand;
    private List<String> testInput;

    @Before
    public void initTests() {
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new CreateTeam(repository, factory);
        testInput = new ArrayList<>();
    }

    @Test
    public void execute_should_createTeam() {
        //Arrange
        int teamMapInRepositoryExpectedSize = 3;
        //Act
        testInput.add("team1");
        testCommand.execute(testInput);
        testInput.clear();
        testInput.add("team2");
        testCommand.execute(testInput);
        testInput.clear();
        testInput.add("team3");
        testCommand.execute(testInput);
        //Assert
        Assert.assertEquals(teamMapInRepositoryExpectedSize, repository.getTeams().size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        testInput.add("team1");
        testInput.add("team1");
        //Act & Assert
        testCommand.execute(testInput);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        //Arrange
        //Act & Assert
        testCommand.execute(testInput);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_teamExists() {
        //Arrange
        String teamName = "team1";
        testInput.add(teamName);
        repository.addTeam(teamName, factory.CreateTeam(teamName));
        //Act & Assert
        testCommand.execute(testInput);
    }
}
