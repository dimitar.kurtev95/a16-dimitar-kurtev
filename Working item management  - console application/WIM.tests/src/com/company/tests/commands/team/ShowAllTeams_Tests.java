package com.company.tests.commands.team;

import com.company.commands.contracts.Command;
import com.company.commands.listCommands.ListAllWorkingItemsInTeam;
import com.company.commands.team.ShowAllTeams;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import com.company.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowAllTeams_Tests {
    private Command testCommand;
    private Factory factory;
    private Repository repository;
    private List<String> testList;
    private Team team;
    private Team team1;

    @Before
    public void before() {
        testList = new ArrayList<>();
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new ShowAllTeams(repository, factory);

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_TeamNotExist() {
        // Arrange, Act & Assert
        testList.add("team2");
        testCommand.execute(testList);
    }
    @Test
    public void execute_should_showAllTeams_when_isValid() {
        // Arrange


        team = factory.CreateTeam("team");
        team1 = factory.CreateTeam("team1");
        repository.addTeam(team.getName(), team);
        repository.addTeam(team1.getName(), team1);


        StringBuilder expected = new StringBuilder("All teams in the application are:"+System.lineSeparator());
        repository.getTeams()
                .values()
                .forEach(team -> expected.append(team.toString()+System.lineSeparator()));

        //Act
        String actual = testCommand.execute(testList);
        //Assert
        Assert.assertEquals(expected.toString(),actual);

    }

}
