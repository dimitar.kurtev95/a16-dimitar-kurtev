package com.company.tests.commands.workingItems.feedback;

import com.company.commands.contracts.Command;
import com.company.commands.workingItems.feedback.ChangeFeedbackRating;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Feedback;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangeFeedbackRating_tests {
    private Repository repository;
    private Factory factory;
    private Command testCommand;
    private List<String> testInputParameters;
    private Team team;
    private Board board;
    private Member member;
    private Feedback feedback;
    private int rating;

    @Before
    public void initTests() {
        repository = new RepositoryImpl();
        factory = new FactoryImpl();
        testCommand = new ChangeFeedbackRating(repository, factory);
        testInputParameters = new ArrayList<>();
        team = factory.CreateTeam("team1");
        repository.addTeam(team.getName(), team);
        member = factory.CreateMember("pesho");
        repository.addMember(member.getName(), member);
        team.addMember(member);
        board = factory.CreateBoard("board1");
        repository.addBoard(team.getName(), board);
        rating = 7;
        feedback = factory.CreateFeedback("feeedback1fokad" ,"saodfkoffkafo dosafkap o", rating);
        repository.addFeedback(feedback);
        board.addWorkingItem(feedback);
    }

    @Test
    public void execute_should_changeFeedbackRating_if_parametersAreValid() {
        //Arrange
        rating = 5;
        testInputParameters.add(member.getName());
        testInputParameters.add(String.valueOf(feedback.getId()));
        testInputParameters.add(String.valueOf(rating));
        //Act
        testCommand.execute(testInputParameters);
        //Assert
        Assert.assertEquals(rating, feedback.getRating());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_memberIsNotValid() {
        //Arrange
        testInputParameters.add("ivan");
        testInputParameters.add(String.valueOf(feedback.getId()));
        testInputParameters.add(String.valueOf(rating));
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_idIsNotValid() {
        //Arrange
        testInputParameters.add(member.getName());
        testInputParameters.add(String.valueOf(645));
        testInputParameters.add(String.valueOf(rating));
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_ratingIsNotValid() {
        //Arrange
        rating = 5634;
        testInputParameters.add(member.getName());
        testInputParameters.add(String.valueOf(feedback.getId()));
        testInputParameters.add(String.valueOf(rating));
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedTooManyParameters() {
        //Arrange
        testInputParameters.add(member.getName());
        testInputParameters.add(String.valueOf(feedback.getId()));
        testInputParameters.add(String.valueOf(rating));
        testInputParameters.add(String.valueOf(rating));
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessParameters() {
        //Arrange
        rating = 5634;
        testInputParameters.add(member.getName());
        testInputParameters.add(String.valueOf(feedback.getId()));
        //Act & Assert
        testCommand.execute(testInputParameters);
    }
}
