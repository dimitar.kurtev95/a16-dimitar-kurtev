package com.company.tests.commands.workingItems.story;

import com.company.commands.contracts.Command;
import com.company.commands.workingItems.story.ChangeStoryStatus;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Story;
import com.company.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangeStoryStatus_tests {
    private Factory factory;
    private Repository repository;
    private Command testCommand;
    private List<String> testInputParameters;
    private Team team;
    private Board board;
    private Member member;
    private Story story;
    private String status = "done";

    @Before
    public void initTests() {
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new ChangeStoryStatus(repository, factory);
        testInputParameters = new ArrayList<>();
        team = factory.CreateTeam("team1");
        repository.addTeam(team.getName(), team);
        board = factory.CreateBoard("board1");
        repository.addBoard(team.getName(), board);
        member = factory.CreateMember("pesho");
        repository.addMember(member.getName(), member);
        team.addMember(member);
        story = factory.CreateStory("story1dofkasodf", "descogkfdgposd", "low", "large");
        board.addWorkingItem(story);
        repository.addStory(story);
    }

    @Test
    public void execute_should_changeStoryStatus_when_parametersAreValid() {
        //Arrange
        testInputParameters.add(member.getName());
        testInputParameters.add(String.valueOf(story.getId()));
        testInputParameters.add(status);
        //Act
        testCommand.execute(testInputParameters);
        //Assert
        Assert.assertEquals(status.toUpperCase(), story.getStatus().toString().toUpperCase());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_memberIsNotValid() {
        //Arrange
        testInputParameters.add("ivan");
        testInputParameters.add(String.valueOf(story.getId()));
        testInputParameters.add(status);
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_idIsNotValid() {
        //Arrange
        testInputParameters.add(member.getName());
        testInputParameters.add("453");
        testInputParameters.add(status);
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_statusIsNotValid() {
        //Arrange
        status = "bla";
        testInputParameters.add(member.getName());
        testInputParameters.add(String.valueOf(story.getId()));
        testInputParameters.add(status);
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedTooManyParameters() {
        //Arrange
        testInputParameters.add(member.getName());
        testInputParameters.add(String.valueOf(story.getId()));
        testInputParameters.add(status);
        testInputParameters.add(status);
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessParameters() {
        //Arrange
        testInputParameters.add(member.getName());
        testInputParameters.add(String.valueOf(story.getId()));
        //Act & Assert
        testCommand.execute(testInputParameters);
    }
}
