package com.company.tests.commands.workingItems.bug;

import com.company.commands.contracts.Command;
import com.company.commands.workingItems.bug.ChangeBugStatus;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangeBugStatus_Tests {

    private List<String> testList;
    private Command testCommand;
    private Factory factory;
    private Repository repository;
    private Team team;
    private Member member;
    private Board board;
    private Bug bug;

    @Before
    public void before() {
        testList = new ArrayList<>();
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new ChangeBugStatus(repository, factory);
        team = factory.CreateTeam("team1");
        repository.addTeam("team1", team);
        member = factory.CreateMember("Vanko");
        repository.addMember("Vanko", member);
        team.addMember(member);
        board = factory.CreateBoard("Board");
        repository.addBoard(team.getName(),board);
        team.addBoard("Board", board);

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        testList.add("bug");
        //  Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        testList.add("bug");
        testList.add("bug");
        testList.add("bug");
        testList.add("bug");
        testList.add("bug");
        testList.add("bug");
        testList.add("bug");
        testList.add("bug");
        testList.add("bug");
        testList.add("bug");
        testList.add("bug");
        /// Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_changeStatus_when_inputIsValid() {
        //arrange
        bug = factory.CreateBug("sdadsdasasdaasdsa", "dsadsadsadasdsadsaads", "LOW", "MAJOR");
        repository.addBug(bug);
        board.addWorkingItem(bug);
        testList.add("Vanko");
        testList.add(String.valueOf(bug.getId()));
        testList.add("fixed");
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertEquals("FIXED", bug.getStatus().toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_changeStatus_when_bugNotExist() {
        //Arrange
        testList.add("Vanko");
        testList.add("1");
        testList.add("fixed");
        //Act, Assert
        testCommand.execute(testList);

    }
}


