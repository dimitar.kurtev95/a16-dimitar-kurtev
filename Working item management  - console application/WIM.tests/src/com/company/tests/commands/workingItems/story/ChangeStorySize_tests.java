package com.company.tests.commands.workingItems.story;

import com.company.commands.contracts.Command;
import com.company.commands.workingItems.story.ChangeStorySize;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Story;
import com.company.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangeStorySize_tests {
    private Repository repository;
    private Factory factory;
    private Command testCommand;
    private List<String> testInputParameters;
    private Team team;
    private Board board;
    private Member member;
    private Story story;
    private String size;

    @Before
    public void initTests() {
        repository = new RepositoryImpl();
        factory = new FactoryImpl();
        testCommand = new ChangeStorySize(repository, factory);
        testInputParameters = new ArrayList<>();
        team = factory.CreateTeam("team1");
        repository.addTeam(team.getName(), team);
        board = factory.CreateBoard("board1");
        team.addBoard(board.getName(), board);
        repository.addBoard(team.getName(), board);
        member = factory.CreateMember("pesho");
        team.addMember(member);
        repository.addMember(member.getName(), member);
        size = "medium";
        story = factory.CreateStory("story1dfsg", "descriptionosadfk", "low", size);
        board.addWorkingItem(story);
        repository.addStory(story);
    }

    @Test
    public void execute_should_changeStorySize_if_parametersAreValid() {
        //Arrange
        size = "large";
        testInputParameters.add(member.getName());
        testInputParameters.add(String.valueOf(story.getId()));
        testInputParameters.add(size);
        //Act
        testCommand.execute(testInputParameters);
        //Assert
        Assert.assertEquals(size.toUpperCase(), story.getSizeType().toString().toUpperCase());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_memberIsNotValid() {
        //Arrange
        testInputParameters.add("ivan");
        testInputParameters.add(String.valueOf(story.getId()));
        testInputParameters.add(size);
        //Act & Arrange
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_idIsNotValid() {
        //Arrange
        testInputParameters.add(member.getName());
        testInputParameters.add("34");
        testInputParameters.add(size);
        //Act & Arrange
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_sizeIsNotValid() {
        //Arrange
        size = "bla";
        testInputParameters.add(member.getName());
        testInputParameters.add(String.valueOf(story.getId()));
        testInputParameters.add(size);
        //Act & Arrange
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedTooManyParameters() {
        //Arrange
        testInputParameters.add(member.getName());
        testInputParameters.add(String.valueOf(story.getId()));
        testInputParameters.add(size);
        testInputParameters.add(size);
        //Act & Arrange
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessParameters() {
        //Arrange
        testInputParameters.add(member.getName());
        testInputParameters.add(String.valueOf(story.getId()));
        //Act & Arrange
        testCommand.execute(testInputParameters);
    }
}
