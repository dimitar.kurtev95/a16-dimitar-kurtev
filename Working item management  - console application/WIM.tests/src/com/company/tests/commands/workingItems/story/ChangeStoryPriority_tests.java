package com.company.tests.commands.workingItems.story;

import com.company.commands.contracts.Command;
import com.company.commands.workingItems.story.ChangeStoryPriority;
import com.company.commands.workingItems.story.CreateStory;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Story;
import com.company.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangeStoryPriority_tests {
    private Factory factory;
    private Repository repository;
    private Command testCommand;
    private List<String> testInputParameters;
    private Story story;
    private Member member;
    private Team team;
    private Board board;


    @Before
    public void initTests() {
        repository = new RepositoryImpl();
        factory = new FactoryImpl();
        testCommand = new ChangeStoryPriority(repository, factory);
        testInputParameters = new ArrayList<>();
        story = factory.CreateStory("story1sefgsdfgsdfg", "gsoidjosfiogsdjgfoijdfogd", "low", "large");
        member = factory.CreateMember("pesho");
        team = factory.CreateTeam("team1");
        board = factory.CreateBoard("board1");
        team.addMember(member);
        team.addBoard(board.getName(), board);
        board.addWorkingItem(story);
        repository.addTeam(team.getName(), team);
        repository.addBoard(team.getName(), board);
        repository.addMember(member.getName(), member);
        repository.addStory(story);
    }

    @Test
    public void execute_should_changeStoryPriority_ifParametersAreValid() {
        //Arrange
        String newPriority = "high";
        testInputParameters.add(member.getName());
        testInputParameters.add(String.valueOf(story.getId()));
        testInputParameters.add(newPriority);
        //Act
        testCommand.execute(testInputParameters);
        //Assert
        Assert.assertEquals(newPriority.toUpperCase(), story.getPriorityType().toString().toUpperCase());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_priorityIsNotValid() {
        //Arrange
        String newPriority = "blaa";
        testInputParameters.add(member.getName());
        testInputParameters.add(String.valueOf(story.getId()));
        testInputParameters.add(newPriority);
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_memberIsNotValid() {
        //Arrange
        String newPriority = "high";
        testInputParameters.add("ivan");
        testInputParameters.add(String.valueOf(story.getId()));
        testInputParameters.add(newPriority);
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_idIsNotValid() {
        //Arrange
        String newPriority = "high";
        testInputParameters.add(member.getName());
        testInputParameters.add(String.valueOf(43));
        testInputParameters.add(newPriority);
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessParameters() {
        //Arrange
        testInputParameters.add(member.getName());
        testInputParameters.add(String.valueOf(story.getId()));
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedTooManyParameters() {
        //Arrange
        String newPriority = "blaa";
        testInputParameters.add(member.getName());
        testInputParameters.add(String.valueOf(story.getId()));
        testInputParameters.add(newPriority);
        testInputParameters.add(newPriority);
        testInputParameters.add(newPriority);
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

}
