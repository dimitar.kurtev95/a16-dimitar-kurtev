package com.company.tests.commands.workingItems.bug;

import com.company.commands.contracts.Command;
import com.company.commands.workingItems.bug.ChangeBugSeverity;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangeBugSeverity_Tests {

    private Repository repository;
    private Factory factory;
    private Command testCommand;
    private List<String> testInputParameters;
    private Team team;
    private Board board;
    private Bug bug;
    private Member member;

    @Before
    public void initTests() {
        repository = new RepositoryImpl();
        factory = new FactoryImpl();
        testCommand = new ChangeBugSeverity(repository, factory);
        testInputParameters = new ArrayList<>();
        team = factory.CreateTeam("team1");
        repository.addTeam(team.getName(), team);
        board = factory.CreateBoard("board1");
        team.addBoard(board.getName(), board);
        repository.addBoard(team.getName(), board);
        member = factory.CreateMember("pesho");
        team.addMember(member);
        repository.addMember(member.getName(), member);
        bug = factory.CreateBug("ssdtory1dfsg", "descriptiosdnosadfk", "low", "major");
        board.addWorkingItem(bug);
        repository.addBug(bug);
    }

    @Test
    public void execute_should_changeBugSeverity_if_parametersAreValid() {
        //Arrange

        testInputParameters.add(member.getName());
        testInputParameters.add(String.valueOf(bug.getId()));
        testInputParameters.add("minor");
        //Act
        testCommand.execute(testInputParameters);
        //Assert
        Assert.assertEquals("MINOR", bug.getSeverityType().toString().toUpperCase());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_memberIsNotValid() {
        //Arrange
        testInputParameters.add("ivansdss");
        testInputParameters.add(String.valueOf(bug.getId()));
        testInputParameters.add("MINOR");
        //Act & Arrange
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_idIsNotValid() {
        //Arrange
        testInputParameters.add(member.getName());
        testInputParameters.add("34");
        testInputParameters.add("minor");
        //Act & Arrange
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_severityIsNotValid() {
        //Arrange

        testInputParameters.add(member.getName());
        testInputParameters.add(String.valueOf(bug.getId()));
        testInputParameters.add("minosa");
        //Act & Arrange
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedTooManyParameters() {
        //Arrange
        testInputParameters.add(member.getName());
        testInputParameters.add(String.valueOf(bug.getId()));
        testInputParameters.add("dsads");
        testInputParameters.add("dsads");

        //Act & Arrange
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessParameters() {
        //Arrange
        testInputParameters.add(member.getName());
        testInputParameters.add(String.valueOf(bug.getId()));
        //Act & Arrange
        testCommand.execute(testInputParameters);
    }
}
