package com.company.tests.commands.workingItems.story;

import com.company.commands.contracts.Command;
import com.company.commands.workingItems.story.CreateStory;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateStory_tests {
    private Factory factory;
    private Repository repository;
    private List<String> testInputParameters;
    private Command testCommand;
    private Team team;
    private Board board;
    private Member member;
    private String storyTitle = "Story1 fkaoskfsa oaksdfosa kofaskd";
    private String storyDescription = "sdafasfsf afs sasdf sfsa";
    private String priority = "high";
    private String size = "large";

    @Before
    public void initTests() {
        repository = new RepositoryImpl();
        factory = new FactoryImpl();
        testCommand = new CreateStory(repository, factory);
        testInputParameters = new ArrayList<>();
        team = factory.CreateTeam("Team1");
        repository.addTeam(team.getName(), team);
        board = factory.CreateBoard("Board1");
        repository.addBoard(team.getName(), board);
        member = factory.CreateMember("pesho");
        repository.addMember(member.getName(), member);
        team.addBoard(board.getName(), board);
        team.addMember(member);
    }

    @Test
    public void execute_should_createStory_ifParametersAreValid() {
        //Arrange
        testInputParameters.add(team.getName());
        testInputParameters.add(member.getName());
        testInputParameters.add(board.getName());
        testInputParameters.add(storyTitle);
        testInputParameters.add(storyDescription);
        testInputParameters.add(priority);
        testInputParameters.add(size);
        //Act
        testCommand.execute(testInputParameters);
        //Assert
        Assert.assertEquals(storyTitle, repository.getStory().get(0).getTitle());
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_teamIsNotValid() {
        //Arrange
        testInputParameters.add("Team");
        testInputParameters.add(member.getName());
        testInputParameters.add(board.getName());
        testInputParameters.add(storyTitle);
        testInputParameters.add(storyDescription);
        testInputParameters.add(priority);
        testInputParameters.add(size);
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_memberIsNotValid() {
        //Arrange
        testInputParameters.add(team.getName());
        testInputParameters.add("ivan");
        testInputParameters.add(board.getName());
        testInputParameters.add(storyTitle);
        testInputParameters.add(storyDescription);
        testInputParameters.add(priority);
        testInputParameters.add(size);
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_boardIsNotValid() {
        //Arrange
        testInputParameters.add(team.getName());
        testInputParameters.add(member.getName());
        testInputParameters.add("boarrrddd");
        testInputParameters.add(storyTitle);
        testInputParameters.add(storyDescription);
        testInputParameters.add(priority);
        testInputParameters.add(size);
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_titleIsNotValid() {
        //Arrange
        storyTitle = "oasdk";
        testInputParameters.add(team.getName());
        testInputParameters.add(member.getName());
        testInputParameters.add(board.getName());
        testInputParameters.add(storyTitle);
        testInputParameters.add(storyDescription);
        testInputParameters.add(priority);
        testInputParameters.add(size);
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_descriptionIsNotValid() {
        //Arrange
        storyDescription = "1";
        testInputParameters.add(team.getName());
        testInputParameters.add(member.getName());
        testInputParameters.add(board.getName());
        testInputParameters.add(storyTitle);
        testInputParameters.add(storyDescription);
        testInputParameters.add(priority);
        testInputParameters.add(size);
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_priorityIsNotValid() {
        //Arrange
        priority = "1";
        testInputParameters.add(team.getName());
        testInputParameters.add(member.getName());
        testInputParameters.add(board.getName());
        testInputParameters.add(storyTitle);
        testInputParameters.add(storyDescription);
        testInputParameters.add(priority);
        testInputParameters.add(size);
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_sizeIsNotValid() {
        //Arrange
        size = "bla";
        testInputParameters.add(team.getName());
        testInputParameters.add(member.getName());
        testInputParameters.add(board.getName());
        testInputParameters.add(storyTitle);
        testInputParameters.add(storyDescription);
        testInputParameters.add(priority);
        testInputParameters.add(size);
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedTooManyParameters() {
        //Arrange
        testInputParameters.add(team.getName());
        testInputParameters.add(member.getName());
        testInputParameters.add(board.getName());
        testInputParameters.add(storyTitle);
        testInputParameters.add(storyDescription);
        testInputParameters.add(priority);
        testInputParameters.add(size);
        testInputParameters.add(size);
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessParameters() {
        //Arrange
        testInputParameters.add(team.getName());
        testInputParameters.add(member.getName());
        testInputParameters.add(board.getName());
        testInputParameters.add(storyTitle);
        testInputParameters.add(storyDescription);
        //Act & Assert
        testCommand.execute(testInputParameters);
    }
}
