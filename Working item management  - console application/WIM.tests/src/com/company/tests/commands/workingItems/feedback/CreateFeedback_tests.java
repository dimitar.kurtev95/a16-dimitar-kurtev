package com.company.tests.commands.workingItems.feedback;

import com.company.commands.contracts.Command;
import com.company.commands.workingItems.feedback.CreateFeedback;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateFeedback_tests {
    private Factory factory;
    private Repository repository;
    private Command testCommand;
    private List<String> testInputParameters;
    private Team team;
    private Board board;
    private Member member;
    private String title = "feedbacktitleaodsfk";
    private String description = "faoksdfoakfaffaff";
    private int rating = 5;

    @Before
    public void initTests() {
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new CreateFeedback(repository, factory);
        testInputParameters = new ArrayList<>();
        team = factory.CreateTeam("team1");
        repository.addTeam(team.getName(), team);
        board = factory.CreateBoard("Board1");
        repository.addBoard(team.getName(), board);
        member = factory.CreateMember("pesho");
        repository.addMember(member.getName(), member);
        team.addMember(member);
    }

    @Test
    public void execute_should_createFeedback_if_parametersAreValid() {
        //Arrange
        testInputParameters.add(team.getName());
        testInputParameters.add(member.getName());
        testInputParameters.add(board.getName());
        testInputParameters.add(title);
        testInputParameters.add(description);
        testInputParameters.add(String.valueOf(rating));
        //Act
        testCommand.execute(testInputParameters);
        //Assert
        Assert.assertEquals(title.toUpperCase(), repository.getFeedback().get(0).getTitle().toUpperCase());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_if_teamIsNotValid() {
        //Arrange
        testInputParameters.add("bla");
        testInputParameters.add(member.getName());
        testInputParameters.add(board.getName());
        testInputParameters.add(title);
        testInputParameters.add(description);
        testInputParameters.add(String.valueOf(rating));
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_if_memberIsNotValid() {
        //Arrange
        testInputParameters.add(team.getName());
        testInputParameters.add("bla");
        testInputParameters.add(board.getName());
        testInputParameters.add(title);
        testInputParameters.add(description);
        testInputParameters.add(String.valueOf(rating));
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_if_boardIsNotValid() {
        //Arrange
        testInputParameters.add(team.getName());
        testInputParameters.add(member.getName());
        testInputParameters.add("bla");
        testInputParameters.add(title);
        testInputParameters.add(description);
        testInputParameters.add(String.valueOf(rating));
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_if_titleIsNotValid() {
        //Arrange
        title = "1";
        testInputParameters.add(team.getName());
        testInputParameters.add(member.getName());
        testInputParameters.add(board.getName());
        testInputParameters.add(title);
        testInputParameters.add(description);
        testInputParameters.add(String.valueOf(rating));
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_if_descriptionIsNotValid() {
        //Arrange
        description = "1";
        testInputParameters.add(team.getName());
        testInputParameters.add(member.getName());
        testInputParameters.add(board.getName());
        testInputParameters.add(title);
        testInputParameters.add(description);
        testInputParameters.add(String.valueOf(rating));
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_if_ratingIsNotValid() {
        //Arrange
        rating = 200;
        testInputParameters.add(team.getName());
        testInputParameters.add(member.getName());
        testInputParameters.add(board.getName());
        testInputParameters.add(title);
        testInputParameters.add(description);
        testInputParameters.add(String.valueOf(rating));
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_if_passedTooManyParameters() {
        //Arrange
        testInputParameters.add(team.getName());
        testInputParameters.add(member.getName());
        testInputParameters.add(board.getName());
        testInputParameters.add(title);
        testInputParameters.add(description);
        testInputParameters.add(String.valueOf(rating));
        testInputParameters.add(String.valueOf(rating));
        //Act & Assert
        testCommand.execute(testInputParameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_if_passedLessParameters() {
        //Arrange
        testInputParameters.add(team.getName());
        testInputParameters.add(member.getName());
        testInputParameters.add(board.getName());
        testInputParameters.add(title);
        testInputParameters.add(description);
        //Act & Assert
        testCommand.execute(testInputParameters);
    }
}
