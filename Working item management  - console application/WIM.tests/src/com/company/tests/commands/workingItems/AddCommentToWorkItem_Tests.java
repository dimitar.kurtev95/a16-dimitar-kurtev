package com.company.tests.commands.workingItems;

import com.company.commands.contracts.Command;
import com.company.commands.member.UnassignWorkItemToMember;
import com.company.commands.workingItems.AddCommentToWorkItem;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AddCommentToWorkItem_Tests {
    private Command testCommand;
    private Factory factory;
    private Repository repository;
    private Team team;
    private Board board;
    private Member member;
    private Bug bug;
    private String comment;


    @Before
    public void before() {
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new AddCommentToWorkItem(repository, factory);
        team = factory.CreateTeam("team");
        repository.addTeam("team",team);
        member = factory.CreateMember("vanko");
        team.addMember(member);
        repository.addMember(member.getName(),member);
        board = factory.CreateBoard("board1");
        repository.addBoard("team",board);
        team.addBoard("board1",board);
        bug= factory.CreateBug("dasdsadsaasddsasd", "dsadasdasadssad", "LOW","MAJOR");
        repository.addBug(bug);
        board.addWorkingItem(bug);
        bug.setBoardName(board.getName());
        comment="This is test";
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_memberNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("board1");
        testList.add("vanko");
        testList.add("vanko");

        // Act,Assert
        testCommand.execute(testList);

    }
    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_itemNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("73");
        testList.add("vanko");
        testList.add("vanko");

        // Act,Assert
        testCommand.execute(testList);

    }

    @Test
    public void execute_should_assignMember_when_inputIsValid(){
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add(String.valueOf(bug.getId()));
        testList.add(member.getName());
        testList.add(comment);
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertEquals(1,bug.getComments().size());

    }

    @Test
    public void execute_should_memberShouldAddActivityHistory_when_inputIsValid(){
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add(String.valueOf(bug.getId()));
        testList.add(member.getName());
        testList.add(comment);
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertEquals(1,member.getActivityHistory().size());
    }
    @Test
    public void execute_should_itemShouldAddActivityHistory_when_inputIsValid(){
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add(String.valueOf(bug.getId()));
        testList.add(member.getName());
        testList.add(comment);
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertEquals(1,bug.getMessagesHistory().size());
    }
    @Test
    public void execute_should_boardShouldAddActivityHistory_when_inputIsValid(){
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add(String.valueOf(bug.getId()));
        testList.add(member.getName());
        testList.add(comment);
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertEquals(1,board.getActivityHistory().size());
    }
}
