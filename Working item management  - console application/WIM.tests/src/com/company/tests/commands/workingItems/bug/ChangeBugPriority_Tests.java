package com.company.tests.commands.workingItems.bug;

import com.company.commands.contracts.Command;
import com.company.commands.workingItems.bug.ChangeBugPriority;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import com.company.models.contracts.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangeBugPriority_Tests {
    private List<String> testList1 = new ArrayList<>();;
    private Command testCommand;
    private Factory factory;
    private Repository repository;
    private Team team;
    private Board board;
    private Bug bug;
    Member member;

    @Before
    public void before() {

        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new ChangeBugPriority(repository, factory);
        team = factory.CreateTeam("team1");
        repository.addTeam("team1", team);
        member=factory.CreateMember("Vanko");
        repository.addMember("Vanko", member);
        team.addMember(member);
        board=factory.CreateBoard("Board");
        team.addBoard("Board",board);

    }
    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        testList1.add("bug");
        //  Act & Assert
        testCommand.execute(testList1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        testList1.add("bug");
        testList1.add("bug");
        testList1.add("bug");
        testList1.add("bug");
        testList1.add("bug");
        testList1.add("bug");
        testList1.add("bug");
        testList1.add("bug");
        testList1.add("bug");
        testList1.add("bug");
        testList1.add("bug");
        /// Act & Assert
        testCommand.execute(testList1);
    }

    @Test
    public void execute_should_changePriority_when_inputIsValid(){
        //arrange
        bug=factory.CreateBug("sdadsdasasasdsa","dsadsadsadasddsaads","LOW","MAJOR");
        board.addWorkingItem(bug);
        repository.addBug(bug);
        testList1.add(member.getName());
        testList1.add(String.valueOf(bug.getId()));
        testList1.add("high");
        //Act
        testCommand.execute(testList1);
        //
        Assert.assertEquals("HIGH",bug.getPriorityType().toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_changePriority_when_bugNotExist(){
        //arrange
        testList1.add("Vanko");
        testList1.add("1");
        testList1.add("high");
        //Act
        testCommand.execute(testList1);

    }

}
