package com.company.tests.commands.workingItems.bug;

import com.company.commands.board.CreateBoard;
import com.company.commands.contracts.Command;
import com.company.commands.workingItems.bug.CreateBug;
import com.company.core.contracts.Factory;
import com.company.core.contracts.Repository;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.RepositoryImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateBug_Tests {
    private Command testCommand;
    private List<String> testList= new ArrayList<>();;
    private Factory factory;
    private Repository repository;
    private Team team;
    private Member member;
    private Board board;
    private Bug bug;

    @Before
    public void before() {
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new CreateBug(repository, factory);
        team = factory.CreateTeam("team1");
        member = factory.CreateMember("Vanko");
        repository.addMember("Vanko", member);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        testList.add("bug");
        //  Act & Assert
        testCommand.execute(testList);

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        testList.add("bug");
        testList.add("bug");
        testList.add("bug");
        testList.add("bug");
        testList.add("bug");
        testList.add("bug");
        testList.add("bug");
        testList.add("bug");
        testList.add("bug");
        testList.add("bug");
        testList.add("bug");

        /// Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_createBug_when_inputIsValid() {
        // Arrange
        repository.addTeam("team1", team);
        team.addMember(member);
        board = factory.CreateBoard("Board");
        repository.addBoard("team1", board);

        testList.add("team1");
        testList.add("Vanko");
        testList.add("Board");
        testList.add("titleofthisbug");
        testList.add("descriptionOfThisbug");
        testList.add("LOW");
        testList.add("MAJOR");
        // Act
        testCommand.execute(testList);
        // Assert
        Assert.assertEquals(1, repository.getBugs().size());
        Assert.assertEquals(1, repository.getWorkingItems().size());
        Assert.assertEquals(1, repository.getAssignableWorkingItems().size());

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_teamNotExist() {
        // Arrange
        testList.add("team1");
        testList.add("Vanko");
        testList.add("Board");
        testList.add("titleofthisbug");
        testList.add("descriptionOfThisbug");
        testList.add("LOW");
        testList.add("MAJOR");

        // Act,Assert
        testCommand.execute(testList);

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_memberNotExistInTeam() {
        // Arrange
        team = factory.CreateTeam("team1");
        repository.addTeam("team1", team);

        testList.add("team1");
        testList.add("Vanko");
        testList.add("Board");
        testList.add("titleofthisbug");
        testList.add("descriptionOfThisbug");
        testList.add("LOW");
        testList.add("MAJOR");

        // Act,Assert
        testCommand.execute(testList);

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_boardNotExist() {
        // Arrange
        team = factory.CreateTeam("team1");
        repository.addTeam("team1", team);
        team.addMember(member);

        testList.add("team1");
        testList.add("Vanko");
        testList.add("Board");
        testList.add("titleofthisbug");
        testList.add("descriptionOfThisbug");
        testList.add("LOW");
        testList.add("MAJOR");

        // Act,Assert
        testCommand.execute(testList);

    }


    @Test
    public void execute_should_addActivityHistory_when_inputIsValid() {
        // Arrange
        String message = "test";
        bug = factory.CreateBug("basassaasoard1", "adsdasdasadsasdads", "LOW", "MAJOR");
        repository.addBug(bug);

        // Act
        bug.addToActivityHistory(message);
        // Assert
        Assert.assertEquals(1, repository.getBugs().get(0).getMessagesHistory().size());

    }


}
