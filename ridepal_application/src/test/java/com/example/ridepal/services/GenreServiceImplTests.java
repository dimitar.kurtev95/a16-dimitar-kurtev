package com.example.ridepal.services;

import com.example.ridepal.entities.Genre;
import com.example.ridepal.exceptions.EntityNotFoundException;
import com.example.ridepal.repositories.GenreRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.Silent.class)
public class GenreServiceImplTests {

    @Mock
    GenreRepository genreRepository;

    @InjectMocks
    GenreServiceImpl genreService;

    @Test
    public void findAll_should_callRepository() {
        //Arrange
        Mockito.when(genreRepository.findAll()).thenReturn(Arrays.asList(new Genre(), new Genre()));

        //Act
        List<Genre> genres = genreService.findAll();

        // Assert
        Assert.assertEquals(2, genres.size());
    }

    @Test(expected = EntityNotFoundException.class)
    public void findId_should_throwException() {

        // Arrange
        String genreName = "test";

        //Act
        genreService.findByName(genreName);

        //Assert
        Mockito.verify(genreRepository, Mockito.times(1)).findByName(genreName);
    }

    @Test
    public void findId_should_returnGenre() {

        // Arrange
        Genre genre = new Genre();
        String genreName = "test";
        Mockito.when(genreRepository.findByName(genreName)).thenReturn(genre);

        //Act
        Genre result = genreService.findByName(genreName);

        //Assert
        Assert.assertSame(genre, result);
    }


}
