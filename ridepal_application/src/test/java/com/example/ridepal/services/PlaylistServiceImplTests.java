package com.example.ridepal.services;

import com.example.ridepal.entities.Authority;
import com.example.ridepal.entities.Playlist;
import com.example.ridepal.entities.Track;
import com.example.ridepal.entities.UserDetail;
import com.example.ridepal.exceptions.EntityNotFoundException;
import com.example.ridepal.exceptions.InvalidOperationException;
import com.example.ridepal.models.GenreDTO;
import com.example.ridepal.repositories.AuthorityRepository;
import com.example.ridepal.repositories.PlaylistRepository;
import com.example.ridepal.repositories.TrackRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.Silent.class)
public class PlaylistServiceImplTests {

    private int testId = 1;
    Playlist playlistTest = new Playlist();
    private UserDetail testUserDetail = new UserDetail();
    private Playlist playlist = new Playlist();
    private int duration = 300;
    private List<GenreDTO> genreGenres = new ArrayList<>();
    private UserDetail creator = new UserDetail();
    private List<Authority> authorities = new ArrayList<>();

    @Mock
    PlaylistRepository playlistRepository;

    @Mock
    TrackRepository trackRepository;

    @Mock
    TrackService trackService;

    @Mock
    private Principal principal;

    @InjectMocks
    PlaylistServiceImpl playlistService;

    @Mock
    AuthorityRepository authorityRepository;

    @Before
    public void setUp() {

        creator.setEmail("test@abv.bg");

        playlist.setTitle("Ivan");
        playlist.setCreator(creator);

        GenreDTO genre = new GenreDTO();
        genre.setPercent(100);
        genre.setName("pop");
        genreGenres.add(genre);

    }

    @Test
    public void findId_should_returnPlaylist() {

        // Arrange
        Playlist playlist = new Playlist();

        Mockito.when(playlistRepository.findByIdAndEnabledIsTrue(testId)).thenReturn(playlist);
        //Act
        Playlist result = playlistService.findById(testId);

        //Assert
        Assert.assertSame(playlist, result);
    }


    @Test(expected = EntityNotFoundException.class)
    public void findId_should_throwException() {

        // Arrange
        //Act
        playlistService.findById(testId);

        //Assert
        Mockito.verify(playlistRepository, Mockito.times(1)).findByIdAndEnabledIsTrue(testId);
    }


    @Test
    public void findAll_should_callRepository() {
        //Arrange
        Mockito.when(playlistRepository.findAllByEnabledIsTrue()).thenReturn(Arrays.asList(new Playlist(), new Playlist()));

        //Act
        List<Playlist> playlistList = playlistService.findAll();

        // Assert
        Assert.assertEquals(2, playlistList.size());
    }

    @Test
    public void findAllByCreator_should_callRepository() {
        //Arrange
        int creatorId = 1;
        Mockito.when(playlistRepository.findAllByCreatorIdAndEnabledIsTrue(creatorId)).thenReturn(Arrays.asList(new Playlist(), new Playlist()));

        //Act
        List<Playlist> playlistList = playlistService.findAllByCreator(creatorId);

        // Assert
        Assert.assertEquals(2, playlistList.size());
    }


    @Test
    public void createPlaylistShould_callUser_WhenPlaylistHasRandomTracks() {
        //Arrange
        UserDetail creator = new UserDetail();
        creator.setEmail("test@abv.bg");

        int duration = 300;
        Playlist playlist = new Playlist();
        playlist.setTitle("Ivan");
        playlist.setCreator(creator);

        List<GenreDTO> genreGenres = new ArrayList<>();
        GenreDTO genre = new GenreDTO();
        genre.setPercent(100);
        genre.setName("pop");
        genreGenres.add(genre);

        List<Track> trackList = new ArrayList<>();
        Track track2 = new Track();
        track2.setPreview("test");
        track2.setDuration(290);
        Track track1 = new Track();
        track1.setPreview("test");
        track2.setDuration(300);
        trackList.add(track1);
        trackList.add(track2);

        //doReturn(trackList).when(trackService).findSpecificAmountOfTracksByGenre(genre.getName(),2);
        Mockito.when(trackRepository.findSpecificAmountOfRandomTracksByGenre(anyString(), Mockito.anyInt())).thenReturn(trackList);
        Mockito.when(trackService.findSpecificAmountOfTracksByGenre(anyString(), Mockito.anyInt())).thenReturn(trackList);

        Mockito.when(playlistRepository.save(playlist)).thenReturn(playlist);
        //Act
        Playlist result = playlistService.createPlaylist(duration, genreGenres, playlist);
        //Assert
        Assert.assertSame(playlist, result);

    }

    @Test
    public void createPlaylistShould_callUser_WhenPlaylistHasOrderByRankTracks() {
        //Arrange


        playlist.setTopTracksEnabled(true);

        List<Track> trackList = new ArrayList<>();
        Track track2 = new Track();
        track2.setPreview("test");
        track2.setDuration(290);
        Track track1 = new Track();
        track1.setPreview("test");
        track2.setDuration(300);
        trackList.add(track1);
        trackList.add(track2);


        Mockito.when(trackRepository.findAllByGenre_NameOrderByRankDesc(anyString())).thenReturn(trackList);
        Mockito.when(trackService.getAllOrderedTracksByGenre(anyString())).thenReturn(trackList);

        Mockito.when(playlistRepository.save(playlist)).thenReturn(playlist);
        //Act
        Playlist result = playlistService.createPlaylist(duration, genreGenres, playlist);
        //Assert
        Assert.assertSame(playlist, result);


    }


    @Test(expected = InvalidOperationException.class)
    public void editPlaylistShould_throwException_whenPrincipalNotAdminNorCreator() {

        //Arrange

        authorities.add(new Authority("test@abv.bg", "ROLE_USER"));

        Mockito.when(authorityRepository.findAllByUsername(anyString())).thenReturn(authorities);
        Mockito.when(principal.getName()).thenReturn("other@email.bg");

        //Act
        playlistService.editPlaylist(playlist, principal);

        //Assert
        Mockito.verify(playlistRepository, Mockito.times(1)).save(playlist);
    }

    @Test
    public void editPlaylistShould_callRepository_whenPrincipalIsAdmin() {

        //Arrange

        authorities.add(new Authority("test@abv.bg", "ROLE_USER"));
        authorities.add(new Authority("test@abv.bg", "ROLE_ADMIN"));

        Mockito.when(authorityRepository.findAllByUsername(anyString())).thenReturn(authorities);
        Mockito.when(principal.getName()).thenReturn("other@email.bg");

        //Act
        playlistService.editPlaylist(playlist, principal);

        //Assert
        Mockito.verify(playlistRepository, Mockito.times(1)).save(playlist);
    }

    @Test
    public void editPlaylistShould_callRepository_whenPrincipalIsOnlyCreator() {

        //Arrange

        authorities.add(new Authority("test@abv.bg", "ROLE_USER"));


        Mockito.when(authorityRepository.findAllByUsername(anyString())).thenReturn(authorities);
        Mockito.when(principal.getName()).thenReturn(testUserDetail.getEmail());

        //Act
        playlistService.editPlaylist(playlist, principal);

        //Assert
        Mockito.verify(playlistRepository, Mockito.times(1)).save(playlist);
    }

    @Test
    public void deletePlaylistShould_callRepository_whenPrincipalIsOnlyCreator() {

        //Arrange

        authorities.add(new Authority("test@abv.bg", "ROLE_USER"));


        Mockito.when(authorityRepository.findAllByUsername(anyString())).thenReturn(authorities);
        Mockito.when(principal.getName()).thenReturn(testUserDetail.getEmail());

        //Act
        playlistService.deletePlaylist(playlist, principal);

        //Assert
        Mockito.verify(playlistRepository, Mockito.times(1)).save(playlist);
    }

    @Test
    public void deletePlaylistShould_callRepository_whenPrincipalIsAdmin() {

        //Arrange

        authorities.add(new Authority("test@abv.bg", "ROLE_USER"));
        authorities.add(new Authority("test@abv.bg", "ROLE_ADMIN"));

        Mockito.when(authorityRepository.findAllByUsername(anyString())).thenReturn(authorities);
        Mockito.when(principal.getName()).thenReturn("other@email.bg");

        //Act
        playlistService.deletePlaylist(playlist, principal);

        //Assert
        Mockito.verify(playlistRepository, Mockito.times(1)).save(playlist);
    }

    @Test(expected = InvalidOperationException.class)
    public void deletePlaylistShould_throwException_whenPrincipalNotAdminNorCreator() {

        //Arrange

        authorities.add(new Authority("test@abv.bg", "ROLE_USER"));

        Mockito.when(authorityRepository.findAllByUsername(anyString())).thenReturn(authorities);
        Mockito.when(principal.getName()).thenReturn("other@email.bg");

        //Act
        playlistService.deletePlaylist(playlist, principal);

        //Assert
        Mockito.verify(playlistRepository, Mockito.times(1)).save(playlist);
    }

    @Test
    public void findAllPageable_should_callRepository() {
        //Arrange
        List<Playlist> playlistList = new ArrayList();
        playlistList.add(new Playlist());
        playlistList.add(new Playlist());
        Page<Playlist> pageTrack = new PageImpl(playlistList);
        int size = 1;
        int page = 1;

        Mockito.when(playlistRepository.findAllByEnabledIsTrueOrderByDateCreatedDesc(PageRequest.of(page, size)))
                .thenReturn((pageTrack));
        //Act
        Page<Playlist> resultPlaylists = playlistService.findAll(page, size);

        // Assert
        Assert.assertEquals(2, resultPlaylists.getSize());
    }

    @Test
    public void findAllByEnabledIsTrueOrderByRankAsc_should_callRepository() {
        //Arrange
        List<Playlist> playlistList = new ArrayList();
        playlistList.add(new Playlist());
        playlistList.add(new Playlist());
        Page<Playlist> pageTrack = new PageImpl(playlistList);
        int size = 1;
        int page = 1;

        Mockito.when(playlistRepository.findAllByEnabledIsTrueOrderByRankAsc(PageRequest.of(page, size)))
                .thenReturn((pageTrack));
        //Act
        Page<Playlist> resultPlaylists = playlistService.findAllByEnabledIsTrueOrderByRankAsc(page, size);

        // Assert
        Assert.assertEquals(2, resultPlaylists.getSize());
    }


}


