package com.example.ridepal.services;

import com.example.ridepal.entities.Track;
import com.example.ridepal.repositories.TrackRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TrackServiceImplTests {

    @Mock
    TrackRepository trackRepository;

    @InjectMocks
    TrackServiceImpl trackService;

    @Mock
    private Pageable pageableMock;



@Test
public void getSpecificAmountOfTracksByGenre_should_callRepository(){
    //Act
    String name = "test";
    int number = 1 ;
    Mockito.when(trackRepository.findSpecificAmountOfRandomTracksByGenre(name,number))
            .thenReturn(Arrays.asList(new Track(), new Track()));

    //Act
    List<Track> tracks = trackService.findSpecificAmountOfTracksByGenre(name,number);

    // Assert
    Assert.assertEquals(2, tracks.size());
}

    @Test
    public void getByGenre_should_callRepository(){
        //Act
        String name = "test";
        int number = 1 ;
        Mockito.when(trackRepository.findAllByGenre_Name(name))
                .thenReturn(Arrays.asList(new Track(), new Track()));

        //Act
        List<Track> tracks = trackService.getAllByGenre(name);

        // Assert
        Assert.assertEquals(2, tracks.size());
    }

    @Test
    public void getAllOrderedByGenre_should_callRepository(){
        //Act
        String name = "test";

        Mockito.when(trackRepository.findAllByGenre_NameOrderByRankDesc(name))
                .thenReturn(Arrays.asList(new Track(), new Track()));

        //Act
        List<Track> tracks = trackService.getAllOrderedTracksByGenre(name);

        // Assert
        Assert.assertEquals(2, tracks.size());
    }




    @Test
    public void findAll_should_callRepository() {
        //Arrange
        List<Track> tracks = new ArrayList();
        tracks.add(new Track());
        tracks.add(new Track());
        Page<Track> pageTrack = new PageImpl(tracks);
        int size = 1;
        int page = 1 ;

        Mockito.when(trackRepository.findAll(PageRequest.of(page, size)))
                .thenReturn((pageTrack));

        //Act
        Page<Track> resultTracks = trackService.findAll(page,size);

        // Assert
        Assert.assertEquals(2,resultTracks.getSize());
    }

    @Test
    public void findAllByTitle_should_callRepository() {
        //Arrange
        List<Track> tracks = new ArrayList();
        tracks.add(new Track());
        tracks.add(new Track());
        Page<Track> pageTrack = new PageImpl(tracks);
        int size = 1;
        int page = 1 ;
        String title = "test";

        Mockito.when(trackRepository.findTracksByTitleContaining(title,PageRequest.of(page, size)))
                .thenReturn((pageTrack));

        //Act
        Page<Track> resultTracks = trackService.findAllByTitle(title,page,size);

        // Assert
        Assert.assertEquals(2,resultTracks.getSize());
    }



}
