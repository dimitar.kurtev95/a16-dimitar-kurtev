package com.example.ridepal.models;

import com.example.ridepal.entities.common.GenderType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UpdateUserProfileDTO {

    @Size(min = 2,
            max =30,
            message = "Name size should not be less than {min} and more than {max} characters long")
    private String firstName;

    @Size(min = 2,
            max=30,
            message = "Name size should not be less than {min} and more than {max} characters long")
    private String lastName;

    @NotEmpty
    @NotBlank
    private String picture;

    private String password;

    private String confirmPassword;

    private String description;

    @Min(5)
    @Max(122)
    private int age;

    private GenderType gender;
}
