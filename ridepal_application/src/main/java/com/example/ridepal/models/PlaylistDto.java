package com.example.ridepal.models;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PlaylistDto {

    @Size(min = 3,
            max =30,
            message = "Name size should not be less than {min} and more than {max} characters long")
    String title;

    String start;

    String finish;

    int percent;

    int rank;

    boolean isArtistRepeated;

    boolean isTopTracksEnabled;

    List<GenreDTO> genreList;

}
