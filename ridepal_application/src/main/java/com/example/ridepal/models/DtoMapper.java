package com.example.ridepal.models;

import com.example.ridepal.entities.Genre;
import com.example.ridepal.entities.Playlist;
import com.example.ridepal.entities.UserDetail;
import com.example.ridepal.services.GenreService;
import com.example.ridepal.services.UserDetailsService;
import com.example.ridepal.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.List;

@Component
public class DtoMapper {

    private UserDetailsService userDetailsService;
    private UserService userService;
    private GenreService genreService;

    @Autowired
    public DtoMapper(UserDetailsService userDetailsService, UserService userService,
                     GenreService genreService) {
        this.userDetailsService = userDetailsService;
        this.userService = userService;
        this.genreService = genreService;
    }

    public UserDetail createFromDto(UserDto userDto) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        UserDetail userDetail = new UserDetail();
        userDetail.setFirstName(userDto.getFirstName());
        userDetail.setLastName(userDto.getLastName());
        userDetail.setEmail(userDto.getEmail());
        userDetail.setAge(userDto.getAge());
        userDetail.setGender(userDto.getGender());
        userDetail.setUser(userService.getByUsername(userDto.getEmail()));
        userDetail.setDateCreated(dateFormat.format(date));

        try {
            String path = new File("src/main/media/uploads/default-avatar.jpg").getAbsolutePath();
            byte[] bFile = Files.readAllBytes(Paths.get(path));
            userDetail.setPicture(Base64.getEncoder().encodeToString(bFile));
        } catch (IOException e) {
            e.printStackTrace();
        }


        return userDetail;
    }

    public UserDetail updateUserProfile(UpdateUserProfileDTO updateUserProfileDTO, UserDetail user) {
        user.setFirstName(updateUserProfileDTO.getFirstName());
        user.setLastName(updateUserProfileDTO.getLastName());
        user.setPicture(updateUserProfileDTO.getPicture());
        user.setAge(updateUserProfileDTO.getAge());
        user.setGender(updateUserProfileDTO.getGender());
        user.setDescription(updateUserProfileDTO.getDescription());

        return user;
    }

    public UpdateUserProfileDTO mapUserToUpdateUserProfileDTO(UpdateUserProfileDTO updateUserProfileDTO, UserDetail user) {
        updateUserProfileDTO.setFirstName(user.getFirstName());
        updateUserProfileDTO.setLastName(user.getLastName());
        updateUserProfileDTO.setPicture(user.getPicture());
        updateUserProfileDTO.setAge(user.getAge());
        updateUserProfileDTO.setGender(user.getGender());
        updateUserProfileDTO.setDescription(user.getDescription());

        return updateUserProfileDTO;
    }

    public Playlist createFromDto(PlaylistDto playlistDto, List<GenreDTO> favouriteGenresDTO, Principal principal) {


        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        Playlist playlist = new Playlist();
        playlist.setCreator(userDetailsService.findByEmail(principal.getName()));
        playlist.setTitle(playlistDto.getTitle());
        playlist.setArtistRepeated(playlistDto.isArtistRepeated());
        playlist.setTopTracksEnabled(playlistDto.isTopTracksEnabled());
        playlist.setDateCreated(dateFormat.format(date));
        playlist.setPicture(("/images/playlist-default-photo.jpg"));
        playlist.setEnabled(true);


        for (int i = 0; i < playlistDto.getGenreList().size(); i++) {
            if (playlistDto.getGenreList().get(i).getName() != null) {
                favouriteGenresDTO.add(playlistDto.getGenreList().get(i));
                Genre genre = genreService.findByName(playlistDto.getGenreList().get(i).getName());
                playlist.getGenres().add(genre);
            }
        }

        return playlist;
    }

    public Playlist updatePlaylistTitleFromDTO(UpdatePlaylistDTO updatePlaylistDTO, Playlist playlist) {
        playlist.setTitle(updatePlaylistDTO.getTitle());
        return playlist;
    }
}
