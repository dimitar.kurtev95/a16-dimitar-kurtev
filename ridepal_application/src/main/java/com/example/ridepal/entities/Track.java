package com.example.ridepal.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tracks")
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Track {

    @JsonProperty("id")
    @Id
    @Column(name = "track_id")
    @EqualsAndHashCode.Include
    String track_id;

    @JsonProperty("title")
    @Column(name = "title")
    String title;

    @JsonProperty("rank")
    @Column(name = "rank")
    int rank;

    @JsonProperty("duration")
    @Column(name = "duration")
    int duration;

    @JsonProperty("preview")
    @Column(name = "preview")
    String preview;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "artist_id")
    @JsonProperty("artist")
    Artist artist;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "album_id")
    @JsonProperty("album")
    Album album;

    @ManyToOne
    @JoinColumn(name = "genre_id")
    Genre genre;
}
