package com.example.ridepal.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "albums")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Album {

    @JsonProperty("id")
    @Id
    @Column(name = "id")
    String album_id;

    @JsonProperty("title")
    @Column(name = "title")
    String title;

    @JsonProperty("cover")
    @Column(name = "cover")
    String cover;

    @JsonProperty("cover_small")
    @Column(name = "cover_small")
    public String cover_small;

    @JsonProperty("cover_medium")
    @Column(name = "cover_medium")
    public String cover_medium;

    @JsonProperty("cover_big")
    @Column(name = "cover_big")
    public String cover_big;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "artist_id")
    Artist artist;
}
