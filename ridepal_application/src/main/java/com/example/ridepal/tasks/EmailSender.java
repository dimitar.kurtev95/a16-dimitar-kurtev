package com.example.ridepal.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public final class EmailSender {

    private static final String sender = "ridepal.playlist.generator@gmail.com";
    private static final String recipient = "ridepal.playlist.generator@gmail.com";

    private JavaMailSender javaMailSender;

    @Autowired
    public EmailSender(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void sendEmail(String sender, String subject, String emailBody) {
        SimpleMailMessage email = new SimpleMailMessage();
        email.setFrom(EmailSender.sender);
        email.setTo(EmailSender.recipient);
        email.setSubject(sender + ": " + subject);
        email.setText(emailBody);

        javaMailSender.send(email);
    }
}
