package com.example.ridepal.exceptions;

public class DuplicateEntityException extends RuntimeException {

    public DuplicateEntityException(String itemType, String attribute, String value) {
        super(String.format("%s with %s \"%s\" already exist", itemType, attribute, value));
    }

    public DuplicateEntityException(String itemType) {
        super(String.format("This %s  already exist", itemType));
    }

    public DuplicateEntityException() {

    }
}




