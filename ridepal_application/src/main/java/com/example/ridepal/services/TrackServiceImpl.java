package com.example.ridepal.services;

import com.example.ridepal.entities.Track;
import com.example.ridepal.repositories.AlbumRepository;
import com.example.ridepal.repositories.ArtistRepository;
import com.example.ridepal.repositories.GenreRepository;
import com.example.ridepal.repositories.TrackRepository;
import com.example.ridepal.tasks.StartPopulationThreads;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TrackServiceImpl implements TrackService {
    private TrackRepository trackRepository;
    private GenreRepository genreRepository;
    private ArtistRepository artistRepository;
    private AlbumRepository albumRepository;

    @Autowired
    public TrackServiceImpl(TrackRepository trackRepository, GenreRepository genreRepository, ArtistRepository artistRepository, AlbumRepository albumRepository) {
        this.trackRepository = trackRepository;
        this.genreRepository = genreRepository;
        this.artistRepository = artistRepository;
        this.albumRepository = albumRepository;
    }

    @Override
    public List<Track> findSpecificAmountOfTracksByGenre(String name, int number) {
        return trackRepository.findSpecificAmountOfRandomTracksByGenre(name, number);
    }

    @Override
    public List<Track> getAllByGenre(String name) {
        return trackRepository.findAllByGenre_Name(name);
    }

    public List<Track> getAllOrderedTracksByGenre(String name){
        return trackRepository.findAllByGenre_NameOrderByRankDesc(name);
    }

    @Override
    public Page<Track> findAll(int page, int size) {
        return trackRepository.findAll(PageRequest.of(page, size));
    }

    @Override
    public Page<Track> findAllByTitle(String title, int page, int size) {
        return trackRepository.findTracksByTitleContaining(title, PageRequest.of(page, size));
    }

    @Override
    public Boolean updateTracksByGenre(String genre) {
        new StartPopulationThreads(genreRepository, trackRepository, artistRepository, albumRepository, genre).start();
        return true;
    }
}
