package com.example.ridepal.services;

import com.example.ridepal.entities.Genre;
import com.example.ridepal.exceptions.EntityNotFoundException;
import com.example.ridepal.repositories.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenreServiceImpl implements GenreService {

    private GenreRepository repository;

    @Autowired
    public GenreServiceImpl(GenreRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Genre> findAll() {
        return repository.findAll();
    }

    @Override
    public Genre findByName(String name) {
        Genre genre = repository.findByName(name);

        if (genre != null) {
            return genre;
        } else throw new EntityNotFoundException("Genre", "name",name);
    }
}
