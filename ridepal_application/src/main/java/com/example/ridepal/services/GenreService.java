package com.example.ridepal.services;

import com.example.ridepal.entities.Genre;

import java.util.List;

public interface GenreService {
    List<Genre> findAll();

    public Genre findByName(String name);
}
