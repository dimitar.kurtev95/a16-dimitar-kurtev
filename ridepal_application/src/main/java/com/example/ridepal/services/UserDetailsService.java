package com.example.ridepal.services;

import com.example.ridepal.entities.UserDetail;

import java.security.Principal;
import java.util.List;

public interface UserDetailsService {

    void createUserDetail(UserDetail userDetail);

    UserDetail updateUserDetail(UserDetail userDetail, Principal principal);

    void softDeleteUserDetail(UserDetail userDetail, Principal principal);

    UserDetail findById(int id);

    UserDetail findByEmail(String email);

    boolean isUserExist(String email);

    List<UserDetail> findAll();
}
