package com.example.ridepal.services;

import com.example.ridepal.entities.Track;
import com.example.ridepal.repositories.AlbumRepository;
import com.example.ridepal.repositories.ArtistRepository;
import com.example.ridepal.repositories.GenreRepository;
import com.example.ridepal.repositories.TrackRepository;
import org.springframework.data.domain.Page;

import java.util.List;

public interface TrackService {


    List<Track> findSpecificAmountOfTracksByGenre(String name, int amount);

    List<Track> getAllByGenre(String name);

    List<Track> getAllOrderedTracksByGenre(String name);

    Page<Track> findAll(int page, int size);

    Page<Track> findAllByTitle(String title, int page, int size);

    Boolean updateTracksByGenre(String genre);
}
