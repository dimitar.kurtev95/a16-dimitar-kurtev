package com.example.ridepal.services;

import com.example.ridepal.entities.User;

public interface UserService {

    User getByUsername(String username);


}
