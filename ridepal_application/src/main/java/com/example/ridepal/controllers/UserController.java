package com.example.ridepal.controllers;

import com.example.ridepal.entities.Playlist;
import com.example.ridepal.entities.UserDetail;
import com.example.ridepal.models.DtoMapper;
import com.example.ridepal.models.UpdateUserProfileDTO;
import com.example.ridepal.services.GenreService;
import com.example.ridepal.services.PlaylistService;
import com.example.ridepal.services.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.Principal;
import java.util.Base64;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class UserController {

    private UserDetailsService userDetailsService;
    private DtoMapper mapper;
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private PlaylistService playlistService;
    private GenreService genreService;


    @Autowired
    public UserController(UserDetailsService userDetailsService, DtoMapper mapper,
                          UserDetailsManager userDetailsManager, PasswordEncoder passwordEncoder, PlaylistService playlistService, GenreService genreService) {
        this.userDetailsService = userDetailsService;
        this.mapper = mapper;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.playlistService = playlistService;
        this.genreService = genreService;
    }

    @GetMapping("/user-profile/{id}")
    public String showUserPage(@PathVariable int id, Model model, Principal principal, UpdateUserProfileDTO updateUserProfileDTO) {
        UserDetail user = userDetailsService.findById(id);

        updateUserProfileDTO = mapper.mapUserToUpdateUserProfileDTO(updateUserProfileDTO, user);

        if (principal != null) {
            model.addAttribute("loggedUser", userDetailsService.findByEmail(principal.getName()));
        }

        model.addAttribute("allUsers", userDetailsService.findAll());
        model.addAttribute("user", user);
        model.addAttribute("userProfileDTO", updateUserProfileDTO);
        model.addAttribute("allPlaylistByUser", playlistService.findAllByCreator(id)
                .stream()
                .sorted(Comparator.comparing(Playlist::getDateCreated).reversed())
                .collect(Collectors.toList()));
        model.addAttribute("genres", genreService.findAll());

        return "user-profile";
    }

    @PostMapping("/user-profile/{id}/update")
    public String updateUserProfile(@PathVariable int id,
                                    @ModelAttribute(name = "user") UpdateUserProfileDTO user,
                                    @RequestParam(value = "file") MultipartFile file,
                                    Principal principal) {

        UserDetail userInDB = userDetailsService.findById(id);

        if (!user.getPassword().isEmpty()) {
            changingPassword(user, userInDB);
        }

        saveUserProfilePicture(user, file, principal, userInDB);

        userDetailsService.updateUserDetail(mapper.updateUserProfile(user, userInDB), principal);

        return "redirect:/user-profile/{id}";
    }

    @PostMapping("/user-profile/{id}/delete")
    public String deleteUserProfile(@PathVariable int id, Principal principal, HttpServletRequest request) {

        UserDetail userInDB = userDetailsService.findById(id);

        UserDetail userPrincipal = userDetailsService.findByEmail(principal.getName());

        userDetailsService.softDeleteUserDetail(userInDB, principal);

        if (isSelfDelete(id, userPrincipal)) {
            request.getSession().invalidate();
            return "redirect:/";
        }

        return String.format("redirect:/user-profile/%s", userPrincipal.getId());
    }

    private boolean isSelfDelete(int id, UserDetail userPrincipal) {
        return id == userPrincipal.getId();
    }

    private void saveUserProfilePicture(UpdateUserProfileDTO user, MultipartFile file, Principal principal, UserDetail userInDB) {

        try {
            if (!file.isEmpty()) {
                user.setPicture(Base64.getEncoder().encodeToString(file.getBytes()));
            } else {
                user.setPicture(userInDB.getPicture());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void changingPassword(UpdateUserProfileDTO userDto, UserDetail currentUser) {
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User toUpdateUser =
                new org.springframework.security.core.userdetails.User(
                        currentUser.getEmail(),
                        passwordEncoder.encode(userDto.getPassword()),
                        authorities);
        userDetailsManager.updateUser(toUpdateUser);
    }
}
