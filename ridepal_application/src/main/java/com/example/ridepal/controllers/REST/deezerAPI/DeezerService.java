package com.example.ridepal.controllers.REST.deezerAPI;

import com.example.ridepal.controllers.REST.deezerAPI.searchDTOs.SearchDTO;
import com.example.ridepal.controllers.REST.deezerAPI.tracksDTOs.TrackDTO;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface DeezerService {

    @GET("/search/playlist")
    Call<SearchDTO> getPlayListsByGenre(@Query("q") String genre, @Query("index") int index);

    @GET("/playlist/{id}/tracks")
    Call<TrackDTO> getAllTracksFromPlaylist(@Path("id") String id);

    @GET("/playlist/{id}/tracks")
    Call<TrackDTO> getAllTracksFromPlaylist(@Path("id") String id, @Query("index") int index);
}
