package com.example.ridepal.controllers;

import com.example.ridepal.entities.Playlist;
import com.example.ridepal.entities.Track;
import com.example.ridepal.entities.common.GenderType;
import com.example.ridepal.models.EmailDTO;
import com.example.ridepal.models.UserDto;
import com.example.ridepal.services.GenreService;
import com.example.ridepal.services.PlaylistService;
import com.example.ridepal.services.TrackService;
import com.example.ridepal.services.UserDetailsService;
import com.example.ridepal.tasks.EmailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class HomeController {

    public static final int MAIN_TOP_PLAYLISTS_SIZE = 2;
    public static final int SECONDARY_TOP_PLAYLISTS_SIZE = 10;

    private GenreService genreService;
    private PlaylistService playlistService;
    private TrackService trackService;
    private UserDetailsService userDetailsService;
    private JavaMailSender javaMailSender;

    @Autowired
    public HomeController(GenreService genreService, PlaylistService playlistService,
                          TrackService trackService, UserDetailsService userDetailsService, JavaMailSender javaMailSender) {
        this.genreService = genreService;
        this.playlistService = playlistService;
        this.trackService = trackService;
        this.userDetailsService = userDetailsService;
        this.javaMailSender = javaMailSender;
    }

    @GetMapping("/")
    public String showHomepage(Model model, Principal principal) {
        if (principal != null) {
            model.addAttribute("loggedUser", userDetailsService.findByEmail(principal.getName()));
        }

        model.addAttribute("userDto", new UserDto());
        model.addAttribute("genders", GenderType.values());
        model.addAttribute("genres", genreService.findAll());

        List<Playlist> topPlaylists = playlistService.findAllByEnabledIsTrueOrderByRankAsc(0, 10).get().collect(Collectors.toList());

        if (topPlaylists.size() >= MAIN_TOP_PLAYLISTS_SIZE) {
            model.addAttribute("topPlaylists", topPlaylists.subList(0, MAIN_TOP_PLAYLISTS_SIZE));
            if (topPlaylists.size() >= SECONDARY_TOP_PLAYLISTS_SIZE) {
                model.addAttribute("top8Playlists", topPlaylists.subList(MAIN_TOP_PLAYLISTS_SIZE, SECONDARY_TOP_PLAYLISTS_SIZE));
            }
        }

        return "index";
    }

    @GetMapping("/search/tracks")
    public ResponseEntity<Page<Track>> searchTracks(@RequestParam(name = "page", defaultValue = "0") int page,
                                                    @RequestParam(name = "size", defaultValue = "100") int size,
                                                    @RequestParam(name = "title", defaultValue = "") String title) {

        return new ResponseEntity<>(trackService.findAllByTitle(title, page, size), HttpStatus.OK);
    }

    @GetMapping("/search/playlists")
    public ResponseEntity<Page<Playlist>> searchPlaylists(@RequestParam(name = "page", defaultValue = "0") int page,
                                                          @RequestParam(name = "size", defaultValue = "100") int size,
                                                          @RequestParam(name = "title", defaultValue = "") String title,
                                                          @RequestParam(name = "durationFrom", defaultValue = "0") String durationFrom,
                                                          @RequestParam(name = "durationTo", defaultValue = "2147483647") String durationTo,
                                                          @RequestParam(name = "sort", defaultValue = "notSorted") String sort) {


        return new ResponseEntity<>(playlistService.findPlaylistByTitleContaining(title, page, size, Integer.parseInt(durationFrom), Integer.parseInt(durationTo), sort), HttpStatus.OK);
    }

    @GetMapping("/new-music.html")
    public String showNewMusic(Model model,
                               @RequestParam(name = "page", defaultValue = "0") int page,
                               @RequestParam(name = "size", defaultValue = "21") int size) {
        Page<Track> trackPage = trackService.findAll(page, size);
        model.addAttribute("tracks", trackPage);
        model.addAttribute("trackPages", trackPage.getTotalPages());
        model.addAttribute("currentPage", page);
        return "new-music";
    }

    @GetMapping("/genres.html")
    public String showGenres(Model model) {
        model.addAttribute("genres", genreService.findAll());
        return "genres";
    }

    @GetMapping("/playlists.html")
    public String showPlaylists(Model model,
                                @RequestParam(name = "page", defaultValue = "0") int page,
                                @RequestParam(name = "size", defaultValue = "21") int size) {
        Page<Playlist> playlistPage = playlistService.findAll(page, size);
        model.addAttribute("playlists", playlistPage);
        model.addAttribute("playlistsPages", playlistPage.getTotalPages());
        model.addAttribute("currentPage", page);
        return "playlists";
    }

    @PostMapping("/email")
    public ResponseEntity<String> sendMail(@RequestBody EmailDTO emailDTO) {
        new EmailSender(javaMailSender).sendEmail(emailDTO.getEmailAddress(), emailDTO.getSubject(), emailDTO.getMessage());
        return new ResponseEntity<>("Thank you for reaching us! :)", HttpStatus.OK);
    }

//    @GetMapping("/search-songs")
//    public String searchSongs(@RequestParam(name = "name", required = false) String name,
//                              @RequestParam(name = "genre", required = false) String genre,
//                              Model model) {
//        System.out.println("test");
//        return "new-music";
//    }

    @GetMapping("/tracks/update/{genre}")
    public ResponseEntity<Boolean> updateTracks(@PathVariable String genre) {
        return new ResponseEntity<>(trackService.updateTracksByGenre(genre), HttpStatus.OK);
    }
}
