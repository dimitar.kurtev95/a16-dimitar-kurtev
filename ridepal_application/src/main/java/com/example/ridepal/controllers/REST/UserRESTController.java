package com.example.ridepal.controllers.REST;

import com.example.ridepal.entities.UserDetail;
import com.example.ridepal.models.DtoMapper;
import com.example.ridepal.models.UpdateUserProfileDTO;
import com.example.ridepal.services.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("api/users")
public class UserRESTController {

    private UserDetailsService userDetailsService;
    private DtoMapper mapper;

    @Autowired
    public UserRESTController(UserDetailsService userDetailsService, DtoMapper mapper) {
        this.mapper = mapper;
        this.userDetailsService = userDetailsService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDetail> getById(@PathVariable int id) {
       UserDetail userDetail= userDetailsService.findById(id);

        return ResponseEntity.status(HttpStatus.OK).body(userDetail);
    }

    @GetMapping("/")
    public List<UserDetail> getAll() {

        return userDetailsService.findAll();

    }

    @PutMapping("/{id]")
    public ResponseEntity<?> updateUser(@PathVariable int id,
                                              UpdateUserProfileDTO user, Principal principal){

        UserDetail userInDB = userDetailsService.findById(id);
        userDetailsService.updateUserDetail(mapper.updateUserProfile(user, userInDB), principal);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable int id, Principal principal){

        UserDetail userInDB = userDetailsService.findById(id);

        userDetailsService.softDeleteUserDetail(userInDB,principal);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
