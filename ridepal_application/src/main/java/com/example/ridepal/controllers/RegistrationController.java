package com.example.ridepal.controllers;


import com.example.ridepal.entities.UserDetail;
import com.example.ridepal.exceptions.DuplicateEntityException;
import com.example.ridepal.models.DtoMapper;
import com.example.ridepal.models.UserDto;
import com.example.ridepal.services.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@Controller
public class RegistrationController {
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private UserDetailsService userDetailsService;
    private DtoMapper dtoMapper;


    @Autowired
    public RegistrationController(UserDetailsManager userDetailsManager,
                                  PasswordEncoder passwordEncoder,
                                  UserDetailsService userDetailsService, DtoMapper dtoMapper) {
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.userDetailsService = userDetailsService;
        this.dtoMapper = dtoMapper;


    }


    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute UserDto userDto) {


        if (userDetailsService.isUserExist(userDto.getEmail())) {
            throw new DuplicateEntityException("");
        }

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        userDto.getEmail(),
                        passwordEncoder.encode(userDto.getPassword()),
                        authorities);

        userDetailsManager.createUser(newUser);
        UserDetail userDetail = dtoMapper.createFromDto(userDto);

        userDetailsService.createUserDetail(userDetail);

        return "register-confirmation";
    }


    @GetMapping("/register-confirmation")
    public String showRegisterConfirmation() {
        return "register-confirmation";
    }


    @RequestMapping(value = {"/exist/{name}"}, method = RequestMethod.POST)
    public void checkUsername(@PathVariable("name") String username, HttpServletResponse response) throws IOException {

        response.setContentType("text/html;charset=UTF-8");

        PrintWriter print = response.getWriter();

        if (!userDetailsService.isUserExist(username)) {
            print.println(1);
        }

    }
}

