package com.example.ridepal.repositories;

import com.example.ridepal.entities.Authority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorityRepository extends JpaRepository<Authority,String> {
    List<Authority> findAllByUsername(String name);
}
