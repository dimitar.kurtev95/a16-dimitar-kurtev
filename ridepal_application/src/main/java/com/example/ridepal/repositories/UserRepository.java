package com.example.ridepal.repositories;

import com.example.ridepal.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
    User getByUsername(String username);
}
