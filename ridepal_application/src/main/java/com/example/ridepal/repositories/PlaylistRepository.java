package com.example.ridepal.repositories;

import com.example.ridepal.entities.Playlist;
import com.example.ridepal.entities.UserDetail;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlaylistRepository extends JpaRepository<Playlist, Integer> {

    List<Playlist> findAllByCreatorIdAndEnabledIsTrue(int id);

    List<Playlist> findAllByEnabledIsTrue();

    Page<Playlist> findAllByEnabledIsTrueOrderByDateCreatedDesc(Pageable pageable);

    Playlist findByTitleAndCreatorAndEnabledIsTrue(String title, UserDetail user);

    Playlist findByIdAndEnabledIsTrue(int id);

    Page<Playlist> findPlaylistByTitleContaining(String title, Pageable pageable);

//    Page<Playlist> findPlaylistByTitleContainingAndDurationContaining(String title, int duration, Pageable pageable);

    Page<Playlist> findPlaylistByTitleContainingAndDurationBetweenAndEnabledIsTrue(String title, int durationFrom, int durationTo, Pageable pageable);

    Page<Playlist> findAllByEnabledIsTrueOrderByRankAsc(Pageable pageable);
}