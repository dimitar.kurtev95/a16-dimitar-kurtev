package com.example.ridepal.repositories;

import com.example.ridepal.entities.Track;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrackRepository extends JpaRepository<Track, Integer> {

    @Query(value = "select *\n" +
            "from ridepal.tracks t\n" +
            "left join ridepal.genres g on t.genre_id = g.id \n" +
            "where g.name = :nameTrack\n" +
            "ORDER BY RAND()\n" +
            "limit :numberTrack", nativeQuery = true)
    List<Track> findSpecificAmountOfRandomTracksByGenre(@Param("nameTrack") String nameTrack,
                                                        @Param("numberTrack") int countOfTracks);
    List<Track> findAllByGenre_Name(String name);

    List<Track> findAllByGenre_NameOrderByRankDesc(String name);

    Page<Track> findTracksByTitleContaining(String title, Pageable pageable);
}
