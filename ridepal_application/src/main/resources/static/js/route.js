type = "text/javascript" >

    $(document).ready(function () {

        $("#final-position, #start-position").blur(function () {

            let first = $('#start-position').val();
            let second = $('#final-position').val();

            $.ajax({
                type: "GET",
                url: "/destinationMap/?first=" + first + "&second=" + second,
                success: function (result) {

                    if (result !== '') {
                        $('.image-map').html('<img src=' + 'data:image/png;base64,' + result + '>');
                        $("#btn").removeAttr('disabled');
                        $('.not-valid-destination').css({
                            'visibility': 'hidden'
                        });
                    }
                },
                error: function (error) {
                    console.log(error);
                    $('.not-valid-destination').css({
                        'visibility': 'visible',
                        'color': '#8B0000',
                        'font-weight': 'bold',
                        'font-size': 'large'
                    });
                    $("#btn").attr('disabled', 'disabled');
                    $('.image-map').empty();
                }
            });
        });

        $('.custom-control-input').change(function () {

            var listOfGenres1 = Array.from(document.getElementsByClassName("custom-control-input"));
            var sumOfPercents = 0;

            for (var i = 0; i < listOfGenres1.length; i++) {

                var genre = document.getElementById('genres-box' + i);
                var inputOfPercents = document.getElementById('genres-percent' + i);

                if (genre == null) {
                    continue;
                }

                if (genre.checked) {
                    inputOfPercents.required = true;

                } else {
                    inputOfPercents.value=null;
                    inputOfPercents.required = false;
                }

                sumOfPercents += parseInt(inputOfPercents.value, 10) || 0;
            }


            if (sumOfPercents === 100) {
                $("#btn").removeAttr('disabled');

            } else {
                $("#btn").attr('disabled', 'disabled');
            }

        });

        $(".genreBox").keyup(function () {

            var listOfGenres = Array.from(document.getElementsByClassName("genreBox"));

            var sumOfValues = 0;

            for (var i = 0; i < listOfGenres.length; i++) {
                sumOfValues += parseInt(listOfGenres[i].value, 10) || 0;

                if ((parseInt(listOfGenres[i].value, 10)) === 0) {
                    listOfGenres[i].value = 1;
                    sumOfValues += parseInt(listOfGenres[i].value, 10) || 0;
                }
            }
            if (sumOfValues === 100) {
                $("#btn").removeAttr('disabled');
                $('.genre-error').css({
                    'visibility': 'hidden'

                });

            } else {
                $("#btn").attr('disabled', 'disabled');
                $('.genre-error').css({
                    'visibility': 'visible',
                    'color': '#8B0000',
                    'font-weight': 'bold',
                    'font-size': 'large'
                });

            }

        });

        $("#btn").click(function (a) {

            var listOfGenres1 = Array.from(document.getElementsByClassName("custom-control-input"));

            for (var z = 0; z < listOfGenres1.length; z++) {

                var inputOfPercents = document.getElementById('genres-percent' + z);

                if (inputOfPercents.required && inputOfPercents.value === "") {
                    alert("There is empty input of percents");
                    clearTimeout();
                    return;
                }
            }

            let startPosition = $('#start-position').val();
            let finalPosition = $('#final-position').val();
            let title = $('#title').val();


            if (title === "" || startPosition === "" || finalPosition === "") {
                alert("There are required fields");
                clearTimeout();
                return;
            }

            var listOfGenres2 = Array.from(document.getElementsByClassName("custom-control-input"));
            var counterIfAllGenresAreEmpty = 0;

            for (var i = 0; i < listOfGenres2.length; i++) {
                var genre = listOfGenres2[i];

                if (!genre.checked)
                    counterIfAllGenresAreEmpty++;
            }

            if (counterIfAllGenresAreEmpty === listOfGenres2.length) {
                alert("Please choose genre");
                a.preventDefault();
                return;
            }

            let sitePreloader = $(".preloader");
            sitePreloader.attr("style", "");

            $("#generatePlaylistForm").submit(function (e) {

                e.preventDefault();
                $.ajax({
                    url: '/generate-playlist',
                    type: 'post',
                    dataType: 'html',
                    data: $('#generatePlaylistForm').serialize(),
                    success: function (result) {

                        // sitePreloader.attr("style", "display: none;");
                        // console.log(result);
                        window.location.href='/user-profile/' + $("#btn").attr("data-userID");
                        // $("#site-content-inner").html(result);
                    },
                    error: function () {

                        sitePreloader.attr("style", "display: none;");

                        $('.duration-error').css({
                            'visibility': 'visible',
                            'color': 'red'
                        });
                    }
                });
            })
        });

        $(".searchBox").autocomplete({
            minLength: 4,
            // appendTo: '.start-position-container',
            source: function (request, response) {
                $.ajax({
                    url: "http://dev.virtualearth.net/REST/v1/Locations",
                    dataType: "jsonp",
                    data: {
                        key: "AmnMvkD2lxGS4UfIZugXc3Fkbb-JgPkm8dFlaAAseAzdcT_eeb0NjWc7ZpqZnQhe",
                        q: request.term
                    },
                    jsonp: "jsonp",
                    success: function (data) {
                        var result = data.resourceSets[0];
                        if (result) {
                            if (result.estimatedTotal > 0) {
                                response($.map(result.resources, function (item) {
                                    return {
                                        data: item,
                                        label: item.name,
                                        value: item.name
                                    }
                                }));
                            }
                        }
                    }
                });
            },
            minLength: 1,
            change: function (event, ui) {
                if (!ui.item)
                    $(".searchBox").val('');

            },
            select: function (event, ui) {
                displaySelectedItem(ui.item.data);
            }
        });
    });

function displaySelectedItem(item) {
    $("#searchResult");

}



