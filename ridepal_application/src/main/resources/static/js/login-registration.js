
$(document).ready(function () {

    $(".form-control").keyup(function () {

        let password = $("#register-password").val();
        let confirmPassword = $("#confirm-password").val();

        if (password !== confirmPassword) {
            $('.span-password').css({
                'visibility': 'visible',
                'color': '#8B0000',
                'font-weight': 'bold'
            });
            $("#btn-register").attr('disabled', 'disabled');
        } else {
            $('.span-password').css({
                'visibility': 'hidden',
            });

            $("#btn-register").removeAttr('disabled');
        }

    });

    $("#email").blur(function () {

        let username = $('#email').val();

        $.ajax({
            type: "POST",
            url: "/exist/" + username,
            success:
                function (msg) {
                    if (msg == 1) {
                        $(".status-email").html("<font color=green><b>" + username + "</b> is available");

                        $("#btn-register").removeAttr('disabled');
                    } else {

                        $(".status-email").html("<font color=red><b>" + username + "</b> already exist");

                        $("#btn-register").attr('disabled', 'disabled');
                    }
                }
        });
    });

    $("#update-pass, #update-pass-confirm").on('keyup', function () {

        let password = $("#update-pass").val();
        let confirmPassword = $("#update-pass-confirm").val();

        if (password !== confirmPassword) {
            $('.span-password-user').css({
                'visibility': 'visible',
                'color': 'red'
            });
            $("#saveUserChanges").attr('disabled', 'disabled');
        } else {
            $('.span-password-user').css({
                'visibility': 'hidden',
            });

            $("#saveUserChanges").removeAttr('disabled');
        }
    });

});